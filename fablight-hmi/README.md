# Node.js Upgrade and Dependencies Upgrade - HMI #


Repository : HMI
Updated on : April 10,2023
Version    : v2.0.1


### What is this repository for? ###

This project has recently undergone an upgrade of Node.js to version v19.3.0, and some of its dependencies have also been updated.

### Dependencies Upgrade ###

To upgrade dependencies, we followed these steps:

    1.Backup Fablight-hmi project

    2.Review package.json and npm outdated output to find the packages that need updating

    3.Run npm install to install the latest package versions that match the version ranges specified in package.json

    4.Run npm start to check fabserver is running or not

### Conclusion ###

This project has been successfully upgraded to Node.js version v19.3.0 and all dependencies have been updated to their latest versions.