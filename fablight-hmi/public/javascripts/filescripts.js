// filescripts.js:
// AJAX calls for interface elements on the file upload page

/*############# AJAX calls #############*/


// TODO get ajax working with form upload.
// this code isn't working right now. Using action = 'POST' on the button instead.

$('#upload-form').submit(function(e) {
//    e.preventDefault();
    $(this).ajaxSubmit({

        error: function(xhr) {
            console.log('Error: ' + xhr.status);
        },

        success: function(response) {
            console.log('Success: ' + response);
        }
    });

    return false;
});