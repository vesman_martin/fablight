// ---------- Homing dialog ----------
var attemptedToHome = false;

// Button to open the homing dialog
$homeMachineBtn.on('click', function () {
	homingDialogHeader.text('Home machine?');
	homingDialogContent.html('<h2>Press the button below to home the machine</h2>');
	closeHomingDialogButton.show();
	$homeMachineButton.show();
	if (homingDialog.open === false) {
		homingDialog.showModal();
	}
});

// Button in the dialog that actually sends the homing command
$homeMachineButton.on('click', function () {
	attemptedToHome = true;
	socket.emit('fl_find_limits');
	homingDialogHeader.text('Finding home.');
	homingDialogContent.html('<h2>Finding machine home...</h2>');
	$homeMachineButton.hide();
	closeHomingDialogButton.show();
});

socket.on('fl_find_limits__status', function (s) {
	homingDialogHeader.text('Finding home.');
	homingDialogContent.html('<p>Homing...</p>');
	console.log(s);
	var statusMsg;
	switch (parseInt(s)) {
		case 0:
			statusMsg = 'Starting.'
			break;
		case 1:
			statusMsg = 'Complete.'
			homingDialog.close();
			attemptedToHome = false;
			if(typeof $machineStatus !== 'undefined') {
				$machineStatus.text('Machine homed.')
			}
			break;
		case 2:
			statusMsg = 'Homing Z.'
			break;
		case 3:
			statusMsg = 'Homing Z...complete'
			break;
		case 4:
			statusMsg = 'Homing F'
			break;
		case 5:
			statusMsg = 'Homing F...complete'
			break;
		case 6:
			statusMsg = 'Homing X'
			break;
		case 7:
			statusMsg = 'Homing X...complete'
			break;
		case 8:
			statusMsg = 'Homing Y'
			break;
		case 9:
			statusMsg = 'Homing Y...complete'
			break;
		case 10:
			statusMsg = 'Homing R...complete'
			break;
		case 11:
			statusMsg = 'Homing R...complete'
			break;
		default:
			statusMsg = 'Homing...'
			break;
	}
	homingDialogContent.html('<p>' + statusMsg + '</p>');
	$homeMachineButton.hide();
	closeHomingDialogButton.show();
	if((homingDialog.open === false) && (parseInt(s) !== 1)) {
		homingDialog.showModal();
	}
});