// ------------ logic for preview rendering ------------ //

// generatePreview is the main entry point for the preview logic.
function generatePreview() {
	// Initialize the scene, including camera and renderer
	init();
	// Render the file and animate the preview
	// 2D preview is static. 3D preview rotates the tube slowly.
	if(is3Dfile === true) {
		animate3D();
	}
	else {
		animate2D();
	}
}

generatePreview();

// detects if the browser supports webgl
if (!Detector.webgl) Detector.addGetWebGLMessage();
var group, camera, scene, renderer;

function init() {
	scene = new THREE.Scene();
	renderer = new THREE.WebGLRenderer({
		antialias: true,
		alpha: true
	});
	// The width and height for the camera are set from the canvas width and height.
	// Changes based on which HMI screen resolution we are using. UDOO screen:
	var topBarHeight = 56;
	var width = 800;
	var height = 480 - topBarHeight;
	// New HMI screen (1024 x 600):
	if(window.innerWidth > 1000) {
		topBarHeight = 72;
		width = 1024;
		height = 600 - topBarHeight;
	}
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(width, height);	// Resize the output canvas to the correct size
	renderer.setClearColor(0x000000, 0);
	document.body.appendChild(renderer.domElement);
	console.log('renderer set canvas width:' + width + ' height:' + height)

	var aspectRatio = width/height;

	// Show axes - useful for debugging
	var axis = new THREE.AxisHelper(50)
	// scene.add(axis)
	var grid = new THREE.GridHelper(50, 50);
	// scene.add(grid)

	// get bounding box of geometry in terms of three.js pixel dimensions
	// helper.box.max is {"x": xMax, "y": yMax, "z": zMax}
	// helper.box.min should be {"x":0,"y":0,"z":0}
	var helper = new THREE.BoundingBoxHelper(group, 0xff0000);
	helper.update();
	console.log('bounding box width:' + helper.box.max.x + ' height:' + helper.box.max.y)

	// Default camera dimensions for 2D job. Shows entire bed.
	// These camera parameters are going to change based on the file bounds.
	var cameraParams = {
		left: 0,
		right: 635*aspectRatio,
		top: 635,
		bottom: 0,
		near: -2000,
		far: 2000
	};

	var hoffset = 8;
	var voffset = 4;
	if(is3Dfile === true) {
		hoffset = 2;
		voffset = 5;
		cameraParams.left = -8;
	}

	console.log('is3Dfile = ' + is3Dfile);

	// Start of code to change the camera parameters based on the job file dimensions so the entire
	// file fits on screen. First, get maximum dimension of x, y, and z using helper.box.max
	var maxLength, offset, xDim, yDim, zDim;
	xDim = helper.box.max.x - helper.box.min.x;
	yDim = helper.box.max.y - helper.box.min.y;
	zDim = helper.box.max.z - helper.box.min.z;
	console.log(helper.box)
	// X is the largest dimension - for 2D files, most tubes.
	if((xDim > yDim) && (xDim > zDim)) {
		xLargestDimension(is3Dfile);
	}
	// Y is the largest dimension - for 2D files, very short tube sections.
	else if((yDim > xDim) && (yDim > zDim)) {
		yLargestDimension(is3Dfile)
	}
	// Z is the largest dimension - for very short tube sections.
	else if((zDim > xDim) && (zDim > yDim)) {
		zLargestDimension(is3Dfile)
	}
	// 1. X == Y, so set largest to Y - for 2D files.
	// 2. Y == Z, less than X - for short square tube files
	// 3. All other cases
	else {
		yLargestDimension(is3Dfile);
	}

	function yLargestDimension(is3Dfile) {
		maxLength = helper.box.max.y;
		console.log('yLargestDimension) Y is the largest dimension')
		offset = maxLength * .05;
		if(is3Dfile) {
			cameraParams.top = maxLength + offset;
			cameraParams.bottom = - offset;
			cameraParams.left = - offset;
			cameraParams.right = aspectRatio * (cameraParams.top - cameraParams.bottom) + cameraParams.left
		}
		else {
			cameraParams.top = maxLength + offset;
			cameraParams.bottom = - offset;
			cameraParams.left = - offset;
			cameraParams.right = aspectRatio * (cameraParams.top - cameraParams.bottom) + cameraParams.left
		}
	}

	function xLargestDimension(is3Dfile) {
		maxLength = xDim;
		console.log('xLargestDimension) X is the largest dimension')
		if(is3Dfile) {
			console.log('xLargestDimension) tube height (Y) will fit in view');
			offset = maxLength * 0.1;
			cameraParams.right = maxLength + offset;
			cameraParams.left = - offset;
			cameraParams.top = (maxLength + 2 * offset)/(2 * aspectRatio);
			cameraParams.bottom = - (maxLength + 2 * offset)/(2 * aspectRatio);
			if(helper.box.max.y > cameraParams.top) {
				// Although this file is longest in X, if the view was resized to fit the tube in the X
				// direction, the tube is short enough that it would be cut off in the Y direction.
				console.log('xLargestDimension) tube height (Y) won\'t fit in view');
				yLargestDimension(is3Dfile)
			}
			else if(helper.box.max.z > cameraParams.top) {
				console.log('xLargestDimension) tube depth (Z) won\'t fit in view');
				zLargestDimension(is3Dfile)
			}
		}
		else {
			offset = maxLength * .05;
			cameraParams.bottom = - offset;
			cameraParams.left = - offset;
			cameraParams.right = maxLength + offset;
			cameraParams.top = (cameraParams.right - cameraParams.left)/aspectRatio + cameraParams.bottom
			if(helper.box.max.y > cameraParams.top) {
				console.log('xLargestDimension) Although X is largest dim, part height won\'t fit');
				yLargestDimension(is3Dfile)
			}
		}
	}

	function zLargestDimension(is3Dfile) {
		console.log('zLargestDimension) Z is the largest dimension')
		maxLength = helper.box.max.z;
		offset = maxLength * .1;
		if(is3Dfile) {
			cameraParams.top = maxLength + offset;
			cameraParams.bottom = - offset;
			cameraParams.left = - offset;
			cameraParams.right = aspectRatio * (cameraParams.top - cameraParams.bottom) + cameraParams.left
		}
		else {
			cameraParams.bottom = - offset;
			cameraParams.left = - offset;
			cameraParams.right = maxLength + offset;
			cameraParams.top = (cameraParams.right - cameraParams.left)/aspectRatio + cameraParams.bottom
		}
	}

	camera = new THREE.OrthographicCamera(
		cameraParams.left,
		cameraParams.right,
		cameraParams.top,
		cameraParams.bottom,
		cameraParams.near,
		cameraParams.far
	);
	camera.position.set(0, 0, maxLength * 2.0);
	camera.lookAt(new THREE.Vector3(0, 0, 0));

	scene.add(camera);

	// controls for orbit
	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.minDistance = 20;
	controls.maxDistance = 300;

	scene.add( new THREE.AmbientLight( 0x222222 ) );
	var light = new THREE.PointLight( 0xffffff );
	light.position.copy( camera.position );
	scene.add( light );

	/*
	 The exported geometry from freecad is made up of lots of vectors and faces.
	 I am grouping all the geometry into a threejs group called 'group'.
	 Then a new group is made called group2 which contains group.
	 group2 is used to absolutely position all of the geometry to take care of rotation around the
	 axes, because by default the threejs geometry rotates around it's origin at the corner. We want
	 rotation around the tube's center instead.
	 It may be possible to use a translation matrix here instead?
	 http://stackoverflow.com/questions/17907293/three-js-rotate-object3d-around-y-axis-at-it-center
	 */

	group2 = new THREE.Group();
	group2.add(group);
	scene.add(group2);

	// position of the geometry relative to the origin
	if(is3Dfile === true) {
		group.position.y -= (helper.box.max.y + helper.box.min.y) / 2.0;
	} else {
		group.position.y -= 0;
	}
	group.position.z -= 0;
	group.position.x -= 0;

	resetView();
}

function animate2D() {
	requestAnimationFrame(animate2D);
	render();
}

// same as 2D animate function except rotate the tube
function animate3D() {
	requestAnimationFrame(animate3D);
	group2.rotation.x += 0.004;
	render();
}

function render() {
	renderer.render(scene, camera);
}

function resetView() {
	controls.reset();
}

// REQUIRED for orbit controls
$("canvas").appendTo("#threejs-container");