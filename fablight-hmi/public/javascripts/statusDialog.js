/*
 * STATUS DIALOG JS
 * contains code for displaying a pop-up notice for the machine status, whether offline, paused, limit detect, etc.
 *
 * The data is parsed on the server, from fl_status that is sent by ExtraTech asynchronously over port 33.
 * On the server the data parsed and a fl_status__notice event is emitted by socket.io.
 * This event comes with a noticeObject parameter, an object which has the title and description which are updated on the dialog.
 *
 * On the client side, script must be loaded added AFTER JQuery import and socket.io.
 */

// to open: statusDialog.show();
// to close: statusDialog.close();

// Keep track of the door state
var doorOpen = false;

var statusDialogContent = $('#status-dialog__content');
var statusDialogHeader = $('#status-dialog__title');
var statusDialog = document.querySelector('#status-dialog');
var statusDialogCloseButton = $('#status-dialog__closeButton');
var $statusDialogActionButton = $('#status-dialog__actionButton');
if (!statusDialog.showModal) {
	dialogPolyfill.registerDialog(statusDialog);
}
statusDialog.querySelector('.close').addEventListener('click', function () {
	// if the dialog is open, then close it.
	if(statusDialog.open === true) {
		statusDialog.close();
	}
});

$(document).ready(function() {
	$statusDialogActionButton.hide();
	statusDialogCloseButton.show();
	socket.emit('fl_status');

	/*
	 * SOCKET.IO EVENTS
	 *
	 * fl_status__notice comes with an object
	 * { noticeTitle: <string> ,
	 *   desc: <string> }
	 */
	socket.on('fl_status__notice', function(noticeObj) {
		console.log('notice')
		if(noticeObj.noticeTitle !== '') {
			if(statusDialog.open === false) {
				statusDialog.showModal();
			}
			status_updateContent(noticeObj.desc);
			status_updateHeader(noticeObj.noticeTitle);
			console.log(noticeObj);

			if(noticeObj.tmp === true) {
				setTimeout(function() {
					if(statusDialog.open === true) {
						statusDialog.close();
					}
				}, 2000);
			}
		}
	});

	/* fl_alert - display an fl_alert command based on a noticeObj. If there's an action provided,
	 * then send that action back to the server via socket.io.
	 *
	 * noticeObj = {
			time: <str>,
			level: <str>,
			title: <str>,
			label: <str>,
			message: <HTML str>,
			cmd: <str>,
			action: {	// optional. only added if the user needs to perform some action
				name: <str>,
				command: <str>, 	// command to be emitted via socket.io when this button is pressed
				parameters: <str>
			}
		}
	 */
	socket.on('fl_alert', function(noticeObj) {
		socket.emit('fl_status');
		socket.emit('fl_p');
		console.log(noticeObj);
		if(noticeObj.title !== '') {
			// If there's an action in the fl_alert, then display it
			if(noticeObj.action !== undefined) {
				$statusDialogActionButton.text(noticeObj.action.name);
				$statusDialogActionButton.prop('disabled', false);
				$statusDialogActionButton.off();
				$statusDialogActionButton.on('click', function () {
					$statusDialogActionButton.prop('disabled', true);
					var params = noticeObj.action.parameters ? noticeObj.action.parameters : null;
					socket.emit(noticeObj.action.command, params);
				});
				$statusDialogActionButton.show();
			}
			else {
				$statusDialogActionButton.hide();
			}
			status_updateContent(noticeObj.message);
			status_updateHeader(noticeObj.title);
			if(statusDialog.open === false) {
				statusDialog.showModal();
			}
			// If the homing dialog is open, then hide it.
			if(typeof homingDialog !== "undefined") {
				if(homingDialog.open) {
					homingDialog.close();
				}
			}
		}
		else {
			console.log('No dialog shown.');
		}
	});

	socket.on('door-open', function () {
		console.log('door-open')
		doorOpen = true;
		if(typeof $doorStatus !== "undefined") {
			$doorStatus.text('Open');
		}
	});
	socket.on('door-closed', function () {
		console.log('door-closed')
		if(doorOpen) {
			if(statusDialog.open) {
				statusDialog.close();
			}
		}
		if(typeof $doorStatus !== "undefined") {
			$doorStatus.text('Closed');
		}
		doorOpen = false;
	});

	socket.on('machine_offline', function() {
		// If the machine is offline, go to the status page if you're not already there.
		if(window.location.pathname !== '/') {
			if(statusDialog.open === false) {
				statusDialog.showModal();
			}
			status_updateHeader('Machine is offline.');
			status_updateContent('');
			setTimeout(function () {
				window.location.href = '/';
			}, 1000);
		}
	});

	socket.on('machine_booted', function () {
		// If the machine is booted, go to the status page if you're not already there.
		if(window.location.pathname !== '/') {
			if(statusDialog.open === false) {
				statusDialog.showModal();
			}
			status_updateHeader('Machine is starting.');
			status_updateContent('Redirecting to the status page.');
			setTimeout(function () {
				window.location.href = '/';
			}, 1000);
		}
		if(typeof $machineStatus !== 'undefined') {
			$machineStatus.text('Controller started.');
		}
	});

	socket.on('udp-status-offline', function() {
		if(statusDialog.open === false) {
			statusDialog.showModal();
		}
		status_updateHeader('Machine is offline.');
		status_updateContent('');
		setTimeout(function () {
			if(window.location.pathname !== '/') {
				window.location.href = '/';
			}
		}, 1000);
	});
});

/*
 * HELPER FUNCTIONS
 */
function status_updateHeader(string) {
	statusDialogHeader.text(string);
}
function status_updateContent(string) {
	statusDialogContent.html('<p>' + string + '</p>');
}

function status_showButton() {
	statusDialogCloseButton.show();
}

function status_hideButton() {
	statusDialogCloseButton.hide();
}