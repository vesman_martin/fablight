SELECT *
FROM units, stock_types, materials, dimensions, processes, gases, laser_parameters
WHERE units.unit_id = stock_types.unit_id
AND stock_types.stock_type_id = materials.stock_type_id
AND materials.material_id = dimensions.material_id
AND dimensions.dimension_id = processes.dimension_id
AND processes.process_id = gases.process_id
AND gases.gas_id = laser_parameters.gas_id
AND units.unit_name = "IN"
AND stock_types.stock_type = "SHEET"
AND stock_types.tube_shape is NULL
AND materials.material_name = "BRASS"
AND materials.material_type = "206"
AND dimensions.thickness_value = 0.012
AND dimensions.diameter is NULL
AND dimensions.width is NULL
AND dimensions.height is NULL
AND dimensions.corner_radius is NULL
AND processes.process_name = "CUT"
AND processes.process_type IS NULL
AND gases.gas_name = "O2"