SELECT *
FROM units, stock_types, materials, dimensions, processes, gases, laser_parameters
WHERE units.unit_id = stock_types.unit_id
AND stock_types.stock_type_id = materials.stock_type_id
AND materials.material_id = dimensions.material_id
AND dimensions.dimension_id = processes.dimension_id
AND processes.process_id = gases.process_id
AND gases.gas_id = laser_parameters.gas_id