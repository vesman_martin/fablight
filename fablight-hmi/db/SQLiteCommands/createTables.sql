CREATE TABLE IF NOT EXISTS users (
    user_id         INTEGER PRIMARY KEY,
    user_name       TEXT
);

CREATE TABLE IF NOT EXISTS units (
    unit_id          INTEGER PRIMARY KEY,
    unit_name        TEXT
);
CREATE TABLE IF NOT EXISTS stock_types (
	stock_type_id	    INTEGER PRIMARY KEY,
	stock_type       TEXT DEFAULT "SHEET",
	tube_shape       TEXT,
	unit_id          INTEGER,
	FOREIGN KEY(unit_id) REFERENCES units(unit_id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS materials (
	material_id	    INTEGER PRIMARY KEY,
	user_id          INTEGER DEFAULT 1,
	material_name    TEXT,
	material_type    TEXT,
	stock_type_id     INTEGER,
	FOREIGN KEY(stock_type_id) REFERENCES stock_types(stock_type_id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS properties (
	property_id     INTEGER PRIMARY KEY,
	user_id          INTEGER DEFAULT 1,
	process_name    TEXT,
	process_type    TEXT DEFAULT "DEFAULT",
	thickness_value  REAL,
	diameter        REAL,
	width           REAL,
	height          REAL,
	corner_radius    REAL,
	material_id      INTEGER,
	FOREIGN KEY(material_id) REFERENCES materials(material_id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS gases (
	gas_id	        INTEGER PRIMARY KEY,
	user_id          INTEGER DEFAULT 1,
	gas_name         TEXT,
	property_id       INTEGER,
	FOREIGN KEY(property_id) REFERENCES properties(property_id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS laser_parameters (
	laser_parameter_id	INTEGER PRIMARY KEY,
	user_id          INTEGER DEFAULT 1,
	unit            TEXT,
    acceleration		REAL,
    feedrate		REAL,
    power		INTEGER,
    min_power	INTEGER,
    frequency		    INTEGER,
    pressure		INTEGER,
    gas_delay		INTEGER,
    nozzle_gap		REAL,
    focus_offset		REAL,
    lift_height		REAL,
    laser_mode		INTEGER,
    pierce_laser_mode   INTEGER,
    pierce_power		INTEGER,
    pierce_frequency		INTEGER,
    pierce_pulses	INTEGER,
    pierce_duration	INTEGER,
    pierce_nozzle_gap	REAL,
    pierce_pressure	INTEGER,
    pierce_gas_delay	INTEGER,
    kerf		REAL,
    gas_id           INTEGER,
    is_default      INTEGER DEFAULT 0,
	FOREIGN KEY(gas_id) REFERENCES gases(gas_id) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS machine_settings (
	setting_id	    INTEGER PRIMARY KEY,
	last_modified    TEXT,
	admin_password  TEXT,
	process_pin_enabled INT DEFAULT 0,
	process_pin      TEXT DEFAULT NULL,
	laser_power      INTEGER,
	gas             TEXT,
	serial_number    TEXT,
	model           TEXT,
	ip_address       TEXT,
	kerf        REAL,
	display_unit_system   TEXT
);
CREATE TABLE IF NOT EXISTS history (
	entry_id	    INTEGER PRIMARY KEY,
	last_modified    TEXT,
	user_id         INTEGER,
	job_name        TEXT,
	elapsed_time_s  INTEGER,
	errors          TEXT
);

REPLACE INTO users (user_id, user_name) VALUES (1, 'FACTORY'), (2, 'USER');

REPLACE INTO units (unit_id, unit_name) VALUES (1, 'IN'), (2, 'MM');

REPLACE INTO stock_types (stock_type_id, user_id, stock_type, tube_shape, unit_id) VALUES (
    1,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'SHEET',
    NULL,
    (SELECT unit_id FROM units WHERE unit_name LIKE "IN")
    ),
    (
    2,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'ROUND',
    (SELECT unit_id FROM units WHERE unit_name LIKE "IN")
    ),
    (
    3,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'SQUARE',
    (SELECT unit_id FROM units WHERE unit_name LIKE "IN")
    ),
    (
    4,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'RECTANGLE',
    (SELECT unit_id FROM units WHERE unit_name LIKE "IN")
    ),
    (
    5,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'SHEET',
    NULL,
    (SELECT unit_id FROM units WHERE unit_name LIKE "MM")
    ),
    (
    6,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'ROUND',
    (SELECT unit_id FROM units WHERE unit_name LIKE "MM")
    ),
    (
    7,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'SQUARE',
    (SELECT unit_id FROM units WHERE unit_name LIKE "MM")
    ),
    (
    8,
    (SELECT user_id FROM users WHERE user_name LIKE "FACTORY"),
    'TUBE',
    'RECTANGLE',
    (SELECT unit_id FROM units WHERE unit_name LIKE "MM")
    )
;