/* Store SQL strings and functions to create SQL strings, to be used everywhere in the app. */

module.exports = {

	kerf: null,
	
	get_display_unit: 'SELECT display_unit_system FROM machine_settings',

	create_machine_settings:
	'CREATE TABLE IF NOT EXISTS machine_settings ( ' +
	'setting_id	    INTEGER PRIMARY KEY, ' +
	'last_modified    TEXT, ' +
	'admin_password		TEXT, ' +
	'process_pin_enabled	INTEGER DEFAULT 0, ' +
	'process_pin      INTEGER DEFAULT NULL, ' +
	'laser_power      INTEGER, ' +
	'gas             TEXT, ' +
	'serial_number    TEXT, ' +
	'model           TEXT, ' +
	'ip_address       TEXT, ' +
	'kerf        REAL, ' +
	'display_unit_system	TEXT);'
	,

	create_laser_parameters:
	'CREATE TABLE laser_parameters ( ' +
	'laser_parameter_id	INTEGER PRIMARY KEY, ' +
	'user_id          INTEGER DEFAULT 1, ' +
	'unit            TEXT, ' +
	'acceleration		REAL, ' +
	'feedrate		REAL, ' +
	'power		INTEGER, ' +
	'min_power	INTEGER, ' +
	'frequency		    INTEGER, ' +
	'pressure		INTEGER, ' +
	'gas_delay		INTEGER, ' +
	'nozzle_gap		REAL, ' +
	'focus_offset		REAL, ' +
	'lift_height		REAL, ' +
	'laser_mode		INTEGER, ' +
	'pierce_laser_mode   INTEGER, ' +
	'pierce_power		INTEGER, ' +
	'pierce_frequency		INTEGER, ' +
	'pierce_pulses	INTEGER, ' +
	'pierce_duration	INTEGER, ' +
	'pierce_nozzle_gap	REAL, ' +
	'pierce_pressure	INTEGER, ' +
	'pierce_gas_delay	INTEGER, ' +
	'kerf		REAL, ' +
	'gas_id           INTEGER, ' +
	'is_default      INTEGER DEFAULT 0, ' +
	'FOREIGN KEY(gas_id) REFERENCES gases(gas_id) ON DELETE CASCADE;'
	,

	insert_laser_parameters_from_orig: 'INSERT INTO laser_parameters ( ' +
	'laser_parameter_id, ' +
	'user_id, ' +
	'acceleration, ' +
	'feedrate, ' +
	'power, ' +
	'min_power, ' +
	'frequency, ' +
	'pressure, ' +
	'gas_delay, ' +
	'nozzle_gap, ' +
	'focus_offset, ' +
	'lift_height, ' +
	'laser_mode, ' +
	'pierce_power, ' +
	'pierce_frequency, ' +
	'pierce_pulses, ' +
	'pierce_duration, ' +
	'pierce_nozzle_gap, ' +
	'pierce_pressure, ' +
	'pierce_gas_delay, ' +
	'gas_id, ' +
	'is_default) ' +
	'SELECT ' +
	'laser_parameter_id, ' +
	'user_id, ' +
	'acceleration, ' +
	'feedrate, ' +
	'power, ' +
	'min_power, ' +
	'frequency, ' +
	'pressure, ' +
	'gas_delay, ' +
	'nozzle_gap, ' +
	'focus_offset, ' +
	'lift_height, ' +
	'laser_mode, ' +
	'pierce_power, ' +
	'pierce_frequency, ' +
	'pierce_pulses, ' +
	'pierce_duration, ' +
	'pierce_nozzle_gap, ' +
	'pierce_pressure, ' +
	'pierce_gas_delay, ' +
	'gas_id, ' +
	'is_default FROM laser_parameters_orig;',
	
	get_machine_settings: 'SELECT * FROM machine_settings',

	get_units:
		'SELECT DISTINCT units.unit_name, units.unit_id FROM units'
	,
	get_users:
		'SELECT DISTINCT users.* FROM users'
	,
	get_stock_types:	// get stock type given unit id
	'SELECT DISTINCT stock_types.stock_type ' +
	'FROM stock_types ' +
	'WHERE stock_types.unit_id = $unit_id'
	,
	get_tube_shapes:	// get tube shape given stock type and unit id
	'SELECT DISTINCT stock_types.tube_shape ' +
	'FROM stock_types ' +
	'WHERE stock_types.unit_id = $unit_id ' +
	'AND stock_types.stock_type = $stock_type'
	,
	/**
	 *
	 * @param property_id
	 * @param gas_name
	 * @returns {string}
	 */
	get_laser_parameters: function(property_id, gas_name) {
		var SQL = 'SELECT laser_parameters.* ' + module.exports.full_joined_table +
			' WHERE properties.property_id IS ' + property_id + '' +
			' AND gases.gas_name IS \"' + gas_name + '\"';
		return SQL;
	},
	get_current_gas:
		'SELECT gas FROM machine_settings ORDER BY ROWID ASC LIMIT 1'
	,

	get_process_pin_settings:
		'SELECT process_pin, process_pin_enabled FROM machine_settings'
	,
	set_process_pin: function (newPin) {
		return 'UPDATE machine_settings SET process_pin = \'' + newPin + '\';';
	},
	enable_process_pin: function (enabled) {
		enabled = (enabled)? 1 : 0;	// Convert true/false to 1/0 for database
		return 'UPDATE machine_settings SET process_pin_enabled = \'' + enabled + '\';';
	},

	/* get stock type id
	 * param1: standard object (with keys processes, properties, gas).
	 * 	Properties used are
	 * 	{
	 * 		unit_name:
	 * 		stock_type:
	 * 		*tube_shape:
	 * 	}
	 */
	get_stock_type_id: function(obj) {
		var SQL = 'SELECT stock_type_id FROM stock_types ' +
			'WHERE stock_type IS \"' + obj.properties.stock_type +'\" '
		if(obj.properties.tube_shape != null) {
			SQL += 'AND tube_shape IS \"' + obj.properties.tube_shape + '\" '
		}
		SQL += 'AND unit_id IS ( ' +
			'SELECT unit_id FROM units ' +
			'WHERE unit_name IS \"' + obj.properties.unit_name + '\" ' +
			')';
		return SQL;
	},

	// To indicate a standard type in the database, process_ and material_ types can be these alternate values:
	standard_material_type: 'TYPICAL',
	standard_process_type: 'DEFAULT',

	/* check if material exists by trying to get the material_id
	 * @param1: standard process obj
	 * @param2: stock_type_id
	 */
	check_if_material_exists: function(obj, stock_type_id) {
		var SQL = 'SELECT material_id FROM materials ' +
			'WHERE material_name IS \"' + obj.properties.material_name + '\" ';
		if("material_type" in obj.properties) {
			if(obj.properties.material_type == module.exports.standard_material_type) {
				SQL += 'AND (material_type IS \"' + module.exports.standard_material_type + '\" ' +
					'OR material_type IS NULL)'
			}
			else {
				SQL += 'AND material_type IS \"' + obj.properties.material_type + '\" '
			}
		} else {
			SQL += 'AND (material_type IS NULL OR material_type IS ' +
				'\"' + module.exports.standard_material_type + '\") ';
		}
		SQL += 'AND stock_type_id IS ' + stock_type_id;
		return SQL;
	},

	/* insert into materials table
	 * @param1: standard obj
	 * @param2: stock_type_id
	 */
	insert_new_material_row: function(obj, stock_type_id) {
		var SQL = 'INSERT INTO materials (' +
			'user_id, ' +
			'material_name, ' +
			'material_type, ' +
			'stock_type_id) ' +
			'VALUES ((SELECT user_id FROM users WHERE user_name LIKE \"USER\"), ' +
			'\"' + obj.properties.material_name +  '\", ';
		if("material_type" in obj.properties) {
			if(obj.properties.material_type == null) {
				SQL += 'DEFAULT, '
			}
			else {
				SQL += '\"' + obj.properties.material_type + '\", '
			}
		} else {
			SQL += 'DEFAULT, '
		}
		SQL += stock_type_id + ');';
		return SQL;
	},

	/* Check if property exists using material_id and the processObj.
	 * Checks for dimensions AND process name & type.
	 * @param1: standard process obj
	 * @param2*: material_id
	 */
	check_if_property_exists: function(obj, material_id) {
		var SQL = 'SELECT property_id from properties WHERE ';
		// Only look up thickness_value if it's a CUT process. ENGRAVE and RASTER don't use this for lookup.
		if(obj.processes[0].name == "CUT") {
			SQL += 'thickness_value IS ' + obj.properties.thickness_value + ' AND ';
			// If TUBE, try to match the dimension fields including corner radius for square and round tubes.
			if(obj.properties.stock_type == 'TUBE') {
				if(obj.properties.tube_shape == 'ROUND') {
					SQL += 'diameter IS ' + obj.properties.diameter + ' ' +
						'AND width IS NULL ' +
						'AND height IS NULL ' +
						'AND corner_radius IS NULL ';
				}
				else { // Rect and square.
					SQL += 'diameter IS NULL ' +
						'AND width IS ' + obj.properties.width + ' ' +
						'AND height IS ' + obj.properties.height + ' ' +
						'AND corner_radius IS ' + obj.properties.corner_radius + ' ';
				}
			}
			else { // If SHEET, ignore the dimension fields.
				SQL += 'diameter IS NULL ' +
					'AND width IS NULL ' +
					'AND height IS NULL ' +
					'AND corner_radius IS NULL ';
			}
			SQL += 'AND '
		}

		SQL += 'process_name IS \"' + obj.processes[0].name + '\" ';

		// process_type needs to be handled differently. A value of "DEFAULT" is the same as NULL.
		if("type" in obj.processes[0]) {
			if(obj.processes[0].type == module.exports.standard_process_type) {
				SQL += ' AND (process_type IS \"' + module.exports.standard_process_type + '\" ' +
					'OR process_type IS NULL) '
			}
			else {
				SQL += ' AND process_type IS \"' + obj.processes[0].type + '\" '
			}
		}
		else {
			SQL += ' AND (process_type IS NULL OR process_type IS ' +
				'\"' + module.exports.standard_process_type + '\") ';
		}

		SQL += 'AND material_id IS ' + material_id;
		return SQL;
	}
	,

	/* insert into properties table
	 * @param1: standard obj
	 * @param2: material_id
	 */
	insert_new_property_row: function(obj, material_id) {
		/* user_id
		 * process_name
		 * process_type
		 * thickness_value
		 * diameter
		 * width
		 * height
		 * corner_radius
		 * material_id
		 */
		var SQL = 'INSERT INTO properties (' +
			'user_id,' +
			'process_name,' +
			'process_type,' +
			'thickness_value, ' +
			'diameter, ' +
			'width, ' +
			'height, ' +
			'corner_radius, ' +
			'material_id) VALUES (' +
			'(SELECT user_id FROM users WHERE user_name LIKE "USER"), ';	// user_id
		// process_name, process_type:
		SQL+= '\"' + obj.processes[0].name+ '\", ';
		if("type" in obj.processes[0]) {
			if(obj.processes[0].type == null) {
				SQL += '\"DEFAULT\", ';
			}
			else {
				SQL += '\"' + obj.processes[0].type + '\", ';
			}
		}
		else {
			SQL += '\"DEFAULT\", ';
		}
		// thickness - only add if CUT process. Add a 0.0 otherwise.
		if(obj.processes[0].name == "CUT") {
			SQL += obj.properties.thickness_value + ', ';
		}
		else {
			SQL += '0.0, ';
		}

		//diameter, width, height, corner_radius
		if(obj.properties.stock_type == 'TUBE') {
			if(obj.properties.tube_shape == 'ROUND') {
				SQL += obj.properties.diameter + ', NULL, NULL, NULL, ';
			}
			else { // square or rectangle
				SQL += 'NULL, ' +
					obj.properties.width + ', ' +
					obj.properties.height + ', ' +
					obj.properties.corner_radius + ', ';
			}
		} else {
			// sheet, so other dims are null
			SQL += 'NULL, NULL, NULL, NULL, '
		}
		SQL += material_id + ');';	// material_id
		return SQL;
	},

	/* Check if gas exists using process_id and the processObj
	 * @param1: standard process obj
	 * @param2: property_id
	 * ex)
	 SELECT * FROM gases
	 WHERE property_id IS 21
	 AND (gas_name IS "AIR" OR gas_name IS NULL)
	 AND (user_id IS NULL OR user_id IS 0)
	 */
	check_if_gas_exists: function(obj, property_id) {
		var SQL = 'SELECT * FROM gases ' +
			'WHERE property_id IS ' + property_id + ' ' +
			'AND ((gas_name IS \"' + obj.gas + '\" AND user_id IS NOT 0) OR ' +
			'(gas_name IS NULL AND user_id IS 0))';
		return SQL;
	},

	/** Insert a new gas row given a process_id.
	 * @param {object} obj - standard obj with a single processes[0] obj
	 * @param {int} process_id
	 */
	insert_new_gas_row: function(obj, process_id) {
		return 'INSERT INTO gases (' +
			'user_id, ' +
			'gas_name, ' +
			'property_id) VALUES (' +
			'(SELECT user_id FROM users WHERE user_name LIKE "USER"), ' +
			'\"' + obj.gas + '\", ' +
			process_id + ');';
	},

	/* insert a laser parameters row
	 * @param1: standard obj with a single processes[0] obj
	 * @param2: gas_id
	 *
	 * column order:
	 laser_parameter_id,
	 user_id,
	 acceleration,
	 feedrate,
	 power,
	 min_power,
	 frequency,
	 pressure,
	 gas_delay,
	 nozzle_gap,
	 focus_offset,
	 lift_height,
	 laser_mode,
	 pierce_laser_mode,
	 pierce_power,
	 pierce_frequency,
	 pierce_pulses,
	 pierce_duration,
	 pierce_nozzle_gap,
	 pierce_pressure,
	 pierce_gas_delay,
	 kerf,
	 gas_id,
	 is_default
	 */
	insert_laser_parameters_row: function(obj, gas_id) {
		var currentParams = obj.processes[0].params;
		for(var param in currentParams) {
			if(currentParams[param] == ""){
				currentParams[param] = null;
			}
		}
		// Acceleration and kerf need a default value:
		if("acceleration" in currentParams) {
			if(currentParams.acceleration == null) {
				currentParams.acceleration = 1;
			}
		} else {
			currentParams.acceleration = 1;
		}
		if("kerf" in currentParams) {
			if(currentParams.kerf == null) {
				currentParams.kerf = module.exports.kerf;
			}
		} else {
			currentParams.kerf = module.exports.kerf;
		}
		// Laser mode needs a default if it isn't set:
		if("laser_mode" in currentParams) {
			if(currentParams.laser_mode == null) {
				currentParams.laser_mode = 0;
			}
		}
		// Raster doesn't have a pierce so set the duration to 0:
		if(obj.processes[0].name == "RASTER") {
			currentParams.pierce_duration = 0;
		}
		var SQL = 'INSERT INTO laser_parameters (' +
			'user_id, ' +
			'unit, ' +
			'acceleration, ' +
			'feedrate, ' +
			'power, ' +
			'min_power, ' +
			'frequency, ' +
			'pressure, ' +
			'gas_delay, ' +
			'nozzle_gap, ' +
			'focus_offset, ' +
			'lift_height, ' +
			'laser_mode, ' +
			'pierce_laser_mode, ' +
			'pierce_power, ' +
			'pierce_frequency, ' +
			'pierce_pulses, ' +
			'pierce_duration, ' +
			'pierce_nozzle_gap, ' +
			'pierce_pressure, ' +
			'pierce_gas_delay, ' +
			'kerf, ' +
			'gas_id, ' +
			'is_default' +
			') VALUES (' +
			'2, ' +	// HARDCODED user_id
			'(SELECT display_unit_system FROM machine_settings), ' +	// insert the current machine unit
			(("acceleration" in currentParams) ? currentParams.acceleration : 1) + ', ' +
			currentParams.feedrate + ', ' +
			currentParams.power + ', ' +
			currentParams.min_power + ', ' +
			currentParams.frequency + ', ' +
			currentParams.pressure + ', ' +
			(("gas_delay" in currentParams) ? currentParams.gas_delay : null) + ', ' +
			currentParams.nozzle_gap + ', ' +
			currentParams.focus_offset + ', ' +
			(("lift_height" in currentParams) ? currentParams.lift_height : null) + ', ' +
			// if laser mode is not set, default to 0:
			(("laser_mode" in currentParams) ? currentParams.laser_mode : 0) + ', ' +
			(("pierce_laser_mode" in currentParams) ? currentParams.pierce_laser_mode : null) + ', ' +
			(("pierce_power" in currentParams) ? currentParams.pierce_power : null) + ', ' +
			(("pierce_frequency" in currentParams) ? currentParams.pierce_frequency : null) + ', ' +
			(("pierce_pulses" in currentParams) ? currentParams.pierce_pulses : null) + ', ' +
			(("pierce_duration" in currentParams) ? currentParams.pierce_duration : null) + ', ' +
			(("pierce_nozzle_gap" in currentParams) ? currentParams.pierce_nozzle_gap : null) + ', ' +
			(("pierce_pressure" in currentParams) ? currentParams.pierce_pressure : null) + ', ' +
			(("pierce_gas_delay" in currentParams) ? currentParams.pierce_gas_delay : null) + ', ' +
			(("kerf" in currentParams) ? currentParams.kerf : module.exports.kerf) + ', ' +
			gas_id + ', ' +
			1 +	// is_default
			');';
		return SQL;
	},

	/** Given a gas_id and a process object, update the laser_parameters row given a gas_id.
	 * This is different from update_laser_parameter_row because that function requires a laser_parameter_id.
	 *
	 * IMPORTANT: Assumes that there is only one laser_parameter_row for a single gas_id.
	 *
	 * @param obj
	 * @param gas_id
	 * @returns {string}
	 */
	update_laser_parameters_row: function(obj, gas_id) {
		var p = obj.processes[0].params;
		var SQL = 'UPDATE laser_parameters SET' +
			' user_id = 2,' +	// HARDCODED to 2
			' feedrate = ' + p.feedrate + ',' +
			' acceleration = ' + (("acceleration" in p) ? p.acceleration : 1) + ', ' +
			' power = ' + p.power + ',' +
			' min_power = ' + p.min_power + ',' +
			' frequency = ' + p.frequency + ',' +
			' pressure = ' + p.pressure + ',' +
			' gas_delay = ' + (("gas_delay" in p) ? p.gas_delay : 'NULL') + ',' +
			' nozzle_gap = ' + p.nozzle_gap + ',' +
			' focus_offset = ' + p.focus_offset + ',' +
			' lift_height = ' + (("lift_height" in p) ? p.lift_height : 'NULL') + ',' +
			' laser_mode = ' + (("laser_mode" in p) ? p.laser_mode : 0) + ',' +
			' pierce_laser_mode = ' + (("pierce_laser_mode" in p) ? p.pierce_laser_mode : 'NULL') + ',' +
			' pierce_power = ' + (("pierce_power" in p) ? p.pierce_power : 'NULL') + ',' +
			' pierce_frequency = ' + (("pierce_frequency" in p) ? p.pierce_frequency : 'NULL') + ',' +
			' pierce_pulses = ' + (("pierce_pulses" in p) ? p.pierce_pulses : 'NULL') + ',' +
			' pierce_duration = ' + (("pierce_duration" in p) ? p.pierce_duration : 'NULL') + ',' +
			' pierce_nozzle_gap = ' + (("pierce_nozzle_gap" in p) ? p.pierce_nozzle_gap : 'NULL') + ',' +
			' pierce_pressure = ' + (("pierce_pressure" in p) ? p.pierce_pressure : 'NULL') + ',' +
			' pierce_gas_delay = ' + (("pierce_gas_delay" in p) ? p.pierce_gas_delay : 'NULL') + ',';
		if(p.kerf != null) {
			SQL += ' kerf = ' + p.kerf + ',';
		}
		else { // Use kerf provided in machine_settings
			SQL +=' kerf = ' + module.exports.kerf + ','
		}
		SQL += ' is_default = ' + (("is_default" in p) ? p.is_default : 1) +
			' WHERE gas_id = ' + gas_id + '\;';
		return SQL;
	},

	/** Update a laser parameter row, based on a laser_parameter_id.
	 *
	 * @param1 {obj} p - object containing the entire row's data as a key, value pair.
	 */
	update_laser_parameter_row_from_params: function(p) {
		if(p.acceleration == null) {
			p.acceleration = 1;
		}
		if(p.laser_mode == null) {
			p.laser_mode = 0;
		}
		if(p.pierce_laser_mode == null) {
			p.pierce_laser_mode = p.laser_mode;
		}
		var SQL = 'UPDATE laser_parameters SET' +
			' user_id = ' + p.user_id + ',' +
			' unit = \"' + p.unit + '\",' +
			' feedrate = ' + p.feedrate + ',' +
			' acceleration = ' + p.acceleration + ',' +
			' power = ' + p.power + ',' +
			' min_power = ' + p.min_power + ',' +
			' frequency = ' + p.frequency + ',' +
			' pressure = ' + p.pressure + ',' +
			' gas_delay = ' + p.gas_delay + ',' +
			' nozzle_gap = ' + p.nozzle_gap + ',' +
			' focus_offset = ' + p.focus_offset + ',' +
			' lift_height = ' + p.lift_height + ',' +
			' laser_mode = ' + p.laser_mode + ',' +
			' pierce_laser_mode = ' + p.pierce_laser_mode + ',' +
			' pierce_power = ' + p.pierce_power + ',' +
			' pierce_frequency = ' + p.pierce_frequency + ',' +
			' pierce_pulses = ' + p.pierce_pulses + ',' +
			' pierce_duration = ' + p.pierce_duration + ',' +
			' pierce_nozzle_gap = ' + p.pierce_nozzle_gap + ',' +
			' pierce_pressure = ' + p.pierce_pressure + ',' +
			' pierce_gas_delay = ' + p.pierce_gas_delay + ',';
		if(p.kerf != null) {
			SQL += ' kerf = ' + p.kerf + ',';
		}
		SQL += ' gas_id = ' + p.gas_id + ',' +
			' is_default = ' + p.is_default +
			' WHERE laser_parameter_id = ' + p.laser_parameter_id + ' ' +
			'AND user_id = 2\;';
		return SQL;
	},

	/* fully joined table using INNER JOIN */
	full_joined_table: 'FROM units ' +
	'INNER JOIN stock_types ' +
	'ON stock_types.unit_id=units.unit_id ' +
	'INNER JOIN materials ' +
	'ON materials.stock_type_id=stock_types.stock_type_id ' +
	'INNER JOIN properties ' +
	'ON properties.material_id = materials.material_id ' +
	'INNER JOIN gases ' +
	'ON gases.property_id = properties.property_id ' +
	'INNER JOIN laser_parameters ' +
	'ON laser_parameters.gas_id = gases.gas_id ',

	/*
	 * @param1: process <obj>
	 * @param2: currentProcess <obj> single process
	 */
	get_suggested_material_types: function(pObj, currentProcessName) {
		var SQL = 'SELECT DISTINCT materials.material_type ' + module.exports.full_joined_table +
			'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
			'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
			'AND properties.process_name IS \"' + currentProcessName + '\" ' +
			'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
			'AND gases.gas_name IS \"' + pObj.gas + '\";';
		return SQL;
	},

	/*
	 * @param1: process <obj>
	 * @param2: currentProcess <obj> single process
	 * @param3: materialType <str>
	 */
	get_suggested_tube_shapes: function(pObj, currentProcessName, materialType) {
		var SQL = 'SELECT DISTINCT stock_types.tube_shape ' + module.exports.full_joined_table +
			'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
			'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
			'AND properties.process_name IS \"' + currentProcessName + '\" ' +
			'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
			'AND gases.gas_name IS \"' + pObj.gas + '\" '
		if(materialType == null) {
			SQL += 'AND materials.material_type IS '+ materialType;
		}
		else if(materialType == module.exports.standard_material_type) {
			SQL += 'AND (materials.material_type IS \"'+ materialType + '\" OR ' +
				'materials.material_type IS NULL) ';
		}
		else {
			SQL += 'AND materials.material_type IS \"'+ materialType + '\"';
		}
		return SQL;
	},

	/* @param1: process <obj>
	 * @param2: currentProcess <obj> single process
	 * @param3: materialType <str>
	 * @param4: tubeShape <str>
	 */
	get_suggested_dimensions: function(pObj, currentProcessName, materialType, tubeShape) {
		var SQL = 'SELECT DISTINCT properties.* ' + module.exports.full_joined_table +
			'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
			'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
			'AND properties.process_name IS \"' + currentProcessName + '\" ' +
			'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
			'AND gases.gas_name IS \"' + pObj.gas + '\" ' +
			'AND stock_types.tube_shape IS \"'+ tubeShape + '\" ';
		if(materialType == null) {
			SQL += 'AND materials.material_type IS '+ materialType;
		}
		else if(materialType == module.exports.standard_material_type) {
			SQL += 'AND (materials.material_type IS \"'+ materialType + '\" OR ' +
				'materials.material_type IS NULL) ';
		}
		else {
			SQL += 'AND materials.material_type IS \"'+ materialType + '\"';
		}
		return SQL;
	},

	/* @param1: process <obj>
	 * @param2: currentProcess <obj> single process
	 * @param3: materialType <str>
	 * @param4: tubeShape <str>
	 * @param5: property_id <int>
	 */
	get_suggested_thicknesses: function(pObj, currentProcessName, materialType, tubeShape, property_id) {
		var SQL;
		if(tubeShape) {
			SQL = 'SELECT DISTINCT properties.thickness_value ' + module.exports.full_joined_table +
				'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
				'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
				'AND properties.process_name IS \"' + currentProcessName + '\" ' +
				'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
				'AND gases.gas_name IS \"' + pObj.gas + '\" ' +
				'AND stock_types.tube_shape IS \"'+ tubeShape + '\" ' +
				'AND properties.property_id IS '+ property_id + ' ';
			if(materialType == null) {
				SQL += 'AND materials.material_type IS '+ materialType;
			}
			else if(materialType == module.exports.standard_material_type) {
				SQL += 'AND (materials.material_type IS \"'+ materialType + '\" OR ' +
					'materials.material_type IS NULL) ';
			}
			else {
				SQL += 'AND materials.material_type IS \"'+ materialType + '\"';
			}
			return SQL;
		}
		else {
			SQL = 'SELECT DISTINCT properties.thickness_value ' + module.exports.full_joined_table +
				'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
				'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
				'AND properties.process_name IS \"' + currentProcessName + '\" ' +
				'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
				'AND gases.gas_name IS \"' + pObj.gas + '\" ';
			if(materialType == null) {
				SQL += 'AND materials.material_type IS '+ materialType;
			}
			else if(materialType == module.exports.standard_material_type) {
				SQL += 'AND (materials.material_type IS \"'+ materialType + '\" OR ' +
					'materials.material_type IS NULL) ';
			}
			else {
				SQL += 'AND materials.material_type IS \"'+ materialType + '\"';
			}
			return SQL;
		}
	},

	/* @param1: process <obj>
	 * @param2: currentProcess <obj> single process
	 * @param3: materialType <str>
	 * @param4: tubeShape <str>
	 * @param5: property_id <int>	// if it's a sheet job property_id may be undefined
	 * @param6: thickness_value <int>
	 */
	get_suggested_processes: function(pObj, currentProcessName, materialType, tubeShape, property_id, thickness_value) {
		var SQL = 'SELECT DISTINCT properties.* ' + module.exports.full_joined_table +
			'WHERE materials.material_name IS \"' + pObj.properties.material_name + '\" ' +
			'AND stock_types.stock_type IS \"' + pObj.properties.stock_type + '\" ' +
			'AND properties.process_name IS \"' + currentProcessName + '\" ' +
			'AND units.unit_name IS \"' + pObj.properties.unit_name + '\" ' +
			'AND gases.gas_name IS \"' + pObj.gas + '\" ';
		// If the current process is CUT then include thickness. Otherwise put a thickness of 0.
		if(currentProcessName == "CUT") {
			SQL += 'AND properties.thickness_value IS '+ thickness_value + ' ';
		}
		else {
			SQL += 'AND properties.thickness_value IS 0 ';
		}
		// Only include the property_id if it's defined
		if(typeof property_id != 'undefined'){
			SQL += 'AND properties.property_id IS '+ property_id + ' '
		}
		if(materialType == null) {
			SQL += 'AND materials.material_type IS '+ materialType + ' ';
		}
		else if(materialType == module.exports.standard_material_type) {
			SQL += 'AND (materials.material_type IS \"'+ materialType + '\" OR ' +
				'materials.material_type IS NULL) ';
		}
		else {
			SQL += 'AND materials.material_type IS \"'+ materialType + '\" ';
		}

		if(typeof tubeShape != 'undefined') {
			SQL += 'AND stock_types.tube_shape IS \"'+ tubeShape + '\" '
		}
		else {
			SQL += 'AND stock_types.tube_shape IS NULL '
		}
		return SQL;
	},

	/* Given a process object, generate the SQL string used to get its parameters.
	 * This has to be a function because not all of the db columns are used.
	 * If they are not in the process object, SQL syntax must use a IS NULL,
	 * instead of an = null.
	 *
	 * Also only returns parameters if the gas_id is NOT 0. A 0 indicates a new gas added
	 * from fabcreator.
	 *
	 * CUT uses dimensions to look up the process.
	 * ENGRAVE and RASTER do not.
	 *
	 * @param1: process <obj> complete process object
	 * @param2: currentProcess <obj> single process, since process can contain multiple.
	 */
	get_process_parameter_list: function(process, currentProcess) {
		// mandatory DB fields to look up
		var SQL = 'SELECT laser_parameters.* ' +
			'FROM units, stock_types, materials, properties, gases, laser_parameters ' +
			'WHERE units.unit_id = stock_types.unit_id ' +
			'AND stock_types.stock_type_id = materials.stock_type_id ' +
			'AND materials.material_id = properties.material_id ' +
			'AND properties.property_id = gases.property_id ' +
			'AND gases.gas_id = laser_parameters.gas_id ' +
			'AND units.unit_name = \"' + process.properties.unit_name + '\" ' +
			'AND stock_types.stock_type = \"' + process.properties.stock_type + '\" ' +
			'AND materials.material_name = \"' + process.properties.material_name + '\" ' +
			'AND properties.process_name = \"' + currentProcess.name + '\" ' +
			'AND gases.gas_name = \"' + process.gas + '\" ' +
			'AND gases.user_id != 0 ';	// If a new process is added by fabcreator then the user_id will be 0.

		// Material type is added to the look up if it exists. It can be "TYPICAL"/NULL, or an actual value.
		if("material_type" in process.properties) {
			// fail-safe: If the material_type is "TYPICAL", also look for a NULL entry in the DB.
			if(process.properties.material_type == module.exports.standard_material_type) {
				SQL += 	' AND (materials.material_type IS NULL OR materials.material_type IS ' +
					'\"' + module.exports.standard_material_type + '\") ';
			}
			else {
				// If the material is not "TYPICAL" just use it's value.
				SQL += ' AND materials.material_type IS \"' + process.properties.material_type + '\" ';
			}
		} else {
			// The material_type is not in the properties, so look for a NULL or "TYPICAL" value.
			SQL += 	' AND (materials.material_type IS NULL OR materials.material_type IS ' +
				'\"' + module.exports.standard_material_type + '\") ';
		}

		// Process type follows the same rules as material_type, except it can be "DEFAULT" or NULL.
		if("type" in currentProcess) {
			// IF it's DEFAULT, then also look for NULL.
			if(currentProcess.type == module.exports.standard_process_type) {
				SQL += 	'AND (properties.process_type IS NULL OR properties.process_type IS ' +
					'\"' + module.exports.standard_process_type + '\") ';
			}
			else {
				SQL += ' AND properties.process_type IS \"' + currentProcess.type + '\" ';
			}
		} else {
			// Proces type is not in the properties, so look for NULL or "DEFAULT"
			SQL += 	'AND (properties.process_type IS NULL OR properties.process_type IS ' +
				'\"' + module.exports.standard_process_type + '\") ';
		}

		SQL += 'AND stock_types.tube_shape ';
		if("tube_shape" in process.properties) {
			SQL += 	' = \"' + process.properties.tube_shape + '\" ';
		} else {
			SQL += 	' IS NULL ';
		}

		// Only use the dimension to look up the params if it's a CUT process.
		if(currentProcess.name == "CUT") {
			SQL += 'AND properties.thickness_value = ' + process.properties.thickness_value + ' ';

			SQL += 'AND properties.diameter ';
			if("diameter" in process.properties) {
				SQL += ' = ' + process.properties.diameter + ' ';
			} else {
				SQL += 	' IS NULL ';
			}

			SQL += 'AND properties.width';
			if("width" in process.properties) {
				SQL += ' = ' + process.properties.width + ' ';
			} else {
				SQL += 	' IS NULL ';
			}

			SQL += 'AND properties.height';
			if("height" in process.properties) {
				SQL += ' = ' + process.properties.height + ' ';
			} else {
				SQL += 	' IS NULL ';
			}
			// Also check for corner radius if it is provided or set to NULL (sheet or round tube).
			SQL += 'AND properties.corner_radius';
			if("corner_radius" in process.properties) {
				SQL += ' = ' + process.properties.corner_radius + ' ';
			} else {
				SQL += 	' IS NULL ';
			}
		}
		return SQL;
	},

	/** Update a gas row given the gas_id.
	 * @param {object} obj - standard obj with a single processes[0] obj
	 * @param {int} gas_id
	 */
	update_gas_row: function(obj, gas_id) {
		var SQL = 'UPDATE gases SET ' +
			'user_id = 2, ' +
			'gas_name = \"' + obj.gas + '\" ' +
			'WHERE gas_id = ' + gas_id;
		return SQL;
	},

	/** Given a laser_parameter_id, set this row to be either default or not, using a boolean.
	 *
	 * @param {int} laser_parameter_id
	 * @param {boolean} make_default
	 * @returns {string} - SQL command to update the row as described.
	 */
	set_laser_parameter_row_to_default: function(laser_parameter_id, make_default) {
		return 'UPDATE laser_parameters SET is_default=' + (make_default? 1 : 0) +
			' WHERE laser_parameter_id IS ' + laser_parameter_id;
	}
};