/*
 * dbManager.js
 *
 * stores database so other routes can access it.
 */

var dbManager;

const path = require('path');
var fs = require('fs');
var config = require('./config');
const util = require('util');
var db;
var sqlite3 = require('sqlite3').verbose();
var moment = require('moment');
var appRoot = require('app-root-path');
var usbManager = appRoot.require('usbManager');
var SQLcommands = appRoot.require(path.join('db', 'SQLiteCommands', 'SQLiteCommands'));
var logger = appRoot.require('logger');

module.exports = {

	// Store the database here for use elsewhere in the application.
	db: db,

	// get the current DB path
	dbPath: function(callback) {
		module.exports.getDBname(function(err, dbName) {
			if(err !== null) {
				callback(err);
			}
			else {
				callback(null, path.normalize(path.join(__dirname, 'db', dbName)));
			}
		});
	},
	
	/** Find the name of the .db file in the /db folder. If there is more than one file here, just pick
	 * the first one you find. Likely alphabetical. Callback has one parameter, the name of the DB.
	 *
	 * @param callback - one parameter. <name of db in /db folder>
	 */
	getDBname: function(callback) {
		var mountpoint = path.normalize(path.join(__dirname, 'db'));
		fs.readdir(mountpoint, function(err, itemArray) {
			if(err) throw err;

			// filter the results to only show the files (not folders)
			var filesList = [];
			var itemProcessed = 0;
			itemArray.forEach(function(itemName, index) {
				var itemPath = path.normalize(mountpoint + '/' + itemName);
				// need to use a sync function to guarantee completion before callback called
				var itemStat = fs.stat(itemPath, function(err, stats) {
					if(err) {
						throw err;
					}
					else {
						if(stats.isDirectory() == false) {
							// if the file is a fab
							var fileExtension = path.extname(itemPath).toLowerCase();
							if(fileExtension == '.db') {
								filesList.push(itemName);
							}
						}
						itemProcessed++;
						if(itemProcessed === itemArray.length) {
							if(filesList.length == 0) {
								callback('No DB File Exists', null);
							}
							else {
								callback(null, filesList[0]);
							}
						}
					}
				});
			});
		})
	},

	/** Upload the database from the root of a USB drive to the app.
	 * Make sure the machine_settings table matches machine_settings.json.
	 *
	 * @param usbFileName
	 * @param callback(<err>)
	 */
	uploadDB: function(usbFileName, callback) {
		var db = module.exports.db;
		// check for removable USB inserted
		usbManager.getUSBList(function(err, usb, mountpoint) {
			if(err) { callback(err) }
			else {
				// check if the USB has the requested db file in it
				var usbDBfilePath = path.join(mountpoint, usbFileName);
				fs.stat(usbDBfilePath, function(err, stat) {
					if(err) { // The DB file was not found.
						callback('The DB file was not found on the USB.');
					}
					else {
						// the file is on USB. Try closing existing DB.
						module.exports.closeDB(db, function(err) {
							if(err) {
								callback('Error closing DB file.');
							}
							else {
								// rename the existing DB (eventually we delete it)
								module.exports.dbPath(function(err, existingDBfilePath) {
									var existingDBfileRenamed = path.join(__dirname, 'db', 'backup', path.basename(existingDBfilePath, '.db'));
									fs.rename(existingDBfilePath, existingDBfileRenamed, function(err) {
										if(err) {
											callback('Error renaming DB file.');
										}
										else {
											// now copy new DB file from USB to app. source, target, callback
											var targetPath = path.join(__dirname, 'db', usbFileName)
											usbManager.copyFile(usbDBfilePath, targetPath, function(err) {
												if(err) {
													callback('Error copying DB file.');
												}
												else {
													// Open the newly copied database and create dbConnection to it
													module.exports.openDBatPath(targetPath, function(err, dbConnection) {
														if(err) {
															callback('Error opening DB file.');
														}
														// Check the machine_settings table and laser_parameters table for this new db
														module.exports.check_machine_settings(dbConnection, function() {
															module.exports.verify_laser_parameters_schema(dbConnection, function (err) {
																if(err) {
																	logger.log('[uploadDB] DB must be reprocessed in fabcreator. Wrong schema.');
																	// New DB copied from USB IS NOT THE RIGHT FORMAT.
																	// Close and delete this new DB.
																	module.exports.closeDB(dbConnection, function (err) {
																		if(err) {
																			callback('Error closing copied DB.');
																		}
																		else {
																			fs.unlink(targetPath, function(err) {
																				if(err) {
																					callback('Error deleting copied DB.');
																				}
																				else {
																					// Also rename the existing DB and open it.
																					fs.rename(existingDBfileRenamed, existingDBfilePath, function(err) {
																						if(err) {
																							callback('Error renaming existing DB backup.');
																						}
																						else {
																							module.exports.openDB(function (err) {
																								if(err) {
																									callback('Error re-opening DB.');
																								}
																								else {
																									// We successfully re-opened the existing DB.
																									// Need to tell the user to reprocess the DB in fabcreator.
																									callback('Database must be reprocessed in fabcreator.');
																								}
																							})
																						}
																					});
																				}
																			});
																		}
																	});
																}
																else {
																	// If all checks out, FINALLY delete the backup
																	fs.unlink(existingDBfileRenamed, function(err) {
																		if(err) {
																			callback('Error deleting DB backup.');
																		}
																		else {
																			// set the stored db to this new db
																			module.exports.db = dbConnection;
																			callback(null);	// don't send any error
																		}
																	});
																}
															});
														});
													});
												}
											})
										}

									})
								});
							}
						})
					}
					
				})
			}
		})
	},

	/* downloadDB: used to download the database from the app to a USB drive.
	 *
	 * @param1: callback(<err>, <string of DB name>)
	 * 
	 * Callback after completion or with error.
	 * First parameter of callback is an error. Null if no error.
	 */
	downloadDB: function(callback) {
		var db = module.exports.db;
		// first check if there is a removable USB inserted.
		usbManager.getUSBList(function(err, usb, mountpoint) {
			if(err) {
				callback(err, null);
			}
			else {
				// USB was found, close DB on server
				module.exports.closeDB(db, function(err) {
					if(err) {
						callback(err, null);
					}
					else {
						// copy file to plugged in USB
						module.exports.dbPath(function(err, dbPath) {
							// check if DB file exists
							fs.stat(dbPath, function(err, stat) {
								if(err) {
									callback(err, null);
								}
								else {
									var currentDBname = path.basename(dbPath);
									var timeStamp = moment().format('YYYY-MM-DD')
									var downloadedDBname;
									// If the current DB name already has the _download_YYYY-MM-DD extension
									// then just update the date instead of re-appending the entire extension.
									// This prevents getting a file with _download_YYYY-MM-DD_download_YYYY-MM-DD...etc...
									var downloadPattern = /_download_(\d\d\d\d-\d\d-\d\d)\.db/;
									if(currentDBname.match(downloadPattern)) {
										downloadedDBname = currentDBname.replace(downloadPattern, '_download_' + timeStamp + '.db');
									}
									else {
										downloadedDBname = path.basename(currentDBname, '.db') + '_download_' + timeStamp  +'.db';
									}
									var usbDBpath = path.join(mountpoint, downloadedDBname);
									// source, target, callback
									usbManager.copyFileToUSB(dbPath, usbDBpath, function(err) {
										if(err) {
											callback(err, null);
										}
										else {
											// finally, re-open the DB in the app
											module.exports.openDB(function(err) {
												if(err) {
													callback(err, null);
												}
												callback(null, downloadedDBname);
											});
										}
									});
								}
							})
						});
					}
				})
			}
		});
	},

	/* closeDB: Close the database
	 *
	 * @param1: db object
	 * @param2: callback(<err or null>)
	 */
	closeDB: function(db, cb) {
		db.close(function(err) {
			if(err) {
				cb(err);
			}
			else {
				cb(null);
			}
		});
	},

	openDB: function(cb) {
		module.exports.dbPath(function(err, dbPath) {
			var dbConnection = new sqlite3.Database(dbPath,sqlite3.OPEN_READWRITE, function(err) {
				if(err) {
					cb(err);
				}
				module.exports.db = dbConnection;
				cb(null);
			});
		});
	},

	/** Given a DB path, open the database.
	 * NOTE: Callback has the form (err, dbConnection) where dbConnection is a sqlite3 database connection.
	 *
	 * @param dbPath
	 * @param cb(err, dbConnection)
	 */
	openDBatPath: function(dbPath, cb) {
		var dbConnection = new sqlite3.Database(dbPath,sqlite3.OPEN_READWRITE, function(err) {
			if(err) {
				cb(err);
			}
			cb(null, dbConnection);
		});
	},

	/* getParamOptions - find all entries for a certain process.
	 * Useful for seeing if a process exists to allow the user to switch
	 * back to factory on the processDetails page. 
	 * 
	 * Basically a wrapper for the get_process_parameter list.
	 * 
	 * @param1: process <obj>
	 * @param2: currentProcess <obj> 
	 * @param3: callback(err, {
	 * 	userInfo: {
	 * 		1: "FACTORY,
	 * 		2: "USER"
	 * 	},
	 * 	optionsList: [{
	 * 		name: "FACTORY" or "USER",
	 * 		exists: true/false
	 * 		params: <obj>
	 * 	}]
	 * })
	 */
	getParamOptions: function(process, currentProcess, cb) {
		var db = module.exports.db;
		db.serialize(function() {
			// first get the USER and FACTORY id's
			var usersSQL = SQLcommands.get_users;
			var userObj = {};
			db.all(usersSQL, function(err, rows) {
				if(err) throw err;
				// save the user and factory IDs
				for(var j = 0; j < rows.length; j++) {
					var user_id = rows[j].user_id;
					var user_name = rows[j].user_name;
					userObj[user_id] = user_name;
				}
			});
			// next get all the process rows for this process
			var SQL = SQLcommands.get_process_parameter_list(process, currentProcess);
			db.all(SQL, function(err, rows) {
				if(err) throw err;
				var result = [];
				// build up the resultObj
				var currentRow;
				for(var i = 0; i < rows.length; i++) {
					currentRow = rows[i];
					if(currentRow.user_id in userObj) {
						if(userObj[currentRow.user_id] == "USER") {
							result.push({
									name: "USER",
									exists: true,
									params: currentRow
								});
						}
						else {	// FACTORY
							result.push({
								name: "FACTORY",
								exists: true,
								params: currentRow
							});
						}
					}
				}
				cb(null, {
					userInfo: userObj,
					optionsList: result
				});
			})
		});
	},

	/* Get an array of suggested stock objects.
	 * Can be used to copy the process from on the processes page.
	 * Called from socket.io function with event processes_get-suggested-stock.
	 *
	 * @param1 reqData is an obj
	 {
		 request: {
			 type: 'tube_shape',
			 data: {
			 material_type: '304'
		 	}
		 }
		 processObj: process,
		 currentProcess: selectedJobProcess,
		 currentProcessType: selectedJobProcessType
	 }
	 */
	getSuggestedStock: function(request, cb) {
		var requestType = request.stockRequest.type;
		var dbM = module.exports;
		var db = dbM.db;
		var result =[];

		var myProcessArray = [];
		for(var j = 0; j < request.processObj.processes.length; j++) {
			if(request.processObj.processes[j].name == request.currentProcess) {
				// processes array now contains only info for the selected process.
				myProcessArray[0] = request.processObj.processes[j];
			}
		}
		var selectedProcessObj = request.processObj;
		selectedProcessObj.processes = myProcessArray;

		switch(requestType) {
			case 'material_type':
				db.all(SQLcommands.get_suggested_material_types(
					selectedProcessObj, selectedProcessObj.processes[0].name
				), function(err, materialTypeRows) {
					// go through each row and place into obj into result array
					for(var i = 0; i < materialTypeRows.length; i++) {
						result.push({
							value: materialTypeRows[i].material_type
						})
					}
					cb({
						dataType: 'material_type',
						data: result	// array of objects
					});
				});
				break;
			case 'tube_shape':
				db.all(SQLcommands.get_suggested_tube_shapes(
					selectedProcessObj,
					selectedProcessObj.processes[0].name,
					request.stockRequest.data.material_type
				), function(err, tubeShapeRows) {
					// go through each row and place into obj into result array
					for(var i = 0; i < tubeShapeRows.length; i++) {
						result.push({
							value: tubeShapeRows[i].tube_shape
						})
					}
					cb({
						dataType: 'tube_shape',
						data: result	// array of objects
					});
				});
				break;
			case 'dimension':
				var tubeShape = request.stockRequest.data.tube_shape;
				db.all(SQLcommands.get_suggested_dimensions(
					selectedProcessObj,
					selectedProcessObj.processes[0].name,
					request.stockRequest.data.material_type,
					tubeShape
				), function(err, dimensionRows) {
					if(err) throw err;
					// go through each row and place into obj into result array
					for(var i = 0; i < dimensionRows.length; i++) {
						var dimensionObj;
						if(tubeShape == "ROUND") {
							dimensionObj = {
								dimension_id: dimensionRows[i].property_id,
								diameter: dimensionRows[i].diameter
							}
						}
						else {
							dimensionObj = {
								dimension_id: dimensionRows[i].property_id,
								width: dimensionRows[i].width,
								height: dimensionRows[i].height,
								corner_radius: dimensionRows[i].corner_radius
							}
						}
						result.push(dimensionObj)
					}
					cb({
						dataType: 'dimension',
						data: result	// array of objects
					});
				});
				break;
			case 'thickness':
				if(selectedProcessObj.properties.stock_type == "TUBE") {
					db.all(SQLcommands.get_suggested_thicknesses(
						selectedProcessObj,
						selectedProcessObj.processes[0].name,
						request.stockRequest.data.material_type,
						request.stockRequest.data.tube_shape,
						request.stockRequest.data.dimension_id
					), function(err, thicknessRows) {
						if(err) throw err;
						// go through each row and place into obj into result array
						for(var i = 0; i < thicknessRows.length; i++) {
							result.push({
								value: thicknessRows[i].thickness_value
							})
						}
						cb({
							dataType: 'thickness',
							data: result	// array of objects
						});
					});
				}
				else {
					db.all(SQLcommands.get_suggested_thicknesses(
						selectedProcessObj,
						selectedProcessObj.processes[0].name,
						request.stockRequest.data.material_type
					), function(err, thicknessRows) {
						if(err) throw err;
						// go through each row and place into obj into result array
						for(var i = 0; i < thicknessRows.length; i++) {
							result.push({
								value: thicknessRows[i].thickness_value
							})
						}
						cb({
							dataType: 'thickness',
							data: result	// array of objects
						});
					});
				}
				break;
			case 'process':
				db.all(SQLcommands.get_suggested_processes(
					selectedProcessObj,
					selectedProcessObj.processes[0].name,
					request.stockRequest.data.material_type,
					request.stockRequest.data.tube_shape,
					request.stockRequest.data.dimension_id,
					request.stockRequest.data.thickness_value
				), function(err, processRows) {
					if(err) throw err;
					// go through each row and place into obj into result array
					for(var i = 0; i < processRows.length; i++) {
						result.push({
							process_name: processRows[i].process_name,
							process_type: processRows[i].process_type,
							property_id: processRows[i].property_id
						})
					}
					cb({
						dataType: 'process',
						data: result	// array of objects
					});
				});
				break;
		}
	},

	containsObject: function(obj, list) {
	var i;
	for (i = 0; i < list.length; i++) {
		if (JSON.stringify(list[i]) === JSON.stringify(obj)) {
			return true;
		}
	}
	return false;
	},

	makeLaserParametersDefault: function(laser_parameter_id, make_default) {
		module.exports.db.run(
			SQLcommands.set_laser_parameter_row_to_default(laser_parameter_id, make_default),
		function(err) {
			if(err) throw err;
			logger.log('[HMI] Set laser_parameter_id ' + laser_parameter_id +
			' default to ' + make_default);
		});
	},

	/** Create the machine_settings table information if it doesn't exist already, from
	 * the /opt/machine_settings.json file which should be added to the UDOO before shipment.
	 * Either create the table entirely or just update the columns, then insert the json data.
	 *
	 * @param db - nodejs sqlite3 database connection
	 * @param callback - no args
	 */
	check_machine_settings: function(db, callback) {
		db.all('PRAGMA table_info(machine_settings);', function(err, rows) {
			// Check if the columns that we are expecting exist in the DB.
			var requiredCols = [
				'setting_id',
				'last_modified',
				'admin_password',
				'process_pin_enabled',
				'process_pin',
				'laser_power',
				'gas',
				'serial_number',
				'model',
				'ip_address',
				'kerf',
				'display_unit_system'
			];
			// For each col we are expecting, see if it exists in the DB.
			for(var i = 0; i < rows.length; i++) {
				var currentDBcolName = rows[i].name;
				var requiredIndex = requiredCols.indexOf(currentDBcolName);
				if(requiredIndex > -1) {
					// If the column in the DB exists, then remove it from the requiredCols array.
					requiredCols.splice(requiredIndex, 1);
				}
			}
			// At this point, requiredCols only contains column names that we NEED but they DO NOT exist
			// in the dB right now.
			var allColsExistInDB = !(requiredCols.length > 0);
			if(allColsExistInDB == true) {
				// If all columns exist, get the data from the database and compare to the json
				module.exports.compare_json_to_db(db, function() {
					callback()
				})
			}
			else {
				logger.log('[HMI] These machine_settings columns do not exist: ' + requiredCols +
					'\tUpdating table schema...');
				// All of the columns don't exist in the given DB. So drop the entire table
				// and re-create it. Then insert the data from the json.
				db.serialize(function() {
					db.run('ALTER TABLE machine_settings RENAME TO machine_settings_orig;');
					db.run(SQLcommands.create_machine_settings);
					db.run('DROP TABLE machine_settings_orig;');
					// Now insert the row and run the callback
					module.exports.insert_machine_settings(db, function() {
						callback();
					})
				});
			}
		});
	},

	/** Check that the laser parameters table has all the correct columns. If it doesn't, update it (preserving the data)
	 * into a table that DOES have the correct columns. This function assumes that the laser_parameters table exists.
	 *
	 * @param db - nodejs sqlite3 database connection
	 * @param callback - no args
	 */
	verify_laser_parameters_schema: function(db, callback) {
		db.all('PRAGMA table_info(laser_parameters);', function(err, rows) {
			// Check if the columns that we are expecting exist in the DB.
			var requiredCols = [
				'laser_parameter_id',
				'user_id',
				'unit',
				'acceleration',
				'feedrate',
				'power',
				'min_power',
				'frequency',
				'pressure',
				'gas_delay',
				'nozzle_gap',
				'focus_offset',
				'lift_height',
				'laser_mode',
				'pierce_laser_mode',
				'pierce_power',
				'pierce_frequency',
				'pierce_pulses',
				'pierce_duration',
				'pierce_nozzle_gap',
				'pierce_pressure',
				'pierce_gas_delay',
				'kerf',
				'gas_id',
				'is_default'
			];
			// For each col we are expecting, see if it exists in the DB.
			for(var i = 0; i < rows.length; i++) {
				var currentDBcolName = rows[i].name;
				var requiredIndex = requiredCols.indexOf(currentDBcolName);
				if(requiredIndex > -1) {
					// If the column in the DB exists, then remove it from the requiredCols array.
					requiredCols.splice(requiredIndex, 1);
				}
			}
			// At this point, requiredCols only contains column names that we NEED but they DO NOT exist
			// in the dB right now.
			var allColsExistInDB = !(requiredCols.length > 0);
			if(allColsExistInDB == true) {
				// If all columns exist, run the callback
				callback(null);
			}
			else {
				logger.log('[HMI] These laser_parameter columns do not exist: ' + requiredCols);
				// All of the columns don't exist in the given DB. Must reprocess database using fabcreator.
				callback('Must reprocess DB in fabcreator');
			}
		});
	},

	/** Insert data from the machine_settings.json file in /opt (on UDOO) or in the
	 * setup folder (on dev machine). This function assumes that the db has been
	 * created, contains the right amount of columns, and the machine_settings.json
	 * has the right keys.
	 *
	 * @param db - nodejs sqlite3 database connection
	 * @param cb
	 */
	insert_machine_settings: function(db, cb) {
		var machineObj = module.exports.machineSettingsObj;
		var SQLcommand = 'INSERT INTO machine_settings (' +
			'last_modified, ' +
			'admin_password, ' +
			'process_pin_enabled, ' +
			'process_pin, ' +
			'laser_power, ' +
			'gas, ' +
			'serial_number, ' +
			'model, ' +
			'ip_address, ' +
			'kerf, ' +
			'display_unit_system' +
			') VALUES ( ' +
			'\"' + machineObj.last_modified + '\", ' +
			'\"' + machineObj.admin_password + '\", ' +
			'0' + ', ' +
			'NULL' + ', ' +
			machineObj.laser_power + ', ' +
			'\"' + machineObj.gas + '\", ' +
			'\"' + machineObj.serial_number + '\", ' +
			'\"' + machineObj.model + '\", ' +
			'\"' + machineObj.ip_address + '\", ' +
			machineObj.kerf + ', ' +
			'\"' + (typeof machineObj.display_unit_system == 'undefined'? 'IN' : machineObj.display_unit_system) + '\")';
		// console.log('[dbManager][insert_machine_settings] Executing SQL: ' + SQLcommand);
		db.run(SQLcommand, function(err) {
			if(err) throw err;
			cb()
		})
	},

	/** This function basically syncs up the machine_settings table in the database with the
	 * machine_settings.json file.
	 *
	 * We update MOST of the database columns with the info from the json file.
	 * We consider the json file the "source of truth" for the machine settings,
	 * so we update most of the DB columns using the json values.
	 *
	 * However, some items in the DB should not be updated, because they are user-editable.
	 * For example, the gas type on the machine is set by the user through the HMI application.
	 * So this field should persist in the DB upon reboot, it should not be "re-pulled" from the JSON file.
	 *
	 * @param db - nodejs sqlite3 database connection
	 * @param cb
	 */
	compare_json_to_db: function(db, cb) {
		var settingsObj = module.exports.machineSettingsObj;
		db.get(SQLcommands.get_machine_settings, function(err, row) {
			if(typeof row !== 'undefined') {
				// Check each of the DB columns to see if the value matches the JSON.
				var differences = [];
				for(var col in row) {
					// Do not check for a difference in the setting_id col, because this isn't in the JSON.
					// Do not check for a diff in the display_unit_system and gas columns as well because if those exist
					// in the DB, that value needs to be given priority over the json file.
					// Do not check for a diff in process_pin_enabled, because the DB value needs priority over the JSON file.
					// Do not check for a diff in process_pin, because the DB value needs priority over the JSON file.
					if(
						(col !== 'setting_id')
						&& (col !== "display_unit_system")
						&& (col !== "gas")
						&& (col !== 'process_pin_enabled')
						&& (col !== 'process_pin')
					) {
						if(row[col] !== settingsObj[col]) {
							differences.push(col);
						}
					}
					else {
						// Added check for if the setting_id, display_unit_system, or gas happen to be undefined
						if(row[col] === 'undefined') {
							differences.push(col);
						}
					}
				}
				if(differences.length > 0) {
					// There are some differences between the json and db.
					// Delete the current data in machine_settings re-insert the data from the json
					db.run('DELETE FROM machine_settings;', function(err) {
						module.exports.insert_machine_settings(db, function() {
							cb();
						});
					})
				}
				else {
					// There are no differences, so the JSON does match the DB.
					cb();
				}
			}
			else {
				// There are no rows in the table. Insert the data.
				module.exports.insert_machine_settings(db, function() {
					cb();
				});
			}
		});
	},
	
	machineSettingsObj: null,

	getMachineSettingsObj: function() {
		return module.exports.machineSettingsObj;
	},

	/** Look in the database for the display unit currently in the machine_settings table.
	 * 
	 * @param cb(unitSystem {string})
	 */
	getDisplayUnit: function(cb) {
		module.exports.db.get(SQLcommands.get_display_unit, function(err, row) {
			cb(row.display_unit_system)
		});
	},

	/** Get the process pin settings
	 *
	 * @param cb  - has form (err, settings) where settings is:
	 * 	{
	 * 		process_pin_enabled: <int, 0 or 1>
	 * 		process_pin: <int>
	 * 	}
	 */
	getProcessPinSettings: function (cb) {
		module.exports.db.get(SQLcommands.get_process_pin_settings, function (err, row) {
			if(err) {
				cb(err, null);
			}
			else {
				cb(null, {
					process_pin_enabled: row.process_pin_enabled,
					process_pin: (row.process_pin !== null)? row.process_pin.toString() : null
				});
			}
		});
	},

	/** Set the process pin
	 *
	 * @param newPin {int}
	 * @param cb
	 */
	setProcessPin: function (newPin, cb) {
		module.exports.db.run(SQLcommands.set_process_pin(newPin), function (err) {
			if(err) {
				cb(err, null);
			}
			else {
				cb(null);
			}
		});
	},

	/** Enable the process pin
	 *
	 * @param pinEnabled {boolean}
	 * @param cb
	 */
	enableProcessPin: function (pinEnabled, cb) {
		module.exports.db.run(SQLcommands.enable_process_pin(pinEnabled), function (err) {
			if(err) {
				cb(err, null);
			}
			else {
				cb(null);
			}
		});
	}

};

/** Parse the machine_settings.json file and return it as an object.
 * Save obj to module.exports.machineSettingsObj
 *
 * @return machineSettingsObj
 */
function get_machine_settings_json(cb) {
	var machineSettingsPath;
	if(config.production === true) { // Path on UDOO
		machineSettingsPath = path.join(__dirname, '..', 'machine_settings.json');
	}
	else { // Path on Mac
		machineSettingsPath = path.join(__dirname, '/setup', 'machine_settings.json');
	}
	var machineSettingsObj;

	fs.readFile(machineSettingsPath, function(err, data) {
		if(err) {
			// If there's an error reading the file, we are likely on a Mac but set production to "true" to test on the
			// machine. In this case, though we are using the production settings, still set the path for the Mac.
			machineSettingsPath = path.join(__dirname, '/setup', 'machine_settings.json');
			var machineSettingsJSON = fs.readFileSync(machineSettingsPath);
			machineSettingsObj = JSON.parse(machineSettingsJSON);
			module.exports.machineSettingsObj = machineSettingsObj;
			cb(machineSettingsObj);
		}
		else {
			machineSettingsObj = JSON.parse(data);
			module.exports.machineSettingsObj = machineSettingsObj;
			cb(machineSettingsObj);
		}
	});
}

/** Read the db/createTables.sql file to memory and generate an array of SQL strings
 * to be executed.
 * @param cb
 */
function create_tables(cb) {
	var createSQLpath = path.join(__dirname, 'db', 'SQLiteCommands', 'createTables.sql');
	fs.readFile(createSQLpath, "utf8", function(err, data) {
		var createStatementArray = data.split(';');
		createStatementArray.pop(); // The last element is "", empty string.
		var numberOfStatements = createStatementArray.length;
		// Now run all of the create statements.
		module.exports.db.serialize(function() {
			for(var i = 0; i < numberOfStatements; i++) {
				if(i == numberOfStatements - 1) {
					module.exports.db.run(createStatementArray[i], {}, function(err) {
						cb();
					});
				}
				else {
					module.exports.db.run(createStatementArray[i]);
				}
			}
		});
	});
}


// Process table database (SQLite) setup. Runs at app start up.
var dbFile = module.exports.dbPath(function(err, dbPath) {
	if(err) {
		// DB not found. Creating a new one.
		dbPath = path.join(__dirname, 'db', 'processDatabase.db');
	}
	module.exports.db = new sqlite3.Database(dbPath, function(err){
		// Create tables if they don't exist.
		create_tables(function() {
			// Parse the machine_settings.json file and return it as an object.
			get_machine_settings_json(function(obj) {
				module.exports.machineSettingsObj = obj;
				// Save kerf in SQLiteCommands
				SQLcommands.kerf = module.exports.machineSettingsObj.kerf;
				// See if the machine_settings table matches the JSON.
				// Add data to the machine_settings table if it's empty
				module.exports.check_machine_settings(module.exports.db, function() {
					module.exports.verify_laser_parameters_schema(module.exports.db, function (err) {
						if(err) {
							logger.log('[HMI] DB error: ' + err);
						}
					});
				});
			});
		});
	});
});