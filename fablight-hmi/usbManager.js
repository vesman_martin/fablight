/*
 * usbManager.js
 *
 * Maintains array of currently attached USB drives by checking for USB insertion/removal events.
 */


var driveList = require('drivelist');
var fs = require('fs');
var util = require('util');
var path = require('path');
var config = require('./config');

var spawn = require('child_process').spawn;

var cp = require('cp');

var USBarray = [];

module.exports = {
	usbPath: null,
	jobPath: {
		folder: path.normalize(__dirname + '/public/uploads/jobs/'),
		file: null,		// job UCJ file name
		preview: null,	// job preview file name
		process: null,	// job process file name
		location: 'usb',
		fabFileName: null
	},
	
	/* @function getUSBList (callback)
	 * use the drivelist npm module to get a list of all removable drives plugged into USB.
	 *
	 * callback(err, usb <object>, mountpath <string>)
	 * where usb has the following keys:
	 * 	description <string>
	 * 	device <string>
	 * 	mountpoints <array>
	 * 	    { path <string> }
	 * 	protected <bool>
	 * 	raw <string>
	 * 	size <int>
	 * 	system <bool>
	 */
	getUSBList: async function(callback) {
			try {
				var drives = await driveList.list();
				var foundRemovableUSB = false;
				var removableUSBindex;
				var USBpath;
				//console.log('[usbManager.js] Drives:');
				//console.log(util.inspect(drives, false, null));
				// CODE FOR MAC TESTING, easy to tell which is the removable USB (system = false)
				if (config.production == false) {
					for (var d = 0; d < drives.length; d++) { // iterate through all drives
						// check if the drives is a removable usb that has been mounted
						if (drives[d].isSystem == false) { // removable usb
							if (drives[d].mountpoints.length > 0) { //mounted
								foundRemovableUSB = true;
								removableUSBindex = d;
							}
						}
					}
					//console.log('[usbManager.js] Checked all USB drives on Mac');
					if (foundRemovableUSB == true) {
						USBpath = drives[removableUSBindex].mountpoints[0].path;
						module.exports.USBpath = USBpath; // Save USB path to usbManager.

						// console.log('[usbManager][getUSBList] Found USB drive ' + util.inspect(drives[removableUSBindex], false, null));
						callback(null, drives[removableUSBindex], USBpath);
					}
					else {
						callback('No removable USBs found');
					}
				}

				// CODE FOR UDOO, the SD card is considered a removable drive mounted at '/boot'
				else {
					// console.log('[usbManager][getUSBList] Testing on UDOO');
					// find the mounted, removable USB drive out of all drives listed.
					for (var u = 0; u < drives.length; u++) {
						if (drives[u].isSystem == false) { // removable
							if (drives[u].mountpoints.length > 0) { //mounted
								var mountpointsArray = drives[u].mountpoints;

								var isSDcard = false;
								// sometimes a single device can be mounted at multiple paths
								for (var m = 0; m < mountpointsArray.length; m++) {
									// iterate through each path. SD card is mounted at /boot
									if (mountpointsArray[m].path == '/boot') {
										isSDcard = true;
									}
								}
								if (isSDcard == false) {
									foundRemovableUSB = true;
									removableUSBindex = u;
									// console.log('[usbManager.js] ' + util.inspect(drives[u]));
								}
							}
						}
					}
					//console.log('[usbManager.js] Checked all USB drives on UDOO');
					if (foundRemovableUSB == true) {
						USBpath = drives[removableUSBindex].mountpoints[0].path;
						module.exports.USBpath = USBpath; // Save USB path to usbManager.

						// console.log('[usbManager][getUSBList] Found USB drive ' + util.inspect(drives[removableUSBindex], false, null));
						callback(null, drives[removableUSBindex], USBpath);
					}
					else {
						callback('No removable USBs found');
					}
				}
			} catch (error) {
				console.log("p1: getUSBList error");
				throw error;
        	}
			
	},

	/** Given a path, get all of the files and folders in this path.
	 *
	 * @param requestedPath
	 * @param callback(err, filesAndFolders) - Of the form:
	 * {
		path: requestedPath,
		files: [{ name: <str> }],
		folders: [{ name: <str> }]
		}
	 */
	getFilesAndFolders: function(requestedPath, callback) {
		// itemArray is array with all file & folder names in a given directory
		fs.readdir(requestedPath, function(err, itemArray) {
			if(err) {
				// "No such file or directory"
				// If the USB has been unplugged
				if(err.code === "ENOENT") {
					callback('No such file or directory.');
				}
				else {
					callback('Error reading file. Code: ' + err.code);
				}
			}
			else {
				var result = {
					path: requestedPath,
					files: [],
					folders: []
				};
				// Iterate over each item in the array
				var count = 0;
				for (var i = 0; i < itemArray.length; i++) {
					(function(idx) {
						var itemName = itemArray[idx];
						var itemPath = path.join(requestedPath, itemName);
						fs.stat(itemPath, function(err, stats) {
							if(err) throw err;
							if(stats.isDirectory() == true) {
								// Filter out hidden folders, they start with a "."
								if(! /^\..*/.test(itemName)) {
									result.folders.push({
										name: path.basename(itemName)
									});
								}
							}
							else {
								// Filter out hidden files that start with a "._"
								if(! /^\.\_.*/.test(itemName)) {
									// Only care about .fab files, not .fab1 or anything else.
									var fileExtension = path.extname(itemName).toLocaleLowerCase();
									if(fileExtension == ".fab") {
										result.files.push({
											name: path.basename(itemName, '.fab')
										})
									}
								}
							}
							count++;
							// Callback once every item has been processed.
							if(count > itemArray.length - 1) {
								// Sort the files and folders array case-insensitively
								result.files.sort(module.exports.sortNamesAlphaNoCase);
								result.folders.sort(module.exports.sortNamesAlphaNoCase);
								callback(null, result);
							}
						});
					}(i)); // Closure: pass the index from the for loop
				}
			}
		})
	},

	sortNamesAlphaNoCase: function(a, b){
		a = a.name.toLowerCase();
		b = b.name.toLowerCase();
		if (a == b) return 0;
		if (a > b) return 1;
		return -1;
	},

	/* @function getDBFileNames (callback)
	 * given a mountpoint <string> of path USB is mounted at,
	 * get all of the .db files in the root of the USB drive.
	 *
	 * callback (filesList <array of file names in root of USB drive>)
	 */
	getDBFileNames: function(mountpoint, callback) {
		fs.readdir(mountpoint, function(err, itemArray) {
			if(err) throw err;

			// filter the results to only show the files (not folders)
			var filesList = [];
			var itemProcessed = 0;
			itemArray.forEach(function(itemName, index) {
				var itemPath = path.normalize(mountpoint + '/' + itemName);
				fs.stat(itemPath, function(err, stats) {
					if(err) {
						throw err;
					}
					else {
						if(stats.isDirectory() == false) {
							// if the file is a fab
							var fileExtension = path.extname(itemPath).toLowerCase();
							if((fileExtension == '.db') && (itemName.charAt(0) != '.')) {
								filesList.push(itemName);
							}
						}
						itemProcessed++;
						if(itemProcessed === itemArray.length) {
							callback(filesList);
						}
					}
				});
			});
		})
	},

	/* @function getUSBPath (callback)
	 * get the mount path of the USB drive
	 *
	 * callback (err, mountpath <string>)
	 */
	getUSBPath: function(callback) {
		driveList.list(function(error, drives) {
			if (error) { throw error; }
			var foundRemovableUSB = false;
			var removableUSBindex;
			//console.log('[usbManager.js] Drives:');
			//console.log(util.inspect(drives, false, null));

			// CODE FOR MAC TESTING, easy to tell which is the removable USB (system = false)
			if(config.production === false) {
				for(var d = 0; d < drives.length; d++) {	// iterate through all drives
					// check if the drives is a removable usb that has been mounted
					if(drives[d].isSystem === false) {		// removable usb
						if(drives[d].mountpoints.length > 0) {	//mounted
							foundRemovableUSB = true;
							removableUSBindex = d;
						}
					}
				}
				if(foundRemovableUSB === true) {
					callback(null, drives[removableUSBindex].mountpoints[0].path);
				}
				else {
					callback('NOUSB', '[usbManager][error] No removable USBs found');
				}
			}
			// CODE FOR UDOO, the SD card is considered a removable drive mounted at '/boot'
			else {
				for (var u = 0; u < drives.length; u++) {
					if (drives[u].isSystem === false) {		// removable usb
						if (drives[u].mountpoints.length > 0) {	//mounted
							var mountpointsArray = drives[u].mountpoints;

							var isSDcard = false;
							// sometimes a single device can be mounted at multiple paths
							for (var m = 0; m < mountpointsArray.length; m++) {
								// iterate through each path. SD card is mounted at /boot
								if (mountpointsArray[m].path === '/boot') {
									isSDcard = true;
								}
							}
							if(isSDcard === false) {
								foundRemovableUSB = true;
								removableUSBindex = u;
								// console.log('[usbManager.js] ' + util.inspect(drives[u]));
							}
						}
					}
				}
				if(foundRemovableUSB === true) {
					callback(null, drives[removableUSBindex].mountpoints[0].path);
				}
				else {
					callback('NOUSB', '[usbManager][error] No removable USBs found');
				}
			}
		});
	},
	/* @function copyFile
	 * This is a helper function that copies a file using node.js read/write streams (with error handling).
	 * Given a source path, target path, and callback, copy a file from the source to target.
	 */
	copyFile: function (source, target, cb) {
		var cbCalled = false;

		var rd = fs.createReadStream(source);
		rd.on("error", function(err) {
			done(err);
		});
		var wr = fs.createWriteStream(target);
		wr.on("error", function(err) {
			done(err);
		});
		wr.on("close", function(ex) {
			done();
		});
		rd.pipe(wr);

		function done(err) {
			if (!cbCalled) {
				cb(err);
				cbCalled = true;
			}
		}
	},

	/* @function copyFileToUSB
	 * Copies a file using node.js read/write streams (with error handling).
	 *
	 * Uses "sync" linux command after the file is copied to ensure it's written to disk.
	 *
	 * @param1: source file path
	 * @param2: target file path
	 * @param3: callback(<err or null>)
	 */
	copyFileToUSB: function (source, target, cb) {
		fs.chmodSync(source, '755');

		module.exports.copyFile(source, target, function(err) {
			if(err) {
				cb(err);
			}
			else {
				// run 'sync' command
				var test = spawn('sync');
				test.on('close', function() {
					cb(null);
				})
			}
		});
	}
};