/*
* app.js is the entry point of this app.
*/

// require necessary modules
var compression = require('compression');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var session = require('express-session');

var winston = require('./config/winston');

// App config
var config = require('./config');

// require route modules
var index = require('./routes/index');
var home = require('./routes/home');
var newjobfrom = require('./routes/newjobfrom');
var fileupload = require('./routes/fileupload');
var sendfile = require('./routes/sendfile');
var selectfile = require('./routes/selectfile');
var processTable = require('./routes/processTable');
var processes = require('./routes/processes');
var runjob = require('./routes/runjob');
var jog = require('./routes/jog');
var preview = require('./routes/preview');
var files = require('./routes/files');
var settings = require('./routes/settings');

// custom socket modules
var status = require('./machineStatus');

// app variable
var app = express();
app.use(compression());	// Use gzip compression

// socket.io setup with server
var server = require('http').Server(app);
var io = require('socket.io')(server); 		// not sure if this is needed
// socket.io
app.use(function(req, res, next){
	res.io = io;
	next();
});
var socketData = require('./socketDataFunctions');
io.on('connection', function (socket) {
	socketData.handleSocketIO(socket);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
if(config.production === false) {
	// app.use(logger('dev'));
}
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({
	extended: true	// default was false. set to true means deep parsing of request objs.
}));
app.use(cookieParser());
// The static middleware must come after the sass middleware
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
// 	secret: 'qwerty',
// 	resave: true,
// 	saveUninitialized: true
// }));

// routes
app.use('/', index);
app.use('/home', home);
app.use('/fileupload', fileupload);
app.use('/sendfile', sendfile);
app.use('/selectfile', selectfile);
app.use('/processes', processes);
app.use('/processTable', processTable);
app.use('/runjob', runjob);
app.use('/jog', jog);
app.use('/newjobfrom', newjobfrom);
app.use('/preview', preview);
app.use('/files', files);
app.use('/settings', settings);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;

	// add this line to include winston logging
	winston.error((err.status || 500) + ' - ' + err.message + ' - ' + req.originalUrl + ' - ' + req.method);

	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res) {
		// add this line to include winston logging
		winston.error(err.status || 500 + ' - ' + err.message + ' - ' + req.originalUrl + ' - ' + req.method);

		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res) {
	// add this line to include winston logging
	winston.error(err.status || 500 + ' - ' + err.message + ' - ' + req.originalUrl + ' - ' + req.method);
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = {app: app, server: server};