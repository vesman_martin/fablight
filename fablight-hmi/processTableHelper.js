 /*
 * processTableHelper.js
 *
 * This file has helper functions for parsing the ucj job file to get process info.
 * It also has functions to read the processtable JSON file and extract a process (material,
 * thickness, process)
 * Finally, there are functions to generate UCJ process table and insert it back into the jobfile.
 */

var status = require('./machineStatus');

// load modules
var fs = require('fs');
var readline = require('readline');
var path = require('path');
var convert = require('convert-units');
var appRoot = require('app-root-path');
 
// process table database from the dbManager.js file
var dbManager = appRoot.require('dbManager');
var usbManager = appRoot.require('usbManager');
var SQLcommands = appRoot.require(path.join('db', 'SQLiteCommands', 'SQLiteCommands'));
var logger = appRoot.require('logger');

var util = require('util');

 module.exports = {

	/* @addFullProcessTableToUCJ - given a file name and an array of process objs, generate the UCJ
	 * process table and insert it into the process.ucj file at the path
	 *
	 * This is the ENTRY FUNCTION to create a complete UCJ file.
	 *
	 * @param1: file name
	 * @param3: callback(<err>) If there is an error the callback will have an error object
	 *
	 * processArray looks like the following:
	 * [
	 * 	{
	 * 		processNumber: 1,
	 * 		materialName: 'Brass',
	 * 		thicknessValue: 0.063,
	 * 		thicknessUnit: 'inches',
	 * 		processName: 'Engrave',
	 * 		gasType: 'air',
	 * 		processExists: true
	 * 	},
	 * 	{
	 * 		...
	 * 	}
	 * ]
	 *
	 */
	addFullProcessTableToUCJ: function (fileName, callback) {
		var processUCJpath = path.join(usbManager.jobPath.folder, usbManager.jobPath.process);
		module.exports.getCurrentGas(function (err, currentGas) {
			// use the master process object from the processTableHelper to make the UCJ
			// Do NOT point to the object, you must copy it.
			var processObj = JSON.parse(JSON.stringify(module.exports.masterProcessObj));
			processObj.gas = currentGas;	// add the gas to the process object
			module.exports.makeProcessTMP(processUCJpath, processObj, function(){
				// console.log(module.exports.masterProcessObj);
				callback(null);
			});
		});
	},

	 /* makeProcessTMP
	  * Create uploads/processTMP.ucj using the process object.
	  * This file is a copy of process.ucj except it has the UCJ process table inserted.
	  *
	  * @param2: processUCJpath, path to the process.ucj file
	  * @param1: processObj containing processObj.processes[{}, {}, ..]. Each object has a params key.
	  * @param2: callback(<err>)
	  */
	 makeProcessTMP: function(processUCJpath, pObj, callback) {
		 // first generate the UCJ tables for each process.
		 module.exports.generateUCJprocessTable(pObj, function (UCJarray) {
			 // UCJarray is an array of UCJ process table strings, once string for each process.
			 // read the process.ucj file line by line, inserting the process tables at the right time.
			 module.exports.insertUCJprocessesIntoJob(processUCJpath, UCJarray, function () {
				 callback();
			 })
		 });
	 },

	/* @getParamsForProcesses - given an entire process obj, check the DB to get the laser_attributes
	 * for each process and return them as a new key ("params").
	 * See getProcessIdentifiersFromUCJ for process object structure.
	 *
	 * @param1: process <obj>
	 * @param2: callback(<process obj with "params" key added>, <bool: true if ALL processes exist>)
	 *
	 */
	getParamsForProcesses: function (process, callback) {
		// iterate over each process
		var processFromDB = JSON.parse(JSON.stringify(process)); // Do NOT modify the existing object. Create a clone/copy.
		var numToProcess = processFromDB.processes.length;
		var count = 0;
		var allProcessExists = true;
		for(var i = 0; i < numToProcess; i++) {
			(function(p){
				var currentProcess = processFromDB.processes[p]; //obj
				// for each process try to look up the parameters in the database
				// CUT uses the dimensions of the file while ENGRAVE and RASTER do not.
				var dbQuery = SQLcommands.get_process_parameter_list(processFromDB, currentProcess);
				dbManager.db.all(dbQuery, function(err, parameterList) {
					if(err) throw err;
					// For CUT, ENGRAVE, or RASTER there may be 0, 1, or 2 parameter rows returned from the database.
					// 0 = process does not exist
					// 1 = single process exists, either user or factory
					// 2 = there is a user and factory, one of which should be set to default.
					switch(parameterList.length) {
						case 0: // process does not exist. do not add params key to the object.
							allProcessExists = false;
							break;
						case 1:
							// If there is only a single process available and it's NOT default,
							// then set it to be default.
							if(parameterList[0]['is_default'] === 0) {
								dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
										parameterList[0].laser_parameter_id, true));
								// Also update the parameterList object explicitly. Required because
								// DB update is an asynchronous function.
								parameterList[0].is_default = 1;
							}
							currentProcess.params = parameterList[0];
							break;
						case 2:
							// If there are 2 parameters, check if the first is default.
							if(parameterList[0]['is_default'] === 1) {
								currentProcess.params = parameterList[0];
								// Make the other process NOT default.
								dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
										parameterList[1].laser_parameter_id, false));
							}
							// Check if the second one is default. If it is, use it it.
							else if (parameterList[1]['is_default'] === 1) {
								currentProcess.params = parameterList[1];
								// Make the other process NOT default.
								dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
										parameterList[0].laser_parameter_id, false));
							}
							// Otherwise just make the second one default (HARDCODED assumption).
							else {
								// Set it to default in the DB.
								dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
										parameterList[1].laser_parameter_id, true));
								parameterList[1].is_default = 1;
								currentProcess.params = parameterList[1];
							}
							break;
						default:
							logger.log('[HMI] Error retrieving db data.');
							break;
					}
					count ++;
					if(count > numToProcess - 1) {
						// finished creating commands for each of the processes (will only run once)
						callback(processFromDB, allProcessExists);
					}
				});
			}(i));
		}
	},
	 
	 /* @function getLaserParamsForPropertyID - Typically called from /processes going to /processCreator.
	  * Given a property_id <int>, and a gas_name<str>, look up the laser_parameters in the database.
	  * 
	  * The reason we need gas_name is because property_id (in the properties and gases tables) isn't
	  * enough to get a single laser_parameter, because there can be different parameters for different gases.
	  * 
	  * If more than one set of params exist (in the case of user vs factory), get the one marked default=1. If
	  * there is no default, get the USER and set it to default.
	  * If there's no results, that means the database is blank/wrong. Throw an error.
	  *
	  * @param1: property_id <int>
	  * @param2: gas_name <str>    
	  * @param2: callback(err, paramsObj{})
	  */
	 getLaserParamsForPropertyID: function(property_id, gas_name, callback) {
		 dbManager.db.all(
			 SQLcommands.get_laser_parameters(property_id, gas_name),
			 function(err, rows) { 
				 if(err) callback(err, null);
				 if(rows.length > 1) {
					 // Multiple rows can be returned. Find the default one.
					 var defaultParam = {};
					 for(var i = 0; i < rows.length; i++) {
						 if(rows[i].is_default === 1) {
							 defaultParam = rows[i];
						 }
					 }
					 // If there is no default, then set the USER process to default.
					 if(JSON.stringify(defaultParam) === "{}") {
						 // Find the USER process. There can be more than one.
						 var userRowIndices = [];
						 for(var k = 0; k < rows.length; k++) {
							 if(rows[k].user_id === 2) {	// HARDCODED user = 2. (Factory=1)
								 userRowIndices.push(k);
							 }
						 }
						 // userRowIndices is an array of all the indices of the rows that are USER parameters.
						 if(userRowIndices.length > 0) {
							 // If there is at least one USER process, set the first one to default.
							 dbManager.makeLaserParametersDefault(rows[userRowIndices[0]].laser_parameter_id, true);
						 } else {
							 // If there is no user process, arbitrarily set the first row returned to default.
							 // It is probably a FACTORY process.
							 dbManager.makeLaserParametersDefault(rows[0].laser_parameter_id, true);
						 }
						 if(userRowIndices.length > 1) {
							 // If there is more than one USER process, log an error.
							 logger.log('[HMI] DB Warning. There is more than one USER process ' +
								 'for property_id = ' + property_id + ' with gas_name = ' + gas_name);
						 }
					 }
					 callback(null, defaultParam);
				 }
				 else if (rows.length === 1) {
					 // If only a single process is found, then use it.
					 // If it isn't the default, set it to default.
					 if(rows[0].is_default === 0) {
						 dbManager.makeLaserParametersDefault(rows[0].laser_parameter_id, true);
					 }
					 callback(null, rows[0]);
				 }
				 else {
					 // There is an error because the process has not been found in the DB.
					 // Should not reach this case because this function is only called when creating a suggested
					 // process from an already existing one.
					 callback('No process row found.', null);
				 }
		 });
	 },

	 UCJportNumberHash: {
		 "acceleration": 1062,
		 "feedrate": 1050,
		 "power": 1051,
		 "min_power": 1052,
		 "frequency": 1053,
		 "pressure": 1056,
		 "gas_delay": 1057,
		 "nozzle_gap": 1058,
		 "focus_offset": 1059,
		 "lift_height": 1060,
		 "laser_mode": 1061,
		 "pierce_laser_mode": 1069,
		 "pierce_power": 1070,
		 "pierce_frequency": 1071,
		 "pierce_pulses": 1072,
		 "pierce_duration": 1073,
		 "pierce_nozzle_gap": 1074,
		 "pierce_pressure": 1075,
		 "pierce_gas_delay": 1076,
		 "kerf": 601
	 },

	 /* @function generateUCJprocessTable - generate the process table for a processObj containing one or more processes.
	  *
	  * @param1: processObj w/params in processObj.processes[{}, {}, ...]
	  * @param2: callback(UCJcommands <array of objs>)
	  *
	  * [{
	  * 	number: <int>,
	  * 	UCJtable: <str of entire ucj table to insert>
	  * }]
	  */
	generateUCJprocessTable: function (pObj, callback) {
		// hash of laser_parameter column name : UCJ port number
		var UCJportNumberHash = module.exports.UCJportNumberHash;
		// store the UCJ commands in an array
		var UCJarray = [];
		var UCJstring;
		// Special params for Raster and Engrave that must be set independently
		var paramsToSetToZero = ['pierce_duration', 'pierce_power'];
		// Iterate through each process in the process object.
		for(var i = 0; i < pObj.processes.length; i ++) {
			var processArray = [];
			var currentProcess = pObj.processes[i];
			// If we are looking at RASTER or ENGRAVE process, set the kerf to null.
			if((currentProcess.name === "RASTER") || (currentProcess.name === "ENGRAVE")) {
				currentProcess.params.kerf = null;
			}
			// If the process unit is MM, then we need to convert to IN.
			if(currentProcess.params.unit === "MM") {
				pObj.processes[i].params = module.exports.convertParamUnits(pObj.processes[i].params, "IN");
			}
			// filter to only get the values in the UCJ port numbers to build the process table:
			for(var param in pObj.processes[i].params) {
				if(UCJportNumberHash[param] !== undefined) {
					// now check if the param value is null. it shouldn't be. but this indicates that there is a problem
					// ex) pierce_delay = null but it should have a value of 0.
					// There is a special case below for RASTER and ENGRAVE, where pierce params will be null,
					// indicating that they don't have a pierce. This means a 0 pierce_delay should be sent
					// to the controller.
					if(pObj.processes[i].params[param] == null) {
						// 1. Determine which params NEED to be set to 0 using the paramsToSetToZero array.
						if(paramsToSetToZero.indexOf(param) > -1) {
							if((currentProcess.name === "RASTER") || (currentProcess.name === "ENGRAVE")) {
								// 2. Set the params in paramsToSetToZero to 0 for RASTER and ENGRAVE processes.
								UCJstring =  module.exports.setPortEx(
									UCJportNumberHash[param],
									0,
									4006,
									param
								);
								processArray.push(UCJstring);
							}
						}
						// logger.log('[HMI] DB Warning. Entry for ' + param + ' is null');
					}
					else {
						// Special case if the feedrate is less than 0.01in/s then set to 0.01. This is the
						// hardcoded min feedrate.
						if(param === 'feedrate') {
							if(parseFloat(pObj.processes[i].params[param]) <= 0.01) {
								pObj.processes[i].params[param] = 0.01.toString(); // hardcoded minimum feedrate
							}
						}
						// make a string with the port number, value, and the set_port_ex command
						// Round to 4 decimal places
						UCJstring = module.exports.setPortEx(
							UCJportNumberHash[param],	// port ID
							pObj.processes[i].params[param],	// value
							4006,	// port
							param // comment
						);
						processArray.push(UCJstring);
					}
				}	
			}
			// All UCJ params added, add the final line.
			processArray.push(module.exports.setPortEx(9999, pObj.processes[i].number, 4006));
			UCJarray.push({
				number: parseFloat(pObj.processes[i].number),
				UCJtable: processArray.join('\r\n')
			});	// Join the commands for the process with CRLF and save to UCJarray.
		}
		callback(UCJarray);
	},

	 /** Helper function. Given a value (float) create a set_port_ex string.
	  *
	  * Round the value to 4 decimal places.
	  *
	  * ex) 1070 0. 4006 set_port_ex ` pierce_power
	  *
	  * @param port_id
	  * @param value
	  * @param {int} port
	  * @param {string} comment - optional
	  * @return {string} - formatted string
	  */
	 setPortEx: function(port_id, value, port, comment) {
		 var command = port_id + ' '
			 + module.exports.convertToUCJFloat(module.exports.round(value, 4)) + ' '
			 + port + ' set_port_ex';
		 if(comment) {
			 command += ' \t` ' + comment;
		 }
		 return command;
	 },

	/*
	 * @function convertToUCJFloat
	 *
	 * Takes a string containing a value ex) "0.5" or "2" and
	 * turns it into a UCJ floating number. A period (.) needs to be added to the end
	 * of a value (if it does not have a '.')
  	 * If the number already has a decimal in it, like "0.5" then do not change it.
	 */
	convertToUCJFloat: function convertToUCJFloat(number) {
		var numberString = number.toString();
		// if the input value string DOES NOT have a '.' period, then add one to the end.
		if (numberString.indexOf('.') === -1) {
			return numberString + '.';
		}
		return numberString;
	},

	/* @function insertUCJprocessesIntoJob: given an array of all processes in the job, insert the commands
	 * into the processTMP.ucj file.
	 *
	 * @param1: UCJfilePath <string> file path of original UCJ file
	 * @param2: processArray <array of objs created by generateUCJprocessTable>
	 * @param3: callback() called when complete
	 */
	insertUCJprocessesIntoJob: function (UCJfilePath, processArray, callback) {
		var oldFilePath = path.parse(UCJfilePath);
		var processTMPfilePath = path.join(oldFilePath.dir, 'processTMP.ucj');
		// stream set up. read from process.ucj, write to uploads/jobs/processTMP.ucj
		var readableStream = fs.createReadStream(UCJfilePath);
		var writeableStream = fs.createWriteStream(processTMPfilePath);
		readableStream.setEncoding('utf8');
		var rl = readline.createInterface({
			input: readableStream,
			output: writeableStream
		});
		rl.on('line', function(line) {
			// match the process line
			var matchProcess = line.match(/^(\d).* ~(.*)\|(.*)~ 4600 set_port_ex.*$/);
			if(matchProcess) {
				writeableStream.write(line + '\n'); // write the process line
				var processNumber = parseFloat(matchProcess[1]);	// get processNumber in UCJ file
				// insert the UCJ table from the processArray
				// get the index of the ucj table obj with the correct process number.
				var UCJindex;
				for(var i = 0; i < processArray.length; i++) {
					if(parseFloat(processArray[i].number) === processNumber) {
						UCJindex = i;
					}
				}
				writeableStream.write(processArray[UCJindex].UCJtable + '\n');
			}
			else {
				writeableStream.write(line + '\n');
			}
		});
		rl.on('close', function () {
			callback();
		})

	},

	 res: null,

	 /* insertEntryIntoDB - given a process object which includes laser_parameters
	  * and all properties needed for all tables, traverse the tables to insert it.
	  * Works with foreign key constraints.
	  * 
	  * We assume that the units and stock_types tables are already filled out.
	  *
	  * @param1: processObj of standard form, inclues only ONE process in processObj.processes
	  * @param2: res - response object, optional
	  */
	 insertEntryIntoDB: function(pObj, res) {
		 module.exports.res = res;
		 var db = dbManager.db;
		 // Assumed all stock types and unit is already in db.
		 // 1. Get the stock_type_id of the process object using it's stock_type and tube_shape.
		 db.get(SQLcommands.get_stock_type_id(pObj), function(err, rowObj) {
			 if(err) throw err;
			 // Assume you will get ONE row result i.e. only one IN, SQUARE exists.
			 var stock_type_id = rowObj.stock_type_id;
			 // 2. Check if the material exists. Get the material_id using the process object
			 // and the stock_type_id.
			 db.get(SQLcommands.check_if_material_exists(pObj, stock_type_id), function(err, rowObj) {
				 if(err) throw err;
				 if (typeof rowObj === 'undefined') {
					 // The material does not exist in the db already, so insert it.
					 // This will eventually render the page.
					 module.exports.insert_material(pObj, stock_type_id);
				 }
				 else {
					 // 3. The material DOES exist in the db already, now check for the properties.
					 // Try to get the property_id given the material_id and the process object.
					 var material_id = rowObj.material_id;
					 db.get(SQLcommands.check_if_property_exists(pObj, material_id), function(err, rowObj) {
						 if (err) throw err;
						 if (typeof rowObj !== 'undefined') {
							 // 4. Property (including process and dimensions) exist, now check for gas.
							 // Try to get the gas_id given the property_id and then process obj.
							 var property_id = rowObj.property_id;
							 db.get(SQLcommands.check_if_gas_exists(pObj, property_id), function (err, rowObj) {
								 if(err) throw err;
								 if (typeof rowObj != 'undefined') {
									 // 5. Gas exists but it can be NULL or filled in.
									 // If the gas_name is NULL and user_id is 0:
									 // 	Then just update the row. The user made a material in fabcreator.
									 var gas_id = rowObj.gas_id;
									 if(rowObj.user_id === 0) {
										 // Update the gas_name and user_id, then update the laser_parameter row.
										 module.exports.update_gas(pObj, gas_id);
									 }
									 else {
										 // If the gas_name is <str> and user_id is not 0:
										 // 	Insert the parameters using the gas_id (and render the page)
										 module.exports.insert_laser_parameter(pObj, gas_id);	 
									 }
								 }
								 else {
									 // Gas does not exist, so insert a new row.
									 module.exports.insert_gas(pObj, property_id);
								 }
							 });
						 }
						 else {
							 // Property does not exist, create new by starting the insert cascade from property.
							 module.exports.insert_property(pObj, material_id);
						 }
					 });
				 }
			 });
		 });
	 },

	 // insert_material_promise: function() {
		//  return new Promise(function(resolve, reject) {
		// 	 db.run(SQL.insert_new_material_row(pObj, stock_type_id), function(err) {
		// 		 if(err) {
		// 			 reject();
		// 		 }
		// 		 else {
		// 			 var material_id = this.lastID;
		// 			 resolve(material_id);
		// 		 }
		// 	 });
		//  })
	 // },
	 /* USED FOR insertEntryIntoDB
	  * start of long list of functions to insert data into db.
	  * represents a hierarchy of inserts.
	  * eventually clean up w/promises instead of callback chain.
	  */
	 insert_material: function(pObj, stock_type_id) {
		 dbManager.db.run(SQLcommands.insert_new_material_row(pObj, stock_type_id), function(err) {
			 if (err) throw err;
			 var material_id = this.lastID;
			 module.exports.insert_property(pObj, material_id);
		 });
	 },
	 insert_property: function(pObj, material_id) {
		 dbManager.db.run(SQLcommands.insert_new_property_row(pObj, material_id), function(err) {
			 if (err) throw err;
			 var property_id = this.lastID;
			 module.exports.insert_gas(pObj, property_id);
		 });
	 },
	 insert_gas: function(pObj, property_id) {
		 dbManager.db.run(SQLcommands.insert_new_gas_row(pObj, property_id), function(err) {
			 if(err) throw err;
			 var gas_id = this.lastID;
			 module.exports.insert_laser_parameter(pObj, gas_id);
		 });
	 },
	 insert_laser_parameter: function(pObj, gas_id) {
		 dbManager.db.run(SQLcommands.insert_laser_parameters_row(pObj, gas_id), function(err) {
			 if(err) throw err;
			 var laser_parameter_id = this.lastID;
			 module.exports.inserts_complete(pObj, laser_parameter_id);
		 });
	 },
	 inserts_complete: function(pObj, laser_parameter_id) {
		 // Save the added process to the master process object.
		 // Also save the laser parameter ID to the master process object.
		 var currentProcess = pObj.processes[0];
		 for(var i = 0; i < module.exports.masterProcessObj.processes.length; i++) {
			 if(module.exports.masterProcessObj.processes[i].name === currentProcess.name) {
				 module.exports.masterProcessObj.processes[i].params = pObj.processes[0].params;
				 module.exports.masterProcessObj.processes[i].params.laser_parameter_id = laser_parameter_id;
				 module.exports.masterProcessObj.processes[i].params.user_id = 2; //HARDCODED user_id to 2
			 }
		 }
		 // render the page
		 module.exports.res.sendStatus(200).end();
	 },

	 // Update a gas entry given a gas_id. Then update the laser parameter row.
	 update_gas: function(pObj, gas_id) {
		 dbManager.db.run(SQLcommands.update_gas_row(pObj, gas_id), function(err) {
			 if(err) throw err;
			 module.exports.update_laser_parameter(pObj, gas_id);
		 });
	 },
	 // Update a laser_parameter_row given a process object and a gas_id.
	 update_laser_parameter: function(pObj, gas_id) {
		 dbManager.db.run(SQLcommands.update_laser_parameters_row(pObj, gas_id), function(err) {
			 if(err) throw err;
			 var laser_parameter_id = this.lastID;
			 module.exports.inserts_complete(pObj, laser_parameter_id);
		 });
	 },

	 /* @function generalizeString
	  *
	  * Used for better string comparison to look up material name in DB.
	  *
	  * Given a string: remove extra spaces, trim(), replace spaces with _, and make UPPERCASE.
	  */
	 generalizeString: function(string) {
		 return string.trim().replace(/\s+/g,'_').trim().toUpperCase();
	 },
	 unGeneralizeString: function(string) {
		 return string.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	 },

	/* @function getProcessIdentifiersFromUCJ
	 * Given the path to the .ucj and a callback, parse the ucj line by line and
	 * build up a process object.
	 * This obj is typically fed into a DB search.
	 *
	 * @param1: fileName <string>
	 * @param2: callback(err, <obj>)
	 {
		 "properties" : {
			 "unit_name" : "in",
			 "stock_type" : "tube",
			 "tube_shape": "square",
			 "material_name" : "stainless",
			 "material_type" : "304",
			 "height" : 1.5,
			 "width" : 1.5,
			 "corner_radius" : 0.1
		 },
		 "processes" : [
			 {
				 "number" : 1,
				 "name" : "cut",
				 "type" : null
			 },
			 {
				 "number" : 2,
				 "name" : "engrave",
				 "type" : "light"
			 }
		 ]
	 }

	 */
	getProcessIdentifiersFromUCJ: function(filePath, callback) {
		var readStream = fs.createReadStream(filePath);	// readable stream for the file
		var rl = readline.createInterface({			// readline interface for the stream
			input: readStream
		});
		var UCJversion;
		var process = {
			"properties" : {},
			"processes" : []
		};
		rl.on('line', function(line) {
			var matchUCJversion = line.match(/^0\.0 ~ ?(.*) ?~ 4513 set_port_ex.*$/);
			if(matchUCJversion) {
				UCJversion = parseFloat(matchUCJversion[1].trim());
			}

			if(!("unit_name" in process.properties)) {
				var matchUnit = line.match(/^(.*)4211 set_port_ex.*$/);
				if(matchUnit) {
					var matchedUnit = matchUnit[1].split('~');
					process.properties['unit_name'] = matchedUnit[1].trim().toUpperCase();
				}
			}
			if(!("stock_type" in process.properties)) {
				var matchStockType = line.match(/^(.*)4070 set_port_ex.*$/);
				if(matchStockType) {
					var matchedArray = matchStockType[1].split(' ');
					matchedArray.pop();
					switch(parseInt(matchedArray[0])){
						case 0:
							process.properties['stock_type'] = 'SHEET';
							break;
						case 1:
							process.properties['stock_type'] = 'TUBE';
							process.properties['tube_shape'] = 'ROUND';
							break;
						case 2:
							process.properties['stock_type'] = 'TUBE';
							process.properties['tube_shape'] = 'SQUARE';
							break;
						case 3:
							process.properties['stock_type'] = 'TUBE';
							process.properties['tube_shape'] = 'RECTANGLE';
							break;
						default:
							process.properties['stock_type'] = 'SHEET';
							break;
					}
				}
			}
			if(!("material_name" in process.properties)) {
				var matchMaterial = line.match(/^\d.* ~(.*)\|(.*)~ 4210 set_port_ex.*$/);
				if(matchMaterial) {
					process.properties['material_name'] = matchMaterial[1].trim().toUpperCase();
					var materialType = matchMaterial[2].trim().toUpperCase();
					if(materialType.length > 0) {
						process.properties['material_type'] = materialType;
					}
				}
			}
			var matchDimensions = line.match(/^(\d) (.*) 4071 set_port_ex.*$/);
			if(matchDimensions) {
				var dimensionValue = parseFloat(matchDimensions[2]);
				switch(parseInt(matchDimensions[1])) {
					case 0:
						process.properties['diameter'] = dimensionValue;
						break;
					case 1:
						process.properties['height'] = dimensionValue;
						break;
					case 2:
						process.properties['width'] = dimensionValue;
						break;
					case 3:
						process.properties['thickness_value'] = dimensionValue;
						break;
					case 4:
						process.properties['corner_radius'] = dimensionValue;
						break;
					default:
						break;
				}
			}
			var matchProcess = line.match(/^(\d).* ~(.*)\|(.*)~ 4600 set_port_ex.*$/);
			if(matchProcess) {
				var thisProcess = {};
				// build up thisProcess and add to process.processes
				thisProcess['number'] = parseInt(matchProcess[1]);
				thisProcess['name'] = matchProcess[2].trim().toUpperCase();
				// only add "type" key if a type is provided.
				var processType = matchProcess[3].trim().toUpperCase();
				if(processType.length > 0) {
					thisProcess['type'] = processType.substr(0).trim();
				}
				process.processes.push(thisProcess);
			}
		});
		rl.on('close', function () {
			// catch case where UCJ version is not supported. File does not parse any data.
			if(JSON.stringify(process.properties) === JSON.stringify({})) {
				callback({
					name: 'UCJ version not supported.'
				}, process);
			}
			else {
				callback(null, process);
			}
		});
	},

	 /*
	  * @function getJobFilePath
	  *
	  * Generic function for looking through the /public/uploads/jobs/fileName directory
	  * and get the file path for a certain file name.
	  *
	  * @param1: fileNameWithExtension <string>
	  * @param2: callback(<err>, <string: requestedFilePath>)
	  * 	Error is set if the file is not found.
	  */
	 getJobFilePath: function(fileName, callback) {
		 var filePath = path.join(__dirname, '/public/uploads/jobs/', fileName);
		 fs.stat(filePath, function(err, stats) {
			if(err) {
				// file does not exist
				callback(err, null);
			}
			 else {
				callback(null, filePath);
			}
		 });
	 },

	/**	@function: getCurrentGas
	 * Queries the database for the current gas that's on the machine.
	 *
	 * @param1 callback(err, <gas name as a string>)
	 */
	getCurrentGas: function (callback) {
		dbManager.db.get(SQLcommands.get_current_gas, function (err, row) {
			if(err) {
				callback(err, null);
			}
			else {
				callback(null, row.gas);	
			}
		});
	},

	 /** @function getJobBoundsFromProcessFile
	  * Get job bounds from process.ucj file.
	  *
	  * @param1 fileName {String} file name without any extension
	  * @param2 callback
	  */
	 getJobBoundsFromProcessFile: function(fileName, callback) {

		 var filePath = path.join(usbManager.jobPath.folder, usbManager.jobPath.process);
		 var readStream = fs.createReadStream(filePath);	// create readable stream for the file
		 var rl = readline.createInterface({			// create readline interface for the stream
			 input: readStream
		 });

		 var bounds = {};
		 // read the job file line by line
		 rl.on('line', function(line) {
			 // use regex to find the bounding info. example:
			 // 1 0.98750 4011 set_port_ex ` Y max
			 var boundsMatch = line.match(/^([0-1]) (.*) (401[0-1]) set_port_ex.*$/);
			 if (boundsMatch) {
				 var axis = boundsMatch[1];
				 var value = boundsMatch[2];
				 var port = boundsMatch[3];

				 switch(parseInt(port)) {
					 case 4010:
						 // axis minimums
						 if(axis === '0') {
							 bounds['xMin'] = value;
						 }
						 else if(axis === '1') {
							 bounds['yMin'] = value;
						 }
						 break;
					 case 4011:
						 // axis maximums
						 if(axis === '0') {
							 bounds['xMax'] = value;
						 }
						 else if(axis === '1') {
							 bounds['yMax'] = value;
							 rl.close();	// this is the last bounds value in the UCJ
						 }
						 break;
					 default:
						 break;
				 }
			 }
		 });
		 rl.on('close', function () {
			 callback(null, bounds);
		 });
		 rl.on('error', function(err) {
			 callback(err)
		 });
	 },

	 convertToMetric: {
		 kerf: function(kerf) {
			 return module.exports.round(convert(kerf).from('in').to('mm'), 2);
		 },
		 feedrate: function(feedrate) {
			 return module.exports.round(feedrate * 25.4, 0);
		 },
		 pressure: function(pressure) {
			 return module.exports.round(convert(pressure).from('psi').to('bar'), 1);
		 },
		 nozzle_gap: function(nozzle_gap) {
			 return module.exports.round(convert(nozzle_gap).from('in').to('mm'), 1);
		 },
		 focus_offset: function(focus_offset) {
			 return module.exports.round(convert(focus_offset).from('in').to('mm'), 1);
		 },
		 pierce_nozzle_gap: function(pierce_nozzle_gap) {
			 return module.exports.round(convert(pierce_nozzle_gap).from('in').to('mm'), 1);
		 },
		 pierce_pressure: function(pierce_pressure) {
			 return module.exports.round(convert(pierce_pressure).from('psi').to('bar'), 1);
		 }
	 },

	 convertToInches: {
		 kerf: function(kerf) {
			 return module.exports.round(convert(kerf).from('mm').to('in'), 3);
		 },
		 feedrate: function(feedrate) {
			 return module.exports.round(feedrate / 25.4, 1);
		 },
		 pressure: function(pressure) {
			 return module.exports.round(convert(pressure).from('bar').to('psi'), 0);
		 },
		 nozzle_gap: function(nozzle_gap) {
			 return module.exports.round(convert(nozzle_gap).from('mm').to('in'), 2);
		 },
		 focus_offset: function(focus_offset) {
			 return module.exports.round(convert(focus_offset).from('mm').to('in'), 2);
		 },
		 pierce_nozzle_gap: function(pierce_nozzle_gap) {
			 return module.exports.round(convert(pierce_nozzle_gap).from('mm').to('in'), 2);
		 },
		 pierce_pressure: function(pierce_pressure) {
			 return module.exports.round(convert(pierce_pressure).from('bar').to('psi'), 0);
		 }
	 },

	 /** Given a complete process object, generate a display object whose laser parameter values match
	  * the current value in the machine_settings table in the db.
	  *
	  * @param pObj
	  * @param cb(<displayProcessObjct>) - complete process object
	  */
	 generateDisplayProcessObject: function (pObj, cb) {
		 var displayProcess = JSON.parse(JSON.stringify(pObj));
		dbManager.getDisplayUnit(function(machineUnit) {
			// Iterate over each process in the process object.
			var currentProcess;
			for(var i = 0; i < displayProcess.processes.length; i++) {
				currentProcess = displayProcess.processes[i];
				if("params" in currentProcess) {
					// For each process, check if the machine unit matches the laser_parameter unit.
					if(currentProcess.params.unit !== machineUnit) {
						if(machineUnit === "MM") {
							// machine is MM, process is IN. Convert process to MM.
							displayProcess.processes[i].params = module.exports.convertParamUnits(pObj.processes[i].params, "MM");
						}
						else {
							// machine is IN, process is MM. Convert process to IN.
							displayProcess.processes[i].params = module.exports.convertParamUnits(pObj.processes[i].params, "IN");
						}
					}
					else {
						// If the machine unit and the laser_parameter units are the same, just round the params for display.
						displayProcess = module.exports.roundProcessObj(pObj);
					}
				}
			}
			cb(displayProcess);
		});
	 },

	 /** Given an object containing all parameters, return a new object with all of the units converted
	  * to the requested unit.
	  *
	  * @param paramObj {object} Contains all parameters
	  * @param unit {string} Unit to convert TO, either "MILLIMETER" or "INCHES"
	  */
	 convertParamUnits: function(paramObj, unit) {
		 var newParams = JSON.parse(JSON.stringify(paramObj));
		 var convertObj;
		 if(unit === "MM") {
			 convertObj = module.exports.convertToMetric;
		 }
		 else {
			 convertObj = module.exports.convertToInches;
		 }
		 // Convert each of the parameters using the appropriate convert function object
		 for(var toConvert in convertObj) {
			 var currentValue = newParams[toConvert];
			 newParams[toConvert] = convertObj[toConvert](currentValue);
		 }
		 return newParams;
	 },

	 roundParametersIN: {
		 kerf: function(kerf) {
			 return module.exports.round(kerf, 3);
		 },
		 feedrate: function(feedrate) {
			 var roundedFeedrate;
			 // Round to 2 decimals. If this ends up being 0, then set to a hardcoded min feedrate value.
			 roundedFeedrate = module.exports.round(feedrate, 2);
			 roundedFeedrate = (roundedFeedrate === 0)? 0.01 : roundedFeedrate;
			 return roundedFeedrate;
		 },
		 pressure: function(pressure) {
			 return module.exports.round(pressure, 0);
		 },
		 nozzle_gap: function(nozzle_gap) {
			 return module.exports.round(nozzle_gap, 3);
		 },
		 focus_offset: function(focus_offset) {
			 return module.exports.round(focus_offset, 2);
		 },
		 pierce_nozzle_gap: function(pierce_nozzle_gap) {
			 return module.exports.round(pierce_nozzle_gap, 2);
		 },
		 pierce_pressure: function(pierce_pressure) {
			 return module.exports.round(pierce_pressure, 0);
		 }
	 },

	 roundParametersMM: {
		 kerf: function(kerf) {
			 return module.exports.round(kerf, 2);
		 },
		 feedrate: function(feedrate) {
			 var roundedFeedrate;
			 // Round to 1 decimal. If this ends up being 0, then set to a hardcoded min feedrate value.
			 roundedFeedrate = module.exports.round(feedrate, 1);
			 roundedFeedrate = (roundedFeedrate === 0)? 0.01 : roundedFeedrate;
			 return roundedFeedrate;
		 },
		 pressure: function(pressure) {
			 return module.exports.round(pressure, 1);
		 },
		 nozzle_gap: function(nozzle_gap) {
			 return module.exports.round(nozzle_gap, 1);
		 },
		 focus_offset: function(focus_offset) {
			 return module.exports.round(focus_offset, 1);
		 },
		 pierce_nozzle_gap: function(pierce_nozzle_gap) {
			 return module.exports.round(pierce_nozzle_gap, 1);
		 },
		 pierce_pressure: function(pierce_pressure) {
			 return module.exports.round(pierce_pressure, 1);
		 }
	 },


	 /** Given a complete process object, return a new object with all of the parameters rounded to fewer
	  * decimal points. The rounding strategy depends on the current unit of the laser parameters.
	  * 
	  * @param pObj
	  */
	 roundProcessObj: function(pObj) {
		 var newObj = JSON.parse(JSON.stringify(pObj));
		 // Iterate through each of the processes and round them
		 for(var i = 0; i < newObj.processes.length; i++) {
			 var currentProcess = newObj.processes[i];
			 // If the params exist, then round each of them according to MM or IN rules,
			 // based on the current unit of the params.
			 if("params" in currentProcess) {
				 var toRound;
				 if(currentProcess.params.unit === "MM") {
					 toRound = module.exports.roundParametersMM;
				 }
				 else {
					 toRound = module.exports.roundParametersIN;
				 }
				 // Actually round each of the listed params in the roundParametersIN/MM variables
				 for(var toConvert in toRound) {
					 var currentValue = currentProcess.params[toConvert];
					 // Only round the parameter value if it's not null to prevent NaN values in the process table
					 if(currentValue != null) {
						 currentProcess.params[toConvert] = toRound[toConvert](currentValue);
					 }
				 }
			 }
			 // Update the newObj with the rounded process
			 newObj.processes[i] = currentProcess;
		 }
		 return newObj;
	 },

	 round: function (value, decimals) {
		 return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
	 },

	 /* getFileSize
	  *	Given a file path, return the file size.
	  *
	  *	callback(fileSize)
	  */
	 getFileSize: function(filePath, callback) {
		 // get the file size
		 fs.stat(filePath, function(err, stat) {
			 if(err) {
				 throw err;
			 }
			 callback(stat.size);
		 });
	 },
	 
	 masterProcessObj: {},
	 temporaryProcessObj: {}
};