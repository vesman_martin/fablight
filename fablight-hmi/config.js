/*
 * config.js
 *
 * Configuration for app, mainly to decide what port to use.
 * env = 'development' means running on the Mac and
 * uses ports 1323 and 1329 on localhost (127.0.0.1)
 * while env = 'production' means running on the UDOO and
 * uses ports 23 and 29 on the fablight's static IP, 192.168.3.232.
 *
 * DEMO_MODE is set to true to allow users to use the HMI without a machine connected.
 * All of the features are available. This is useful for testing the UI.
 *
 * This file is also used to set up the sockets in the /socket routes.
 *
 */

var appRoot = require('app-root-path');
var packageJSON = appRoot.require('package.json');
var getos = require('getos')

var env = process.env.NODE_ENV || 'development';	// 'development' or 'production'
var demoMode = process.env.DEMO_MODE || true;	// true or false
if(typeof demoMode === 'string') {
	demoMode = (demoMode === 'true');
}

console.log('[HMI BOOT] env: ' + env + ' ver: ' + packageJSON.version);

var os = getos(function(e,os) {
	if(e) return console.log(e)
	return os
})
/* os is of the form:
{
  os: "linux",
  dist: "Ubuntu Linux",
  codename: "precise",
  release: "12.04"
}
OR (on mac)
{ os: 'darwin' }
 */

// Running locally on Mac
var development = {
	server: {
		enabled: true,
		path: '/public/uploads/'
	},
	laser: {
		job_port: 1323,
		command_port: 1333,
		info_port: 1329,
		ip_address: "127.0.0.1",
		udp_ping_port: 6201,
		udp_listen_port: 6202
	},
	logging: {
		file_name: 'fablight-hmi.log',
		location: '/var/log/'
	},
	production: false,
	demoMode: true,
	os: os
};

// Running on UDOO
var production = {
	server: {
		enabled: true,
		path: '/opt/fabserver/public/uploads/'
	},
	laser: {
		job_port: 23,
		command_port: 33,
		info_port: 29,
		ip_address: "192.168.3.232",
		udp_ping_port: 6201,
		udp_listen_port: 6202
	},
	logging: {
		file_name: 'fablight-hmi.log',
		location: '/var/log/'
	},
	production: true,
	demoMode: false,
	os: os
};

var config;
config = {
	development,
	production
};

if(typeof env === 'undefined') {
	module.exports = config['development'];
}
else {
	module.exports = config[env];
}
