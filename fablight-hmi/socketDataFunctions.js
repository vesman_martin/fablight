/*
 * socketDataFunctions.js
 *
 * Contains all functions used in the on.('data', ..) event for the TCP socket.
 * Added to this separate file due to anonymous function in listener.
 * Check index.js for context.
 *
 */

var status = require('./machineStatus');
var config = require('./config');

// load modules
var fs = require('fs');
var readline = require('readline');
var path = require('path');
var util = require('util');
var convert = require('convert-units');
var moment = require('moment');
var timemachine = require('timemachine');
var appRoot = require('app-root-path');
var driveList = require('drivelist');

// process table database from the dbManager.js file
var dbManager = appRoot.require('dbManager');
var processTableHelper = appRoot.require('processTableHelper');
var SQLcommands = appRoot.require(path.join('db', 'SQLiteCommands', 'SQLiteCommands'));
var usbManager = appRoot.require('usbManager');
var logger = appRoot.require('logger');

module.exports = {
	/*
	 * @handleStream23 - handles lines (\n) of data recieved from TCP socket 23
	 * Lines of data are buffered with split() and piped into this function from
	 * index.js
	 *
	 */
	handleStream23: function(chunk, res) {
		// console.log('23>' + chunk);
		res.io.emit('port23-data', chunk);
	},

	/*
	 * @handleStream33 - handles lines (\n) of data received from TCP socket 33
	 * Lines of data are buffered with split() and piped into this function from index.js
	 */
	handleStream33: function(chunk, command, res) {
		var lowTempLogged = false;
		// The controller sends over a timestamp.
		if(command.cmd === 'fl_time') {
			var controllerTime = command.d;
			var controllerMoment = moment(controllerTime, 'YYYY-MM-DD HH:mm:ss');
			// Use timemachine module to change the date and time of the HMI application to match the controller.
			// timemachine requires a dateString of the format 'December 25, 1991 13:12:59'
			timemachine.config({
				dateString: controllerMoment.format('MMMM D, YYYY H:mm:ss'),
				tick: true
			});
			logger.log('- current controller time')
		}

		// fl_find_limits
		// {cmd:fl_find_limits,s:1,err:false or code}
		else if(command.cmd === 'fl_find_limits') {
			status.machineHomed = true;
			res.io.emit('fl_find_limits__status', command.s);
		}
		// fl_p - get machine coordinates
		else if(command.cmd === 'fl_p') {
			status.position.x = command.d.x;
			status.position.y = command.d.y;
			res.io.emit('fl_p_result', command);
		}
		else if(command.cmd === 'fl_park') {
				res.io.emit('fl_park', command);
		}
		// fl_rot - switch to rotary mode
		else if(command.cmd === 'fl_rot') {
			if(parseInt(command.s) === 1) {
				res.io.emit('fl_rot_result', {
					s: parseInt(command.s),
					e: command.e
				});
			}
		}
		else if(command.cmd === 'fl_gas') {
			res.io.emit('fl_gas', command.d);
		}
		// fl_m2a - 2D absolute move
		else if(command.cmd === 'fl_m2a') {
			if(parseInt(command.e) === 9) {
				res.io.emit('fl_m2a_error', command.e);
			}
			if(parseInt(command.s) === 2) {
				res.io.emit('fl_m2a_complete', command);
			}
		}
		// fl_m2a - 1D absolute move
		else if(command.cmd === 'fl_m1a') {
			if(parseInt(command.e) === 9) {
				res.io.emit('fl_m1a_error', command.e);
			}
		}

		// fl_sh_rot_here - homing the rotary
		else if(command.cmd === 'fl_sh_rot_here') {
			res.io.emit('fl_sh_rot_here', {
				e: command.e
			});
		}

		// fl_sh_xy_here - set the XY home
		else if(command.cmd === 'fl_sh_xy_here') {
			res.io.emit('fl_sh_xy_here', {
				e: command.e
			});
		}

		else if(command.cmd === 'fl_alert') {
			var levels = ['debug', 'info', 'warn', 'error', 'fault'];
			var fl_alert = command;
			logger.log(JSON.stringify(fl_alert), false);
			// Only show the alert as a pop up if the level is a warning or higher.
			// Pages that have the statusDialog.js file will be listening for this event.
			var currentLevel = levels.indexOf(fl_alert.level);
			if(currentLevel >= 2) {
				var message = fl_alert.message ? fl_alert.message : '';
				var title = fl_alert.title ? fl_alert.title : '';
				noticeObject = {
					time: fl_alert.time ? fl_alert.time : '',
					level: fl_alert.level ? fl_alert.level : '',
					title: title,
					label: fl_alert.label ? fl_alert.label : '',
					message: fl_alert.message ? fl_alert.message : '',
					cmd: fl_alert.cmd ? fl_alert.cmd : ''
				};
				res.io.emit('fl_alert', noticeObject);
			}
		}

		/* fl_status - get machine status */
		else if(command.cmd === 'fl_status') {
			var fl_status = JSON.parse(chunk.substr(chunk.indexOf('{"cmd')));
			var stat = {
				status: {
					homed: 				(fl_status.d & 0x0001) !== 0,
					limit_detect: 		(fl_status.d & 0x0002) !== 0,
					drive_fault: 		(fl_status.d & 0x0004) !== 0,
					laser_err: 			(fl_status.d & 0x0008) !== 0,
					rot_mode: 			(fl_status.d & 0x0010) !== 0,
					paused: 			(fl_status.d & 0x0100) !== 0,
					running: 			(fl_status.d & 0x0200) !== 0,
					ready: 				(fl_status.d & 0x0400) !== 0,
					chs_calibrated: 	(fl_status.d & 0x0800) !== 0,
					calibrating: 		(fl_status.d & 0x1000) !== 0,
					wait_keypress:		(fl_status.d & 0x2000) !== 0,
					door_open:			(fl_status.d & 0x4000) !== 0
				},
				pause_reason: fl_status.p,
				fault_reason: {
					limitX:				(fl_status.f & 0x0001) !== 0,
					limitY:				(fl_status.f & 0x0002) !== 0,
					limitZ:				(fl_status.f & 0x0004) !== 0,
					limitR:				(fl_status.f & 0x0008) !== 0,
					driveFaultX:		(fl_status.f & 0x0010) !== 0,
					driveFaultY:		(fl_status.f & 0x0020) !== 0,
					driveFaultR:		(fl_status.f & 0x0080) !== 0,
					palletOut:			(fl_status.f & 0x0100) !== 0,
					OOBXY:				(fl_status.f & 0x0200) !== 0,
					CHSwrongV:			(fl_status.f & 0x0400) !== 0,
					noMaterial:			(fl_status.f & 0x0800) !== 0,
					calibrationFailed: 	(fl_status.f & 0x1000) !== 0,
					OOBIdler:			(fl_status.f & 0x2000) !== 0,
					tubeWrongSize:		(fl_status.f & 0x4000) !== 0,
					nozzleOffFault:		(fl_status.f & 0x8000) !== 0
				}
			};

			var noticeObject = {};
			var noticeTitle = '';	// title of the event, will show up in the status dialog
			var desc = '';			// event description helptext, will show up in the dialog
			var tmp = false;		// flag for dialog, if true will auto-close dialog after X seconds

			// fl_status__not-homed and fl_status__homed are for the index page
			if(stat.status.homed === false) {
				res.io.emit('fl_status__not-homed');
			}
			else if(stat.status.homed === true) {
				res.io.emit('fl_status__homed');
			}

			// used on jogging screen
			if(stat.status.ready === true) {
				status.machineState = 1;
				if(stat.status.wait_keypress === true) {
					res.io.emit('fl_status__ready', {
						waitingForKeypress: true
					});
				}
				else {
					res.io.emit('fl_status__ready', {
						waitingForKeypress: false
					});
				}
				if(stat.status.calibrating === true) {
					noticeTitle = 'Capacitive Sensor is calibrating...';
					desc = 'Please wait while the sensor calibrates.';
				}
				if(stat.status.chs_calibrated === false) {
					noticeTitle = 'Capacitive Sensor needs to be calibrated.';
					desc = 'Go to the settings page and calibrate the CHS.';
				}
			}
			else if(stat.status.paused === true) {
				res.io.emit('fl_status__paused');
				status.machinePaused = true;
				status.machineState = 2;
			}
			else if(stat.status.running === true) {
				if(status.machinePaused === true) {
					// The machine was paused previously, and now it's running
					res.io.emit('fl_status__continue');
				}
				status.machinePaused = false;
				res.io.emit('fl_status__running');
			}

			// Door open code
			if(parseInt(stat.pause_reason) === 6) {
				res.io.emit('door-open');
			}
			else {
				res.io.emit('door-closed');
			}

			// lastly, emit the fl_status__notice event if the title is populated.
			// Pages that have the statusDialog.js file will be listening for this event.
			if(noticeTitle.length > 0) {
				noticeObject['noticeTitle'] = noticeTitle;
				noticeObject['desc'] = desc;
				noticeObject['tmp'] = tmp;
				res.io.emit('fl_status__notice', noticeObject);
			}
			//console.log('[SDF][hS33]' + util.inspect(fl_status));
			// also emit the fl_status event for the index page.
			res.io.emit('fl_status', stat);
		}

		// fl_h - for homing a single axis. Usually the rotary.
		else if(command.cmd === 'fl_h') {
			res.io.emit('fl_h', {
				s: parseInt(command.s),
				e: command.e
			});
		}

		/* {
				“cmd”: “fl_job_info”,
				“job”: <”start” or “end” string>,
				“time”: <timedate string>,		// time at start or end of job, depending on the “job” value
				“d”: {
					“tip_touch”: <int>,
					“tip_touch_pause”: <int>,
					“job_end_info”: <string>	// why the job was ended. “No errors” if completed normally
				}
			}
		 */
		else if(command.cmd === 'fl_job_info') {
			if(command.job.toLowerCase() === 'start') {
				status.job.startTime = moment(command.time, 'YYYY-MM-DD HH:mm:ss');
				logger.log('----------------------------------------------------');
				logger.log('\t\t\t\t\t |----- Job start at:\t\t' + command.time, false);
				logger.log('\t\t\t\t\t ----------------------------------------------------', false);
				res.io.emit('fl_job_info', {
					job: command.job // 'start' or 'stop'
				});
			}
			else {
				// When the job is quit, write an fl_status. This is mostly to clear the door state.
				module.exports.writeToLaser(33, 'fl_status');
				// Calculate the elapsed time:
				status.job.endTime = moment(command.time, 'YYYY-MM-DD HH:mm:ss');
				var elapsedJobTime = status.job.endTime.diff(status.job.startTime);
				elapsedJobTime = moment.utc(elapsedJobTime).format('HH:mm:ss');
				logger.log('|----- Job end at:\t\t\t' + command.time);
				logger.log('\t\t\t\t\t |----- Elapsed time:\t\t' + elapsedJobTime, false);
				logger.log('\t\t\t\t\t |----- Tip touches (total):\t' + command.d.tip_touch, false);
				logger.log('\t\t\t\t\t |----- Tip touches (paused):\t' + command.d.tip_touch_pause, false);
				logger.log('\t\t\t\t\t |----- Job end info:\t\t' + command.d.job_end_info, false);
				logger.log('\t\t\t\t\t |----- Z axis check:\t\t' +
					((typeof command.d.z_axis_check === 'undefined') ? 'N/A' : command.d.z_axis_check), false);
				logger.log('\t\t\t\t\t |----- CHS at start of job:\t' +
					((typeof command.d.chs_at_soj === 'undefined') ? 'N/A' : command.d.chs_at_soj), false);
				res.io.emit('fl_job_info', {
					job: command.job // 'start' or 'stop'
				});
			}
		}

		// fl_abort_job is sent when the job is aborted
		else if(command.cmd === 'fl_abort_job') {
			var abortJobReasons = {
				0: "No error",
				1: "User Canceled",
				2: "Limit detect",
				3: "Nozzle off",
				4: "E-stop",
				5: "Laser error",
				6: "Door opened",
				7: "Out of bounds",
				8: "Unknown",
				9: "No material",
				10: "Pallet error"
			};
			logger.log('|----- Job aborted. Reason:\t' + abortJobReasons[command.d]);
			logger.log('----------------------------------------------------');
		}

		// fl_go_home
		else if(command.cmd === 'fl_go_home') {
			res.io.emit('fl_go_home', parseInt(command.s));
		}

		// Commands below are used on settings page.

		// fl_cal_caphead - calibrating the CHS
		else if(command.cmd === 'fl_cal_caphead') {
			res.io.emit('fl_cal_caphead', parseInt(command.s));
		}

		// fl_purge_gas - run out gas for 5s.
		else if(command.cmd === 'fl_purge_gas') {
			res.io.emit('fl_purge_gas', parseInt(command.s));
		}

		// x. y. fl_focus_test - run focus test at that position
		else if(command.cmd === 'fl_focus_test') {
			res.io.emit('fl_focus_test', parseInt(command.s));
		}

		// get current focus offset for display, with 2 digits passed the decimal
		else if(command.cmd === 'fl_get_focus_offset') {
			res.io.emit('fl_get_focus_offset', parseFloat(command.d));
		}

		else if(command.cmd === 'fl_beam_centering') {
			res.io.emit('fl_beam_centering', parseInt(command.s));
		}

		else if(command.cmd === 'fl_rot_align_theta') {
			res.io.emit('fl_rot_align_theta', parseInt(command.s));
		}

		else if(command.cmd === 'fl_rot_align_y') {
			res.io.emit('fl_rot_align_y', parseInt(command.s));
		}

		else if(command.cmd === 'fl_check_laser_low_temp') {
			res.io.emit('fl_check_laser_low_temp', command.d);
		}

		else if(command.cmd === 'check_cal') {
			res.io.emit('check_cal', command);
		}
		else if(command.cmd === 'fl_init_version') {
			res.io.emit('fl_init_version', command.d);
		}
		else if (command.cmd === 'fl_reboot') {
			logger.log('[fl_reboot] Controller rebooted.');
			// Disconnect the HMI from the controller because the controller is rebooting.
			status.laserSocket33.destroy();
			status.laserSocket33.unref();
			status.laserSocket23.destroy();
			status.laserSocket23.unref();
			res.io.emit('machine_offline')
			status.initLoaded = false;
			status.isOnline = false;
			status.TCPconnected = false;
		}
	},

	/* =========== Write command to TCP laser socket ===========
	 *
	 * @writeToLaser - write to the laser socket. Wrapper function to allow demoMode.
	 * Optional callback with error argument.
	 */
	writeToLaser: function(socketNumber, message, callback) {
		//Add a newline to the message
		message = message + '\r\n';
		if(socketNumber === 33) {
			if (status.laserSocket33 !== null) {
				if ((status.connectionState & status.port33flag) !== 0) {
					status.laserSocket33.write(message);
				}
			}
			else {
				var err = 'Port 33 is closed. Can\'t write command.';
				logger.log('[HMI] ' + err);
				if(callback !== undefined) {
					callback(err);
				}
			}
		}
	},

	/* =========== Handle socket.io events ===========
	 *
	 * @handleSocketIO - handles socket.io events received by node.js server.
	 * entry point of this function is in app.js
	 * 
	 * socket name convention:
	 * view-backEnd__eventName
	 */
	handleSocketIO: function(socket) {

		// handle socket.io errors.
		socket.on('error', function(err){
			logger.log('[HMI] Socket.io error: '+err);
		});

		/* ---------- EVENT: check_fabserver ----------
		 * From home page. Check if the fabserver is connected.
		 */
		socket.on('check_fabserver', function () {
			driveList.list(function(error, drives) {
				if (error) { throw error; }
				else {
					console.log(drives)
				}
			})
		});

		/* ---------- EVENT: process_pin_entered ----------
 		* Keep track of if the process pin has been entered or not.
 		*/
		socket.on('process_pin_entered', function (bool) {
			status.processes.processPinEntered = bool;
		});

		/* ---------- EVENT: fl_find_limits ----------
		 * From index page.
		 * Emitted after the user presses the home machine button
		 */
		socket.on('fl_find_limits', function () {
			module.exports.writeToLaser(33, 'fl_find_limits')
		});

		/* ---------- EVENT: fl_sh_rot_here ----------
			 * From jog3d page.
			 * Emitted after the user presses the send to laser button.
			 */
		socket.on('fl_sh_rot_here', function () {
			status.laserSocket33.write('fl_sh_rot_here\r\n');
		});
		/* ---------- EVENT: fl_sh_xy_here ----------
		 * From jog2d page.
		 * Emitted after the user presses the send to laser button.
		 */
		socket.on('fl_sh_xy_here', function () {
			status.laserSocket33.write('fl_sh_xy_here\r\n');
		});

		/* ---------- EVENT: index-tcp33__findLimits ----------
		 * From index page.
		 * Emitted after the user presses the home machine button.
		 */
		socket.on('index-tcp33__findLimits', function() {
			module.exports.writeToLaser(33, 'fl_find_limits');
		});

		/* ---------- EVENT: settings_gas-selected ----------
		 * From settings page.
		 * Data: selectedGas <string>
		 */
		socket.on('settings_gas-selected', function (gas) {
			var SQLchangeGas = 'UPDATE machine_settings SET gas = \'' + gas + '\';';
			dbManager.db.run(SQLchangeGas, function (err) {
				if(err) throw err;
				else {
					socket.emit('settings_gas-selected-complete');
				}
			});
		});

		/* ---------- EVENT: settings_enable-process-pin ----------
		 * Data: enablePin <boolean>
		 * Returns true/false of the current pin enabled status
		 */
		socket.on('settings_enable-process-pin', function (enablePin) {
			dbManager.enableProcessPin(enablePin, function (err) {
				if(err) throw err;
				else {
					socket.emit('settings_enable-process-pin_complete', enablePin);
				}
			})
		});

		/* ---------- EVENT: settings_get-process-pin-settings ----------
 		* Get the pin number and if it's enabled or not from the database
 		* 	{
	 	* 		process_pin_enabled: <int, 0 or 1>
	 	* 		process_pin: <int>
	 	* 	}
 		*/
		socket.on('settings_get-process-pin-settings', function () {
			dbManager.getProcessPinSettings(function (err, settings) {
				if(err) throw err;
				else {
					socket.emit('settings_get-process-pin-settings', settings);
				}
			})
		});

		/* ---------- EVENT: settings_change-process-pin ----------
		 * Data: newPin <integer>
		 */
		socket.on('settings_change-process-pin', function (newPin) {
			dbManager.setProcessPin((newPin), function (err) {
				if(err) throw err;
				else {
					socket.emit('settings_change-process-pin_complete');
				}
			});
		});

		/* ---------- EVENT: settings_unit-selected ----------
		 * From settings page.
		 * Data: selectedUnit <string>
		 */
		socket.on('settings_unit-selected', function (unit) {
			var SQLchangeUnit = 'UPDATE machine_settings SET display_unit_system = \'' + unit + '\';';
			dbManager.db.run(SQLchangeUnit, function (err) {
				if(err) throw err;
				else {
					socket.emit('settings_unit-selected-complete');
				}
			});
		});

		/* ---------- EVENT: jog-jog__XY ----------
		 * From jog.pug page
		 * Emitted after the user releases either of the jog handles.
		 * Used in the 2D jogging screen.
		 * @param {object} position - object with x and y positions
		 * @param {string} unit (optional) - either "MM" or "IN"
		 */
		socket.on('jog-jog__XY', function(position, unit) {
			if(status.laserSocket33 != null) {
				var xPos = processTableHelper.convertToUCJFloat(position.x);
				var yPos = processTableHelper.convertToUCJFloat(position.y);
				if(unit == "MM") {
					xPos = processTableHelper.convertToUCJFloat(convert(xPos).from('mm').to('in'));
					yPos = processTableHelper.convertToUCJFloat(convert(yPos).from('mm').to('in'));
				}
				var jogPos = xPos + ' ' + yPos + ' fl_m2a\n ';
				module.exports.writeToLaser(33, jogPos);
			}
		});

		/* ---------- EVENT: jog-jog__X ----------
		 * From jog.pug page
		 * Emitted after the user releases the X jog handle on Rotary jog3d.
		 */
		socket.on('jog-jog__X', function(xPos, unit) {
			if(status.laserSocket33 != null) {
				if(unit == "MM") {
					xPos = processTableHelper.convertToUCJFloat(convert(xPos).from('mm').to('in'));
				}
				var jogPos = processTableHelper.convertToUCJFloat(xPos) + ' 0 fl_m1a\n ';
				module.exports.writeToLaser(33, jogPos);
			}
		});

		/* ---------- EVENT: jog-jog__Y_relative ---------- */
 		socket.on('jog-jog__Y_relative', function(yPos, unit) {
			if(status.laserSocket33 != null) {
				if(unit == "MM") {
					yPos = processTableHelper.convertToUCJFloat(convert(yPos).from('mm').to('in'));
				}
				var jogPos = processTableHelper.convertToUCJFloat(yPos) + ' 1 fl_m1r\n ';
				module.exports.writeToLaser(33, jogPos);
			}
		});

		/* ---------- EVENT: fl_status ----------
		 * Requests the machine status
		 */
		socket.on('fl_status', function() {
			module.exports.writeToLaser(33, 'fl_status', function (err) {
				if(err) {
					socket.emit('writeToLaser_err', err);
				}
			});
		});

		/* ---------- EVENT: fl_check_errors ----------
		 * Requests the machine status after booted
		 */
		socket.on('fl_check_errors', function() {
			module.exports.writeToLaser(33, 'fl_check_errors');
		});

		/* ---------- EVENT: fl_check_errors ----------
		 * Requests the machine status after booted
		 */
		socket.on('fl_init_version', function() {
			module.exports.writeToLaser(33, 'fl_init_version');
		});


		/* ---------- EVENT: controller_command ----------
		 * Send a command to the controller
		 */
		socket.on('controller_command', function (data) {
			module.exports.writeToLaser(33, data);
		});

		/* ---------- EVENT: fl_p ----------
		 * From jog page.
		 * Emitted after page loads.
		 * Requests the machine position
		 */
		socket.on('fl_p', function() {
			module.exports.writeToLaser(33, 'fl_p');
		});

		/* ---------- EVENT: lift_head ----------
		 * Lift the head up in the Z axis
		 */
		socket.on('lift_head', function() {
			module.exports.writeToLaser(33, '2 fl_h');
		});

		/* ---------- EVENT: settings__fl_rot_align_y ----------
		 * From settings page.
		 * Starts the align Y macro
		 */
		socket.on('fl_rot_align_y', function() {
			module.exports.writeToLaser(33, 'fl_rot_align_y');
		});

		/* ---------- EVENT: settings__fl_rot_align_theta ----------
		 * From settings page.
		 * Starts the align theta macro
		 */
		socket.on('fl_rot_align_theta', function() {
			module.exports.writeToLaser(33, 'fl_rot_align_theta');
		});

		/* ---------- EVENT: fl_park ----------
		 * From index page. Used to park the axes.
		 */
		socket.on('fl_park', function() {
			if(status.laserSocket33 != null) {
				module.exports.writeToLaser(33, 'fl_park');
			}
		});

		/* ---------- EVENT: go_home ----------
		 * From index page. Used to go to the home position.
		 */
		socket.on('go_home', function() {
			module.exports.writeToLaser(33, 'gohome');
		});

		/* ---------- EVENT: fl_go_home ----------
		 * From index page. Used to go to the home position.
		 */
		socket.on('fl_go_home', function() {
			module.exports.writeToLaser(33, 'fl_go_home');
		});

		/* ---------- EVENT: fl_purge_gas ----------
		 * From settings page. Used to purge the gas.
		 */
		socket.on('fl_purge_gas', function() {
			module.exports.writeToLaser(33, 'fl_purge_gas');
		});

		/* ---------- EVENT: fl_focus_test ----------
		 * From settings page. Used to run the focus test at a given position obj with x and y.
		 * If no position is provided, then run at the current machine position.
		 * This position is set in machineSettings.js from the fl_p command
		 */
		socket.on('fl_focus_test', function(pos) {
			if(typeof pos !== 'undefined') {
				module.exports.writeToLaser(33,
					processTableHelper.convertToUCJFloat(pos.x) + ' ' +
					processTableHelper.convertToUCJFloat(pos.y) + ' fl_focus_test');
			}
			else {
				module.exports.writeToLaser(33,
					processTableHelper.convertToUCJFloat(status.position.x) + ' ' +
					processTableHelper.convertToUCJFloat(status.position.y) + ' fl_focus_test');
			}
		});

		/* ---------- EVENT: fl_set_focus_offset ----------
		 * From settings page. Used to set a new focus position.
		 */
		socket.on('fl_set_focus_offset', function (offset) {
			module.exports.writeToLaser(33,
				processTableHelper.convertToUCJFloat(offset) + ' fl_set_focus_offset');
			module.exports.writeToLaser(33, 'fl_get_focus_offset');
		});
		// Get the current focus offset position
		socket.on('fl_get_focus_offset', function () {
			module.exports.writeToLaser(33, 'fl_get_focus_offset');
		});

		/* ---------- EVENT: fl_beam_centering ----------
		 * From settings page. Used to run the beam centering test.
		 */
		socket.on('fl_beam_centering', function(pos) {
			module.exports.writeToLaser(33, processTableHelper.convertToUCJFloat(pos.x) + ' ' +
				processTableHelper.convertToUCJFloat(pos.y) + ' fl_beam_centering');
		});

		/* ---------- EVENT: fl_xy ----------
		 * Puts the machine in XY mode
		 */
		socket.on('fl_xy', function() {
			module.exports.writeToLaser(33, 'fl_xy');
		});

		/* ---------- EVENT: fl_rot ----------
		 * Puts the machine in rot mode
		 */
		socket.on('fl_rot', function() {
			module.exports.writeToLaser(33, 'fl_rot');
		});

		/* ---------- EVENT: fl_cal_caphead ----------
		 * starts the calibration routine.
		 * Moves to XY mode 0,0 and starts calibrating.
		 */
		socket.on('fl_cal_caphead', function(position) {
			var xPos = processTableHelper.convertToUCJFloat(position.x);
			var yPos = processTableHelper.convertToUCJFloat(position.y);
			var calPos = xPos + ' ' + yPos + ' fl_m2a\n ';
			module.exports.writeToLaser(33, 'fl_xy\n' + calPos + 'fl_cal_caphead');
		});

		/* ---------- EVENT: check_cal ----------
		 * Check the calibration
		 * Moves to (3, 3) and starts calibrating.
		 */
		socket.on('check_cal', function() {
			var xPos = processTableHelper.convertToUCJFloat(3);
			var yPos = processTableHelper.convertToUCJFloat(3);
			var calPos = xPos + ' ' + yPos + ' fl_m2a\n ';
			module.exports.writeToLaser(33, 'fl_xy\n' + calPos + 'check_cal');
		});

		/* ---------- EVENT: fl_check_laser_temp ----------
 		* Check the laser temperature. Used to check if the temperature is low.
 		*/
		socket.on('fl_check_laser_temp', function() {
			module.exports.writeToLaser(33, 'fl_check_laser_temp');
		});

		/* ---------- EVENT: view_cal ----------
		 * Show the stored calibration table with 710 list.
		 */
		socket.on('view_cal', function() {
			module.exports.writeToLaser(33, '710 list');
		});


		/* ---------- EVENT: fl_h ----------
		 * for homing one axis
		 */
		socket.on('fl_h', function(axis) {
			module.exports.writeToLaser(33, axis + ' fl_h');
		});

		/* ---------- EVENT: fl_vacuum_on ---------- */
		socket.on('fl_vacuum_on', function() {
			module.exports.writeToLaser(33, 'vacuum_on');
		});

		/* ---------- EVENT: fl_vacuum_off ---------- */
		socket.on('fl_vacuum_off', function() {
			module.exports.writeToLaser(33, 'vacuum_off');
		});

		/* ---------- EVENT: processes_get-suggested-stock ----------
		 * from /processes
		 * Emitted when select option box is pressed to create a new process.
		 * Will emit socket msg with a nested obj of suggested stock.
		 *
		 * @param1 dataObj =
		 * {
		 * 	stockRequest: {
			 type: requestTypeStr
			 data: {
				material_name: str
			 }
			},
			processObj: process,
			currentProcess: selectedJobProcess,
			currentProcessType: selectedJobProcessType
			}
		 * @returns suggestedStockObj
		 */
		socket.on('processes_get-suggested-stock', function(dataObj) {
			dbManager.getSuggestedStock(dataObj, function(suggestedStockObj) {
				socket.emit('processes_suggested-stock-result', suggestedStockObj);
			});
		});

		/* ---------- EVENT: processes_get-db-param-options ----------
		 * from /processes
		 * Emitted when settings button is pressed to see if USER and/or
		 * FACTORY values exist for the current job.
		 *
		 * ResultObj:
		 * {
		 * 	userInfo: {
		 * 		1: "USER",
		 * 		2: "FACTORY
		 * 	},
		 * 	optionsList: [{
		 * 		name: "FACTORY",
		 * 		exists: true,
		 * 		params: {<full params obj row from DB>}
		 * 	}, {
		 * 		...another obj...
		 * 	}]
		 * }
		 */
		socket.on('processes_get-db-param-options', function(processObj) {
			dbManager.getParamOptions(processObj, processObj.processes[0], function(err, resultObj) {
				socket.emit('processes_db-param-options-result', resultObj)
			});
		});

		/* ---------- EVENT: processes_switch-user ----------
		 * from /processes/details
		 * Emitted when "Use custom process" or "Use fablight process" are pressed.
		 * Switches to USER or FACTORY and sets is_default in the DB for both accordingly.
		 * Does NOT change the master process object.
		 *
		 * Request: {
		 * 	process: <obj>,
		 * 	switchTo: 'USER' or 'FACTORY',
		 * 	currentProcessTemporary: <bool>	True if current process is temporary.
		 * }
		 *
		 * ResultObj emitted with process_switch-user-complete:
		 * { err: null,
		 * 	process: <process obj with new params>
		 * 	}
		 */
		socket.on('processes_switch-user', function(requestObj) {
			var process = requestObj.process;
			var requestedUser = requestObj.switchTo;
			dbManager.getParamOptions(process, process.processes[0], function(err, paramOptions) {
				// Get the USER params from the paramOptions object
				var newParams = null;
				for(var i = 0; i < paramOptions.optionsList.length; i++) {
					if(paramOptions.optionsList[i].name == requestedUser) {
						newParams = paramOptions.optionsList[i].params;
					}
				}
				newParams.is_default = 1;
				// Set the USER process to be default in the DB. Un-default the previously used one.
				var oldParams = process.processes[0].params;
				dbManager.db.serialize(function () {
					dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
						newParams.laser_parameter_id, true));
					// If the current process IS temporary, meaning we are now trying to save to the
					// database, then update the master and emit the result event.
					if(requestObj.currentProcessTemporary == "true") {
						// Create the result object
						var resultProcessObj = process;
						resultProcessObj.processes[0].temporary = false;
						resultProcessObj.processes[0].params = newParams;
						socket.emit('process_switch-user-complete', {
							err: null,
							process: resultProcessObj
						});
					}
					// If the current process is NOT temporary, meaning it's in the DB, then set this
					// current process to is_default=0.
					else {
						dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
							oldParams.laser_parameter_id, false), function(err) {
							// Create the result object
							var resultProcessObj = process;
							resultProcessObj.processes[0].params = newParams;
							socket.emit('process_switch-user-complete', {
								err: null,
								process: resultProcessObj
							});
						});
					}
				});
			});
		});
		/* ---------- EVENT: processes_update-master-process-obj ----------
		 * Given a process object, saves
		 *
		 * Request: <processObj with only a single process>
		 * Response: none
		 */
		socket.on('processes_update-master-process-obj', function(processObj) {
			var currentProcess = processObj.processes[0];
			for(var i = 0; i < processTableHelper.masterProcessObj.processes.length; i++) {
				if(currentProcess.name == processTableHelper.masterProcessObj.processes[i].name) {
					processTableHelper.masterProcessObj.processes[i] = currentProcess;
				}
			}
			socket.emit('processes_update-master-process-obj-result');
		});
		/* ---------- EVENT: processes_save-process-obj ----------
		 * from /processes
		 * Emitted when "Edit process" icon button is pressed. Saves the current process obj.
		 * Needed so the cancel button returns the process obj back to it's original state.
		 *
		 * Request: <processObj>
		 * Response: none
		 */
		socket.on('processes_save-process-obj', function(processObj) {
			processTableHelper.temporaryProcessObj = processObj;
		});
		/* ---------- EVENT: processes_cancel-process-changes ----------
		 * from /processes/details
		 * Emitted when "Cancel" button is pressed. Gets the temporaryProcessObject from processTableHelper.
		 * Server restores master process object to this one.
		 *
		 * Request: <process obj>
		 * Response: <process obj>
		 */
		socket.on('processes_cancel-process-changes', function(currentProcessObj) {
			// Determine if the current process object is temporary or not.
			var currentProcessTemporary = currentProcessObj.processes[0].temporary;
			// Make sure to reset DB changes that have been made between USER and FACTORY.
			// 1. Get the original saved process (before changes in the processDetails page).
			// This process was saved from the edit process btn on /processes.
			var processName = currentProcessObj.processes[0].name;
			var originalProcess = null;
			for(var j = 0; j < processTableHelper.temporaryProcessObj.processes.length; j++){
				if(processTableHelper.temporaryProcessObj.processes[j].name == processName) {
					originalProcess = processTableHelper.temporaryProcessObj.processes[j];
				}
			}
			// 2. Get the current process
			var currentProcess = currentProcessObj.processes[0];
			// 3. Reset the DB accordingly to the original state.
			dbManager.db.serialize(function () {
				// Set the original process to be is_default=1
				dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
					originalProcess.params.laser_parameter_id, true), function(err) {
					// 4a. If the current process is temporary, then we are done.
					if(currentProcessTemporary == true) {
						// Replace the master with the temporary process object (the original one), which was saved earlier.
						processTableHelper.masterProcessObj = processTableHelper.temporaryProcessObj;
						socket.emit('processes_cancel-process-changes-result', processTableHelper.masterProcessObj);
					}
					else {
						// 4b. If the current process is NOT temporary then we need to reset the DB further.
						// Set the modified process to be is_default=0
						dbManager.db.run(SQLcommands.set_laser_parameter_row_to_default(
							currentProcess.params.laser_parameter_id, false), function(err) {
							// Replace the master with the temporary process object (the original one), which was saved earlier.
							processTableHelper.masterProcessObj = processTableHelper.temporaryProcessObj;
							socket.emit('processes_cancel-process-changes-result', processTableHelper.masterProcessObj);
						});
					}
				});
			});
		});

		/* ---------- EVENT: settings_show-port-33-data ----------
		 * from /settings
		 * Sometimes the HMI needs to display data from port 33.
		 * The page will send this event with a <bool>.
		 *
		 * Request: <bool> If TRUE, the user wants to see the port 33 data.
		 */
		socket.on('settings_show-port-33-data', function(showData) {
			status.showPort33data = showData;
		});

		/* ---------- EVENT: fileupload_folder ----------
		 * from /fileupload
		 * Emitted when a folder is selected
		 *
		 * data:
		 * {
		 * 	currentPath: <str>,
		 * 	selectedItem: {
		 * 		name: <str>
		 * 		type: <str: "file", "folder">
		 * 	  	}
		 * }
		 *
		 * Emits filesAndFolders, an object with 3 keys:
		 * {
		 * 	"path:"	/Volumes/OLSTICK/ramya",
		 * 	"files":
		 * 		[
		 * 			{"name":"tube square 1.5in 0.065thick 0.125rad"}
		 * 		],
		 * 	"folders":
		 * 		[
		 * 			{"name":"bottle opener"},
		 * 			{"name":"processtable-utility"}
		 * 		]
		 * 	}
		 */
		socket.on('fileupload_select-folder', function(data) {
			var pathToCheck = path.join(data.currentPath, data.selectedItem.name);
			usbManager.getFilesAndFolders(pathToCheck , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: fileupload_select-navigation ----------
		 * from /fileupload
		 * Emitted when the "go up" button is pressed
		 *
		 * path: {string} Current navigation path. Need to go up a level.
		 */
		socket.on('fileupload_select-navigation', function(currentPath) {
			var requestedPath = path.join(currentPath, '..');
			usbManager.getFilesAndFolders(requestedPath , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					// Added case for the UDOO. If "going up a level" does not have any files or folders,
					// this indicates the USB has been unplugged. There should always be at least one folder
					// on your "way up", because that is how you got to be one level deeper.
					if((filesAndFolders.folders.length === 0)) {
						socket.emit('fileupload_error', 'Change of USB detected. Refreshing...');
					}
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: restart_hmi ---------- */
		socket.on('restart_hmi', function () {
			require('reboot').rebootImmediately();
		});

		/* ---------- EVENT: restart_controller ---------- */
		socket.on('restart_controller', function () {
			module.exports.writeToLaser(33, 'cold', function (err) {
				if(err) {
					socket.emit('writeToLaser_err', err);
				}
			});
		});

		/* ---------- EVENT: reconnect_hmi ---------- */
		socket.on('reconnect_hmi', function () {
			status.laserSocket33.destroy();
			status.laserSocket33.unref();
			status.laserSocket23.destroy();
			status.laserSocket23.unref();
			status.initLoaded = false;
			status.isOnline = false;
			status.TCPconnected = false;
			socket.emit('machine_offline');
		});
	}

};