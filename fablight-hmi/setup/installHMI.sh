#!/usr/bin/env bash
# This script is run on a newly flashed HMI SD card.
# It updates and installs required packages.
# If an HMI zip file is provided, it will be installed.
#
# Required usage:
# sudo bash installHMI.sh <HMI zip file>.zip

echo -e "\e[42mStarting the FabLight installHMI script...VERSION 1.0\e[39m"

set -u      # Throw error if an variable is referenced before being set
set -e      # Exit as soon as any line fails
set -x      # Shows the commands that get run

# Print a statement to the terminal and color code it. Takes 2 parameters:
# $1 - level. 0=error, 1=warning, 2=info
# $2 - message
printLevel() {
    levelString=""
    startFormat=""
    endFormat=""
    if [[ $1 == 0 ]]; then
        levelString="\e[41m Error: \e[49m"
    elif [[ $1 == 1 ]]; then
        levelString="\e[43m Warning: \e[49m"
    else
        levelString="\e[42m Info: \e[49m"
    fi
    echo -e "$levelString $2"
}

usage() { printLevel 0 "Usage: sudo bash $0 <HMI zip file>.zip" 1>&2; exit 1; }
# If there isn't exactly one argument then exit.
if [[ $# != 1 ]]; then
    usage
fi

# Script variables
HMIpackage=$1

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  printLevel 2 "Connected to the network."
else
  printLevel 0 "No network connection. Internet is required to download apt-get packages."
  exit 2
fi

printLevel 2 "Updating apt-get and apt..."
sudo apt-get update
sudo apt update
npm update

printLevel 2 "Current node version is..."
node -v

printLevel 2 "Configuring fullscreen mode and disabling desktop."
printLevel 2 "Select \"Anybody\" if prompted."
sudo apt install --no-install-recommends openbox pulseaudio
sudo dpkg-reconfigure x11-common  # select Anybody
echo "Disabled desktop" | sudo tee /etc/init/lightdm.override  # disable desktop

printLevel 2 "Installing build-essential. May take a few moments."
sudo apt-get install build-essential libudev-dev

if [[ -f /etc/cron.daily/logrotate ]]; then
    printLevel 2 "Moving logrotate to run hourly..."
    mv /etc/cron.daily/logrotate /etc/cron.hourly/logrotate
else
    printLevel 2 "Logrotate already runs hourly."
fi

printLevel 2 "Install package \"usbmount\", may take a few moments."
sudo apt-get install usbmount
sudo sed -i '/\bMOUNTOPTIONS\b/s/\("\?\)$/,user,umask=000\1/' /etc/usbmount/usbmount.conf
sudo sed -i '/\bMOUNTOPTIONS\b/s/sync,\?//' /etc/usbmount/usbmount.conf

printLevel 2 "Installing nfs-common for file sharing for FabServer"
sudo apt-get install nfs-common

# Now we are installing the HMI package
HMIpackage=$1
    printLevel 2 "Beginning install of HMI application to /opt..."
    cd /opt/
    sudo unzip "$HMIpackage"


printLevel 2 "Script completed"