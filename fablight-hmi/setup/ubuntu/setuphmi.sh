#!/bin/bash
# Given a complete zip file for an HMI node.js application,
# this script sets up the HMI on a new mini PC.
#
# For consistency, place this script into /opt/ directory.
#
# Usage: sudo bash setuphmi.sh <path to zipped new HMI file>.zip

set -u      # Throw error if an variable is referenced before being set
set -e      # Exit as soon as any line fails
#set -x      # Shows the commands that get run

usage() { echo "Usage: sudo bash $0 [HMI upgrade file].zip" 1>&2; exit 1; }
# If there isn't exactly one argument then exit.
if [[ $# > 1 ]]; then
    usage
else
    if [[ $# == 0 ]]; then
        echo "\e[43m Warning: \e[49m No HMI package provided when running this script."
        echo "After this script is run, you will need to install the HMI package manually."
        echo "Do you wish to continue? [y/n]"
        read continue
        if [[ "$continue" == "y" ]]; then
            echo "Continuing script..."
        else
            echo "Exiting script."
            exit 2
        fi
    fi
fi

echo -e "\e[42mStarting the 3D Fab Light set up HMI script...VERSION 1.0\e[49m"
newHMI=$1
newHMIname="$(basename "$newHMI" ".zip")"

# Print a statement to the terminal and color code it. Takes 2 parameters:
# $1 - level. 0=error, 1=warning, 2=info
# $2 - message
printMsg() {
    levelString=""
    startFormat=""
    endFormat="\e[39m"
    if [[ $1 == 0 ]]; then
        levelString="\e[41m Error: \e[49m"
        startFormat="\e[31m"
    elif [[ $1 == 1 ]]; then
        levelString="\e[43m Warning: \e[49m"
        startFormat="\e[33m"
    else
        levelString="\e[42m Info: \e[49m"
        startFormat="\e[32m"
    fi
    echo -e "$levelString $startFormat $2 $endFormat"
}

printMsg 2 "Installing OpenSSH server"
apt-get install openssh-server

printMsg 2 "Installing accessory packages"
apt-get install nfs-common
apt-get install build-essential libudev-dev
apt-get install -y unclutter
apt-get install dconf-editor
apt-get install openbox

printMsg 2 "Installing NVM and node LTS"
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
# Reload profile to use nvm now
export NVM_DIR="$HOME/.nvm"
[[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[[ -s "$NVM_DIR/bash_completion" ]] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# install boron node.js LTS version using nvm
nvm install --lts=boron
cp -r /home/kiosk/.nvm/versions/node/v6.17.1/bin /usr/local
cp -r /home/kiosk/.nvm/versions/node/v6.17.1/lib /usr/local
cp -r /home/kiosk/.nvm/versions/node/v6.17.1/share /usr/local
chmod -R 755 /home/kiosk/.nvm/versions/node/v6.17.1/bin/*

printMsg 2 "Installing HMI"
cd /tmp
unzip ${newHMI}
mv fablight-hmi /opt

FILE_LIGHTDM_CONF_1="/etc/lightdm/lightdm.conf"
FILE_LIGHTDM_CONF_2="/etc/lightdm/lightdm.conf.d/50-myconfig.conf"
FILE_FABLIGHT_SERVICE="/etc/systemd/system/fablight-hmi.service"
FILE_KIOSK_AUTOSTART="/home/kiosk/.config/autostart/kiosk.desktop"
FILE_KIOSK_SCRIPT="/home/kiosk/kiosk.sh"
NEW_SCRIPT_LOCATION="/opt/fablight-hmi/setup/ubuntu/"

printMsg 2 "Creating new folders for service scripts"
mkdir -p /etc/lightdm/lightdm.conf.d/
mkdir -p /home/kiosk/.config/autostart/

printMsg 2 "Copying new service scripts"
# lightdm.conf script
# If the script exists in the ubuntu setup folder, then copy it to the correct location
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_1}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_1}) $(dirname ${FILE_LIGHTDM_CONF_1})
    printMsg 2 "Copied lightdm.conf"
else
    printMsg 0 "Did not copy lightdm.conf - must copy manually to /etc/lightdm/lightdm.conf"
fi
# 50-myconfig.conf script
cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_2}) $(dirname ${FILE_LIGHTDM_CONF_2})
printMsg 2 "Copied 50-myconfig.conf"

if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_FABLIGHT_SERVICE}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_FABLIGHT_SERVICE}) $(dirname ${FILE_FABLIGHT_SERVICE})
    printMsg 2 "Copied fablight-hmi.service"
else
    printMsg 0 "Did not copy fablight-hmi.service - Must copy manually to "$(dirname ${FILE_FABLIGHT_SERVICE})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_AUTOSTART}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_AUTOSTART}) $(dirname ${FILE_KIOSK_AUTOSTART})
    printMsg 2 "Copied kiosk.desktop"
else
    printMsg 0 "Did not copy kiosk.desktop - Must copy manually to "$(dirname ${FILE_KIOSK_AUTOSTART})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_SCRIPT}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_SCRIPT}) $(dirname ${FILE_KIOSK_SCRIPT})
    printMsg 2 "Copied kiosk.sh"
else
    printMsg 0 "Did not copy kiosk.sh - Must copy manually to "$(dirname ${FILE_KIOSK_SCRIPT})
fi

# Copy machine_settings.json into /opt

printMsg 2 "Enable the service"
systemctl enable fablight-hmi