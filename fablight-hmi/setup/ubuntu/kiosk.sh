#!/bin/bash

# Place in:
# /home/kiosk/kiosk.sh

export DISPLAY=:0
unclutter &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
/usr/bin/chromium-browser --disable-features=TranslateUI --kiosk --incognito --no-default-browser-check --no-first-run 'http://localhost:3000' --disable-pinch --overscroll-history-navigation=0 --use-gl=egl --enable-webgl-draft-extensions --no-sandbox --ignore-gpu-blacklist --disable-session-crashed-bubble --disable-infobars