#!/bin/bash
# This script needs to stay within this app location at:
# /opt/fablight-hmi/setup/ubuntu/start.sh

echo "fablight start.sh script starting..."
#echo "sourcing nvm"
. /home/oem/.nvm/nvm.sh
#echo "sourced nvm, now starting npm"
npm start
#google-chrome --kiosk http://localhost:3001