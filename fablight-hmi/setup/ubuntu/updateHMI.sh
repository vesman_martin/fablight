#!/bin/bash
# Given a complete zip file for an HMI node.js application,
# this script replaces the existing HMI with the provided HMI package.
#
# For consistency, place this script into /opt/ directory.
#
# This script unzips the new HMI into temporary location /tmp, copies the existing DB to /tmp as well,
# then replaces the existing
#
#
# Usage: sudo bash updateHMI.sh <path to zipped new HMI file>.zip

set -u      # Throw error if an variable is referenced before being set
set -e      # Exit as soon as any line fails
#set -x      # Shows the commands that get run

usage() { echo "Usage: sudo bash $0 [HMI upgrade file].zip" 1>&2; exit 1; }
# If there isn't exactly one argument then exit.
if [[ $# != 1 ]]; then
    usage
fi

echo -e "\e[42mStarting the FabLight updateHMI script...VERSION 2.0\e[49m"

newHMI=$1
newHMIname="$(basename "$newHMI" ".zip")"
currentHMIPath="/opt/fablight-hmi/"
DBName=""
DBPath=""

# Print a statement to the terminal and color code it. Takes 2 parameters:
# $1 - level. 0=error, 1=warning, 2=info
# $2 - message
printLevel() {
    levelString=""
    startFormat=""
    endFormat="\e[39m"
    if [[ $1 == 0 ]]; then
        levelString="\e[41m Error: \e[49m"
        startFormat="\e[31m"
    elif [[ $1 == 1 ]]; then
        levelString="\e[43m Warning: \e[49m"
        startFormat="\e[33m"
    else
        levelString="\e[42m Info: \e[49m"
        startFormat="\e[32m"
    fi
    echo -e "$levelString $startFormat $2 $endFormat"
}

# Check if there is already an older version of the HMI installed.
# If not, then it needs to be installed from a different script. Stop this script.
if [[ ! -d ${currentHMIPath} ]]; then
    printLevel 0 "There is no existing HMI application on this device. Did you mean to run the HMI install script?"
    printLevel 0 "SCRIPT EXIT CODE 2"
    exit 2
fi

printLevel 2 "Copying the existing HMI for safe keeping in /tmp/fablight-hmi_old.zip"
cd /opt
zip -r /tmp/fablight-hmi_old.zip fablight-hmi

# Unzip HMI to the /tmp folder and rename to fablight-hmi
cd /tmp/
unzip $1
rm -rf __MACOSX
# Check to see if the newly unzipped folder created a single folder called "fablight-hmi".
# If it did, then rename it to fablight-hmi_new.
# If not, then the rest of the script won't work so show an error and exit.
if [[ -d /tmp/fablight-hmi ]]; then
    mv /tmp/fablight-hmi /tmp/fablight-hmi_new/
else
    printLevel 0 "Unzipped HMI package does not contain folder \"fablight-hmi\". Double check your HMI update package."
    printLevel 0 "SCRIPT EXIT CODE 3"
    exit 3
fi

# Check if we've included the node_modules package in the newly unzipped HMI
if [[ ! -d /tmp/fablight-hmi_new/node_modules ]]; then
    printLevel 0 "Incomplete upgrade package. node_modules MISSING. Install with npm install or use a complete HMI package."
    printLevel 0 "SCRIPT EXIT CODE 4"
    rm -rf /tmp/fablight-hmi_new/
    exit 4
fi

# Delete DB that is in the newly unzipped HMI and replace with the existing one
# Then get the path of the existing, already installed DB.
rm -rf /tmp/fablight-hmi_new/db/*.db
for DBPath in "/opt/fablight-hmi/db"/*.db; do
        DBPath="$DBPath"
        DBName="$(basename "$DBPath" ".db").db"
done
printLevel 2 "Existing DB on HMI is called ${DBName}"
cp "${DBPath}" /tmp/fablight-hmi_new/db/
printLevel 2 "Copied DB for update to /tmp/fablight-hmi_new/db/${DBName}"
printLevel 2 "Copying DB for archiving to temp location /tmp. What would you like to name it?"
printLevel 2 "Suggested naming format: FLXX00_YYYY-MM-DD_<customer name>.db"
read tempDBname
cp "${DBPath}" "/tmp/$tempDBname"
printLevel 2 "You can copy this DB to the service computer using: pscp udooer@192.168.3.235:/tmp/$tempDBname ."

# Remove any existing scripts and replace with the new ones
printLevel 2 "Removing existing setup scripts..."
FILE_LIGHTDM_CONF_1="/etc/lightdm/lightdm.conf"
FILE_LIGHTDM_CONF_2="/etc/lightdm/lightdm.conf.d/50-myconfig.conf"
FILE_FABLIGHT_SERVICE="/etc/systemd/system/fablight-hmi.service"
FILE_KIOSK_AUTOSTART="/home/kiosk/.config/autostart/kiosk.desktop"
FILE_KIOSK_SCRIPT="/home/kiosk/kiosk.sh"
if [[ -f ${FILE_LIGHTDM_CONF_1} ]]; then
    rm ${FILE_LIGHTDM_CONF_1}
fi
if [[ -f ${FILE_LIGHTDM_CONF_1} ]]; then
    rm ${FILE_LIGHTDM_CONF_1}
fi
if [[ -f ${FILE_FABLIGHT_SERVICE} ]]; then
    rm ${FILE_FABLIGHT_SERVICE}
fi
if [[ -f ${FILE_KIOSK_AUTOSTART} ]]; then
    rm ${FILE_KIOSK_AUTOSTART}
fi
if [[ -f ${FILE_KIOSK_SCRIPT} ]]; then
    rm ${FILE_KIOSK_SCRIPT}
fi

NEW_SCRIPT_LOCATION="/tmp/fablight-hmi_new/setup/ubuntu/"
printLevel 2 "Copying new service scripts..."
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_1}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_1}) $(dirname ${FILE_LIGHTDM_CONF_1})
    printLevel 2 "Copied lightdm.conf"
else
    printLevel 0 "Did not copy lightdm.conf - Must copy manually to "$(dirname ${FILE_LIGHTDM_CONF_1})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_2}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_LIGHTDM_CONF_2}) $(dirname ${FILE_LIGHTDM_CONF_2})
    printLevel 2 "Copied 50-myconfig.conf"
else
    printLevel 0 "Did not copy 50-myconfig.conf - Must copy manually to "$(dirname ${FILE_LIGHTDM_CONF_2})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_FABLIGHT_SERVICE}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_FABLIGHT_SERVICE}) $(dirname ${FILE_FABLIGHT_SERVICE})
    printLevel 2 "Copied fablight-hmi.service"
else
    printLevel 0 "Did not copy fablight-hmi.service - Must copy manually to "$(dirname ${FILE_FABLIGHT_SERVICE})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_AUTOSTART}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_AUTOSTART}) $(dirname ${FILE_KIOSK_AUTOSTART})
    printLevel 2 "Copied kiosk.desktop"
else
    printLevel 0 "Did not copy kiosk.desktop - Must copy manually to "$(dirname ${FILE_KIOSK_AUTOSTART})
fi
if [[ -f ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_SCRIPT}) ]]; then
    cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_KIOSK_SCRIPT}) $(dirname ${FILE_KIOSK_SCRIPT})
    printLevel 2 "Copied kiosk.sh"
else
    printLevel 0 "Did not copy kiosk.sh - Must copy manually to "$(dirname ${FILE_KIOSK_SCRIPT})
fi

# Machine settings is a special case. The file is machine-specific and should not need to change.
# See if there are differences in the files and allow the user to choose which file to use.
if [[ -f /opt/machine_settings.json ]]; then
    printLevel 1 "Machine settings file already exists!"
    printLevel 1 "Do you want to [1] use the new machine_settings file (recommended) or [2] keep the existing file?:"
    read userChoice
    if [[ "$userChoice" == "1" ]]; then
        printLevel 1 "OVERWRITING machine settings file."
        cp /tmp/fablight-hmi_new/setup/machine_settings.json /opt/
        nano /opt/machine_settings.json
    else
        printLevel 1 "Keeping existing machine settings file."
    fi
else
    printLevel 1 "Machine settings file does not already exist. Copying new machine_settings.json file..."
    printLevel 1 "You must edit the file /opt/machine_settings.json manually"
    cp /tmp/fablight-hmi_new/setup/machine_settings.json /opt/
    nano /opt/machine_settings.json
fi

# Edit the config.js file
printLevel 1 "Editing the config.js file. Make sure the server is enabled (or not)."
nano /tmp/fablight-hmi_new/config.js

# Finally, remove the existing HMI and copy the new one there
printLevel 2 "Removing the existing HMI at /opt/fablight-hmi..."
rm -rf /opt/fablight-hmi
printLevel 2 "Moving and renaming the new HMI at /tmp/fablight-hmi_new to /opt/fablight-hmi..."
mv /tmp/fablight-hmi_new /opt/fablight-hmi

# Remove the existing TEMPORARY zip file
rm ${newHMI}

printLevel 2 "Script completed"
printLevel 1 "You must reboot the HMI"