#!/bin/bash
# place into /opt folder

xset -dpms
xset s off
openbox-session &
start-pulseaudio-x11

while true; do
   rm -rf ~/.cache/chromium
   chromium-browser --kiosk --no-first-run 'http://localhost:3000' --disable-pinch --overscroll-history-navigation=0 --use-gl=egl --enable-webgl-draft-extensions --ignore-gpu-blacklist --disable-session-crashed-bubble --disable-infobars
done
