#!/bin/bash
# Given a complete zip file for an HMI node.js application,
# this script replaces the existing HMI with the provided HMI package.
#
# For consistency, place this script into /opt/ directory.
#
# This script unzips the new HMI into temporary location /tmp, copies the existing DB to /tmp as well,
# then replaces the existing
#
#
# Usage: sudo bash updateHMI.sh <path to zipped new HMI file>.zip

set -u      # Throw error if an variable is referenced before being set
set -e      # Exit as soon as any line fails
#set -x      # Shows the commands that get run

usage() { echo "Usage: sudo bash $0 [HMI upgrade file].zip" 1>&2; exit 1; }
# If there isn't exactly one argument then exit.
if [[ $# != 1 ]]; then
    usage
fi

echo -e "\e[42mStarting the FabLight updateHMI script...VERSION 1.1\e[49m"

newHMI=$1
newHMIname="$(basename "$newHMI" ".zip")"
currentHMIPath="/opt/fablight-hmi/"
DBName=""
DBPath=""

# Print a statement to the terminal and color code it. Takes 2 parameters:
# $1 - level. 0=error, 1=warning, 2=info
# $2 - message
printLevel() {
    levelString=""
    startFormat=""
    endFormat="\e[39m"
    if [[ $1 == 0 ]]; then
        levelString="\e[41m Error: \e[49m"
        startFormat="\e[31m"
    elif [[ $1 == 1 ]]; then
        levelString="\e[43m Warning: \e[49m"
        startFormat="\e[33m"
    else
        levelString="\e[42m Info: \e[49m"
        startFormat="\e[32m"
    fi
    echo -e "$levelString $startFormat $2 $endFormat"
}

# Check if there is already an older version of the HMI installed.
# If not, then it needs to be installed from a different script. Stop this script.
if [[ ! -d ${currentHMIPath} ]]; then
    printLevel 0 "There is no existing HMI application on this device. Did you mean to run the HMI install script?"
    printLevel 0 "SCRIPT EXIT CODE 2"
    exit 2
fi

printLevel 2 "Copying the existing HMI for safe keeping in /tmp/fablight-hmi_old.zip"
cd /opt
zip -r /tmp/fablight-hmi_old.zip fablight-hmi

# Unzip HMI to the /tmp folder and rename to fablight-hmi
cd /tmp/
unzip $1
rm -rf __MACOSX
# Check to see if the newly unzipped folder created a single folder called "fablight-hmi".
# If it did, then rename it to fablight-hmi_new.
# If not, then the rest of the script won't work so show an error and exit.
if [[ -d /tmp/fablight-hmi ]]; then
    mv /tmp/fablight-hmi /tmp/fablight-hmi_new/
else
    printLevel 0 "Unzipped HMI package does not contain folder \"fablight-hmi\". Double check your HMI update package."
    printLevel 0 "SCRIPT EXIT CODE 3"
    exit 3
fi

# Check if we've included the node_modules package in the newly unzipped HMI
if [[ ! -d /tmp/fablight-hmi_new/node_modules ]]; then
    printLevel 0 "Incomplete upgrade package. node_modules MISSING. Install with npm install or use a complete HMI package."
    printLevel 0 "SCRIPT EXIT CODE 4"
    rm -rf /tmp/fablight-hmi_new/
    exit 4
fi

# Delete DB that is in the newly unzipped HMI and replace with the existing one
# Then get the path of the existing, already installed DB.
rm -rf /tmp/fablight-hmi_new/db/*.db
for DBPath in "/opt/fablight-hmi/db"/*.db; do
        DBPath="$DBPath"
        DBName="$(basename "$DBPath" ".db").db"
done
printLevel 2 "Existing DB on HMI is called ${DBName}"
cp "${DBPath}" /tmp/fablight-hmi_new/db/
printLevel 2 "Copied DB for update to /tmp/fablight-hmi_new/db/${DBName}"
printLevel 2 "Copying DB for archiving to temp location /tmp. What would you like to name it?"
printLevel 2 "Suggested naming format: FLXX00_YYYY-MM-DD_<customer name>.db"
read tempDBname
cp "${DBPath}" "/tmp/$tempDBname"
printLevel 2 "You can copy this DB to the service computer using: pscp udooer@192.168.3.235:/tmp/$tempDBname ."

# Remove any existing scripts and replace with the new ones
printLevel 2 "Removing existing setup scripts..."
if [[ -f /etc/init/kiosk.conf ]]; then
    rm /etc/init/kiosk.conf
fi
if [[ -f /etc/init/fablight-hmi.conf ]]; then
    rm /etc/init/fablight-hmi.conf
fi
if [[ -f /etc/init/start-hmi-server.conf ]]; then
    rm /etc/init/start-hmi-server.conf
fi
if [[ -f /opt/kiosk.sh ]]; then
    rm /opt/kiosk.sh
fi
if [[ -f /etc/logrotate.d/fablight-hmi ]]; then
    rm /etc/logrotate.d/fablight-hmi
fi
printLevel 2 "Copying new service scripts..."
if [[ -f /tmp/fablight-hmi_new/setup/kiosk.conf ]]; then
    printLevel 2 "Copied kiosk.conf"
    cp /tmp/fablight-hmi_new/setup/kiosk.conf /etc/init/
else
    printLevel 0 "Did not copy kiosk.conf - Must copy manually"
fi
if [[ -f /tmp/fablight-hmi_new/setup/fablight-hmi.conf ]]; then
    printLevel 2 "Copied fablight-hmi.conf"
    cp /tmp/fablight-hmi_new/setup/fablight-hmi.conf /etc/init
else
    printLevel 0 "Did not copy fablight-hmi.conf - Must copy manually"
fi

if [[ -f /tmp/fablight-hmi_new/setup/kiosk.sh ]]; then
    printLevel 2 "Copied kiosk.sh"
    cp /tmp/fablight-hmi_new/setup/kiosk.sh /opt/
else
    printLevel 0 "Did not copy kiosk.sh - Must copy manually"
fi
if [[ -f /tmp/fablight-hmi_new/setup/fablight-hmi ]]; then
    printLevel 2 "Copied fablight-hmi"
    cp /tmp/fablight-hmi_new/setup/fablight-hmi /etc/logrotate.d/
else
    printLevel 0 "Did not copy fablight-hmi - Must copy manually"
fi
if [[ -f /tmp/fablight-hmi_new/setup/installHMI.sh ]]; then
    printLevel 2 "Copied installHMI.sh"
    cp /tmp/fablight-hmi_new/setup/installHMI.sh /opt
else
    printLevel 0 "Did not copy installHMI.sh - Must copy manually"
fi

# Move logrotate so it runs hourly
if [[ -f /etc/cron.daily/logrotate ]]; then
    printLevel 2 "Moving logrotate to run hourly..."
    mv /etc/cron.daily/logrotate /etc/cron.hourly/logrotate
else
    printLevel 2 "Logrotate already runs hourly."
fi

# TO DO: Update this script, updateHMI.sh
# cp /tmp/fablight-hmi_new/setup/updateHMI.sh /opt

# Machine settings is a special case. The file is machine-specific and should not need to change.
# See if there are differences in the files and allow the user to choose which file to use.
if [[ -f /opt/machine_settings.json ]]; then
    printLevel 1 "Machine settings file already exists!"
    printLevel 1 "Do you want to [1] use the new machine_settings file (recommended) or [2] keep the existing file?:"
    read userChoice
    if [[ "$userChoice" == "1" ]]; then
        printLevel 1 "OVERWRITING machine settings file."
        cp /tmp/fablight-hmi_new/setup/machine_settings.json /opt/
        nano /opt/machine_settings.json
    else
        printLevel 1 "Keeping existing machine settings file."
    fi
else
    printLevel 1 "Machine settings file does not already exist. Copying new machine_settings.json file..."
    printLevel 1 "You must edit the file /opt/machine_settings.json manually"
    cp /tmp/fablight-hmi_new/setup/machine_settings.json /opt/
    nano /opt/machine_settings.json
fi

# Edit the config.js file
printLevel 1 "Editing the config.js file. Make sure the server is enabled (or not)."
nano /tmp/fablight-hmi_new/config.js

# Finally, remove the existing HMI and copy the new one there
printLevel 2 "Removing the existing HMI at /opt/fablight-hmi..."
rm -rf /opt/fablight-hmi
printLevel 2 "Moving and renaming the new HMI at /tmp/fablight-hmi_new to /opt/fablight-hmi..."
mv /tmp/fablight-hmi_new /opt/fablight-hmi

# Remove the existing TEMPORARY zip file
rm ${newHMI}

printLevel 2 "Script completed"
printLevel 1 "You must reboot the HMI"