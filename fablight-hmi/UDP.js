/*
 * UDP.js
 *
 * Class for UDP port creation.
 *
 */

// networking
var dgram = require('dgram'); 		// for UDP
const readline = require('readline');	// for parsing TCP info
var split = require('split');

// load modules
var fs = require('fs');
const path = require('path');

// machine status
var status = require('./machineStatus');	// for keeping track of state info
var config = require('./config');	// for keeping track of state info

// port info
const PING_MACHINE_PORT = 6201;		// HMI pings extratech at this port
const MACHINE_REPLY_PORT = 6202;	// HMI listens to the ping reply at this port
const MACHINE_REBOOT_MSG_PORT = 6203;	// HMI listens to this port for a machine reboot message
const MACHINE_IP = config.laser.ip_address;
const PING_MACHINE_MESSAGE = new Buffer('ACKR');

module.exports = {

	/** Creates a new UDP socket to send data to a destination port.
	 *
	 * @param destinationPort
	 * @param IPaddress
	 * @constructor
	 */
	SendPort: function(destinationPort, IPaddress) {
		this.destinationPort = destinationPort;
		this.IPaddress = IPaddress;
		this.socket = dgram.createSocket({
			type: 'udp4'
		});
		this.ping = function(message, callback) {
			this.socket.send(message, 0, message.length, this.destinationPort, this.IPaddress, callback);
		};
		return this.socket;
	},

	/** Creates a UDP socket on the requested port on the HMI to listen for data.
	 * Since the port is a listener from the HMI, there is only one parameter, the port number.
	 *
	 * @param port
	 * @constructor
	 */
	ReceivePort: function(port) {
		this.port = port;
		this.socket = dgram.createSocket({
			type: 'udp4',
			reuseAddr: true
		}).bind(this.port);
		return this.socket;
	},

	/* setUpUDP - binds event listeners to UDPreceive object.
	 * These should only be bound ONCE, which is why this is in a separate function.
	 */
	setUpUDP: function() {
		status.udp.socketReceive.on('listening', function() {
			res.io.emit('udp-status', 'Finding machine...');
			pingUDP(); // start pinging UDP port.
		});

		// when a message is received, open TCP socket.
		status.udp.socketReceive.on('message', function(msg, rinfo) {
			msg = msg.toString();
			// verify message contains "FabLight" or "RPC"
			if((msg.indexOf('FabLight') > -1) || (msg.indexOf('RPC') > -1)){
				status.udp.lastUDPreceived = Date.now();
			}
		});
		status.udp.socketReceive.on('error', function(err) {
			res.io.emit('udp-status', 'UDP error.');
			console.log('index.js: UDPreceive error: ', err.stack);
		});
	},

	pingUDP: function() {
	}

};

// start pinging the machine over UDP
var machinePing = new module.exports.SendPort(PING_MACHINE_PORT, MACHINE_IP);
machinePing.ping(PING_MACHINE_MESSAGE, function() {
	console.log('[UDP] pinging machine');
});

var machineReply = new module.exports.ReceivePort(MACHINE_REPLY_PORT);