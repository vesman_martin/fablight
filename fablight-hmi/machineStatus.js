/*
 * machineStatus.js
 *
 * Checking machine status
 *
 */

var status = {};
var machineStatus;

machineStatus = function() {
	status = {
		isOnline: false,			// general status of machine, on or offline
		isConnected: false,
		UDPconnected: false,
		TCPconnected: false,
		laserSocket23: null,		// laser socket object
		laserSocket29: null,		// laser socket object
		laserSocket33: null,		// laser socket object
		showPort33data: false,
		udp: {
			socketSend: null,
			socketReceive: null,
			lastUDPreceived: null,
			lastUDPsent: null,
			UDPsMissed: 0
		},
		position: {
			x: 0,
			y: 0
		},
		job : {
			startTime: null,
			endTime: null
		},
		machineHomed: false,		// is the machine homed
		machineState: 0,		// keep track of machine state for reconnection
		machinePaused: null,
		connectionState: 0x00,	// keep track of connection state using flags
		port23flag: 0x01,
		port33flag: 0x04,
		portFlagSum: 0x05,
		initLoaded: false,
		previousPage: null,	// For the selectfile and fileupload pages, keep track of which of those pages you're on
		pingInitFile: null,	// For the index page
		processes: {
			processPinEntered: false	// True if the process editing pin has been entered successfully
		}
	};
	return status;
};

module.exports = machineStatus();