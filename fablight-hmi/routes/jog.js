/*
 * jog.js
 * Routes for the /jog
 * renders the jog page.
 * */

var express = require('express');
var router = express.Router();
var appRoot = require('app-root-path');

var status = appRoot.require('machineStatus');
var processTableHelper = appRoot.require('processTableHelper');
var dbManager = appRoot.require('dbManager');
var logger = appRoot.require('logger');

router.get('/', function(req, res) {
	// Get the machine units.
	dbManager.getDisplayUnit(function (unit) {
		if(status.laserSocket33 != null) {
			status.laserSocket33.write('fl_xy\n');
		}
		res.render('jog', {
			title: 'Jog',
			machineUnit: unit
		});
	});
});

module.exports = router;