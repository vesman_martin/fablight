/*
 * routes for /preview page
 */
var express = require('express');
var router = express.Router();
var fs = require('fs');
const path = require('path');
var util = require('util');

var processTableHelper = require('../processTableHelper');
var usbManager = require('../usbManager');

/*
 * GET /preview
 *
 * request is of the form:
 * { fileName: fileNameWithExtension }
 *
 */
router.get('/', function(req, res) {
	var previewExists = false;
	var fileName = req.query.fileName;
	fileName = path.basename(fileName, '.fab');
	var fileNameNoExtension = path.basename(fileName, '.fab');	// strip off the .fab extension from the fileName
	processTableHelper.getJobBoundsFromProcessFile(fileName, function(err, bounds) {
		
		// if there's any problem getting the bounds, set them to 0.
		if(err) {
			bounds = {
				xMin: 0,
				xMax: 0,
				yMin: 0,
				yMax: 0
			}
		}

		var previewPath = null;
		// check if preview file exists
		if(usbManager.jobPath.preview != null) {
			previewExists = true;
			// Preview Path must be /uploads/jobs/hmifile.js
			previewPath = path.join('/uploads/jobs', usbManager.jobPath.preview);
		}

		res.render('preview', {
			title: 'File preview',
			jobName: fileNameNoExtension,
			previewExists: previewExists,
			previewPath: previewPath,
			previewFileName: 'hmifile.js',
			bounds: JSON.stringify(bounds),
			location: usbManager.jobPath.location
		});

	});
});

module.exports = router;