/*
 * index.js
 * Routes for the / page (first page of app)
 * Opens ports and contains logic for communication with machine.
 *
 * This route renders the home page, and pings the machine (via UDP) until it gets a response.
 * Then it attempts to open up TCP sockets for port 23 and 33.
 *
 * */

// ---------- set up ---------- //
var express = require('express');
var router = express.Router();
var net = require('net');			// for TCP socket
var dgram = require('dgram'); 		// for UDP
var readline = require('readline');	// for parsing TCP info
var split = require('split');
var path = require('path');
var appRoot = require('app-root-path');

var status = appRoot.require('machineStatus');	// for keeping track of state info
var socketData = appRoot.require('socketDataFunctions');	// for TCP onData functions
var logger = appRoot.require('logger');

// config prod vs dev for laser address and port, see config.js for info
var config = appRoot.require('config');
var laserPort = config.laser.job_port;
var laserAddress = config.laser.ip_address;
var laserPort33 = config.laser.command_port;	// port 33 for all other commands

// set up for UDP
var pingMessage = new Buffer.from('ACKR');
var HOST = laserAddress;	// laser IP address
var PING_PORT = config.laser.udp_ping_port;		// udp port to send ping to
var LISTEN_PORT = config.laser.udp_listen_port;		// udp port to listen to
var reconnectionTimeout = 1000;
var pingDelay = 5000;	// for UDP

// ACK command over TCP port 33 to verify the init file is loaded.
// Using this message, the controller will echo the response in JSON.
// 13 is the device for port 33 on extratech. Change to 11 for port 23.
var ACK_COMMAND = 'ifexists fl_check_errors fl_check_errors\r\n';

// HMI version
var packagejson = require(path.normalize(path.join('..', 'package.json')));


// ---------- ROUTES ---------- //
/* GET home page. */
router.get('/', function(req, res, next) {
	// ---------- initial TCP & UDP socket set up ---------- //
	/* As HMI first boots, create new TCP sockets and UDP dgram objects.
	 * These should only be created once over the lifetime of the app.
	 * ONLY run if we're NOT in demo mode.
	 */
	if(config.demoMode === false) {
		if (status.laserSocket23 == null) { status.laserSocket23 = new net.Socket(); }
		if (status.laserSocket33 == null) { status.laserSocket33 = new net.Socket(); }
		if (status.udp.socketReceive == null) {
			status.udp.socketReceive = dgram.createSocket({
				type: 'udp4',
				reuseAddr: true
			}).bind(LISTEN_PORT);					// UDP listen on 0.0.0.0:6202
			status.udp.socketSend = dgram.createSocket('udp4');
			status.udp.socketSend.on('error', function(err) {
				logger.log('[HMI] UDP send error ' + err);
			});
			setUpUDP();	// attach event listeners to UDP objects.
		}

		// if all TCP ports are connected
		if(status.connectionState === status.portFlagSum) {
			// socketData.writeToLaser(33, 'fl_check_errors');
		}
	}
	else {
		// If we ARE in demo mode, then pretend we're online.
		status.initLoaded = true;
		status.isOnline = true;
	}

	/* @function setUpUDP - Set up UDP port to receive messages.
	 * Binds event listeners to UDPreceive object.
	 * These should only be bound ONCE when the HMI is first booted
	 */
	function setUpUDP () {
		status.udp.socketReceive.on('listening', function() {
			res.io.emit('udp-status', 'Finding machine...');
			pingUDP();
		});
		// when a message is received, open TCP socket.
		status.udp.socketReceive.on('message', function(msg, rinfo) {
			msg = msg.toString();
			// verify message. we are looking for a message containing "FabLight"
			// Serial number might be RPC (extratech factory default)
			if((msg.indexOf('FabLight') > -1) || (msg.indexOf('RPC') > -1)){
				status.udp.lastUDPreceived = Date.now();
			}
		});
		status.udp.socketReceive.on('error', function(err) {
			logger.log('[HMI] UDPreceive error: ', err.stack);
		});
	}

	// ---------- connection utility functions ---------- //
	/* @function pingUDP: Pings laser at IPaddr with UDP port 6201.
	 * Pings every 5s using a timer interval
	 * The machine responds at UDP port 6202 with it's serial number
	 * Open a new TCP socket over port 23 to communicate to the laser
	 */
	function pingUDP() {
		pingAtInterval = setInterval(function() {
			status.udp.socketSend.send(pingMessage, 0, pingMessage.length, PING_PORT, HOST, function(err, bytes) {
				status.udp.lastUDPsent = Date.now();
				if (err) {}		// don't care about UDP send errors so don't throw any errors.
				var info = status.udp.socketSend.address();
				/*console.log('index.js: pingUDP(): from me ' + info.address + ':' + info.port + '\tsent to' + HOST +
				 ':' + PING_PORT + '<' + pingMessage + '> at' + status.udp.lastUDPsent); */
				// every time you ping, check if you received a reply before.
				// check if the reply was within 1s of the pingDelay
				if(status.udp.lastUDPreceived != null) {
					//console.log('Diff between current time and lastUDPreceived: ' + (Date.now() - status.lastUDPreceived));
					if(Date.now() - status.udp.lastUDPreceived < (pingDelay + 500)) {
						// Reset the count of missed UDP messages.
						status.udp.UDPsMissed = 0;
						// at this point the machine has been found on the network,
						// but the TCP ports may or may not have been connected. Attempt to open them if not open.
						if ((status.connectionState & status.port23flag) === 0) {
							setTimeout(openPort23, reconnectionTimeout); // NOTE port 29 is opened AFTER port 23.
						}
						if ((status.connectionState & status.port33flag) === 0) {
							setTimeout(openPort33, reconnectionTimeout);
						}
						// Every time we receive a valid UDP response, check the status of the TCP ports.
						// If the TCP ports are connected, update the status.
						if(status.connectionState === status.portFlagSum) {
							status.TCPconnected = true;
						}
					}
					// If we are missing UDP messages (more than 1), then the machine is offline and the TCP sockets
					// should be closed. There is a special case when status.lastUDPreceived === 0, when the app first
					// boots up. The difference between the lastUDPreceived will be greater than the allowed time, but
					// this is because the very first lastUDPreceived is 0.
					else if(status.udp.lastUDPreceived !== 0) {
						// logger.log('[HMI] udp.UDPsMissed: ' + status.UDPsMissed);
						if(status.udp.UDPsMissed >= 1) {
							res.io.emit('udp-status-offline');
							// If we previously had the TCP sockets connected, we have transitioned from ONLINE --> OFFLINE.
							if(status.TCPconnected === true) {
								// logger.log('[HMI] Machine offline.');
								res.io.emit('machine_offline');
								status.isOnline = false;
								status.TCPconnected = false;
								status.initLoaded = false;
								status.laserSocket23.destroy();
								status.laserSocket23.unref();
								status.laserSocket33.destroy();
								status.laserSocket33.unref();
							}
							removeConnectionStatus(status.port23flag);
							removeConnectionStatus(status.port33flag);
						}
						status.udp.UDPsMissed += 1;
					}
				}
			});
		}, pingDelay);
	}

	// ---------- TCP net socket functions ---------- //
	// ---------- port 23 functions ---------- //
	function onConnectPort23(socket) {
		// emitted when TCP port 23 is connected
		if(status.laserSocket23 != null) {
			status.laserSocket23.write('task_port23 ptbatch\n');
		}
		// logger.log('[HMI] PORT 23 OPEN');
		addConnectionStatus(status.port23flag);
	}
	function onEndPort23() {
		removeConnectionStatus(status.port23flag);
		status.laserSocket23 = null;
		// logger.log('[HMI] socket23 end event');
	}
	function onClosePort23() {
		removeConnectionStatus(status.port23flag);
		status.laserSocket23 = null;
		// logger.log('[HMI] socket23 close event');
	}
	function onDrainPort23() {
	}

	/* @function openPort23 - sets event listeners.
	 * main entry point for port 23 functions.
	 */
	function openPort23() {
		status.laserSocket23 = net.connect(laserPort, laserAddress);
		status.laserSocket23.setEncoding('utf8');
		// setKeepAlive required for connections that are open a long time, like in this app.
		// every 1min (60,000ms) an empty TCP packet is sent/received. this happens on the TCP layer
		// status.laserSocket23.setKeepAlive(true, 1000 * 60);
		// bind socket events:
		status.laserSocket23.on('connect', onConnectPort23.bind({}, status.laserSocket23));
		status.laserSocket23.on('error', function(err) {
			if(err.code === "ECONNRESET") {
				status.laserSocket23.destroy();
				status.laserSocket23.unref();
			}
			else if(err.code === "ECONNREFUSED") {
				status.laserSocket23.destroy();
				status.laserSocket23.unref();
			}
			// logger.log('[HMI] port 23: ' + err);
		});
		status.laserSocket23.on('end', onEndPort23.bind({}, status.laserSocket23));
		status.laserSocket23.on('close', onClosePort23.bind({}, status.laserSocket23));
		status.laserSocket23.on('drain', onDrainPort23.bind({}, status.laserSocket23));
		// make a new stream that uses split to split the data line by line.
		status.laserSocket23.pipe(split())
			.on('data',function(chunk){
				socketData.handleStream23(chunk, res);
			});
	}
	// ---------- port 33 ACK for init file ---------- //
	// Ping the init file if the TCP sockets are connected
	function startPingingInitFile () {
		if(status.TCPconnected) {
			status.laserSocket33.write(ACK_COMMAND);
		}
	}
	function stopPingingInitFile () {
		clearInterval(status.pingInitFile);
	}

	// ---------- port 33 functions ---------- //
	// entry point for TCP socket on port 33
	function onConnectPort33(socket) {
		// logger.log('[HMI] PORT 33 OPEN');
		status.laserSocket33.write('task_port33 ptbatch\r\n');
		addConnectionStatus(status.port33flag);
		status.pingInitFile = setInterval(startPingingInitFile, 500);
	}
	function onClosePort33(socket) {
		removeConnectionStatus(status.port33flag);
		status.laserSocket33 = null;
		// logger.log('[HMI] Close Port 33');
	}
	function onEndPort33() {
		removeConnectionStatus(status.port33flag);
		status.laserSocket33 = null;
		// logger.log('[HMI] End Port 33');
	}
	function openPort33() {
		//laserSocket33.setEncoding('ascii');
		status.laserSocket33 = net.connect(laserPort33, laserAddress);
		// bind socket events:
		status.laserSocket33.on('connect', onConnectPort33.bind({}, status.laserSocket33));
		status.laserSocket33.on('error', function(err) {
			// logger.log('[HMI] Port 33 error: ' + err);
			// Errors either ECONNRESET or ECONNREFUSED (if something else is also connected)
			if(status.laserSocket33 !== null) {
				status.laserSocket33.destroy();
				status.laserSocket33.unref();
				//setTimeout(openPort33, reconnectionTimeout);
			}
		});
		status.laserSocket33.on('end', onEndPort33.bind({}, status.laserSocket33));
		status.laserSocket33.on('close', onClosePort33.bind({}, status.laserSocket33));
		// make a new stream that uses split to split the data line by line.
		status.laserSocket33.pipe(split())
			.on('data',function(rawChunk) {
				if(status.showPort33data === true) {
					res.io.emit('settings_port-33-data', rawChunk);
				}
				else {
					// console.log('33> ' + rawChunk);
				}

				// catch token errors and undefined errors. If no errors, parse the chunk to get the JS object.
				if(rawChunk.indexOf('token') > -1) {
					logger.log('[HMI] Token error: ' + rawChunk);
				} else if(rawChunk.indexOf('Word not defined') > -1) {
					logger.log('[HMI] Word not defined error: ' + rawChunk);
				} else {
					// Get only the relevant JSON from the init file.
					var chunk = rawChunk.substr(rawChunk.indexOf('{\"'), rawChunk.lastIndexOf('}') + 1);
					// If the chunk has a command in it
					if(chunk.length > 0) {
						if (chunk.includes('\"cmd\":\"ack\"')) {
							// We have an ACK response. Need to strip away the 0x06 characters for the JSON parser
							chunk = chunk.replace(new RegExp(String.fromCharCode(6), 'g'), '');
						}
						// Malformed JSON handling
						var printError = function (error, explicit) {
							var expl = explicit ? 'explicit' : 'inexplicit';
							logger.log('[HMI] ' + expl + ' JSON parsing error: ' + error.name + ': ' + error.message);
						};
						// Log any token errors
						if (chunk.includes('token')) {
							logger.log('[HMI] Uncaught token error: ' + chunk);
						}
						else {
							// Try to parse the JSON and log an error if there is one.
							var JSONcorrect = true;
							try {
								var command = JSON.parse(chunk);
							}
							catch (e) {
								logger.log('bad: ' + chunk)
								if (e instanceof SyntaxError) {
									printError(e, true);
									JSONcorrect = false;
								} else {
									printError(e, false);
									JSONcorrect = false;
								}
							} finally {
								if (JSONcorrect === true) {
									// We get an ACK response. A1 means the init file is loaded. A0 if not loaded.
									if(command.cmd === 'fl_check_errors') {
										if(command.e === "false") {
											if(status.initLoaded === false) {
												// The machine is now booted completely because the init file is loaded.
												stopPingingInitFile();
												status.initLoaded = true;
												status.isOnline = true;
												status.laserSocket33.write('fl_time\r\n');
												status.laserSocket33.write('fl_status\r\n');
												res.io.emit('machine_booted');
											}
										}
										else {
											status.initLoaded = false;
											status.isOnline = false;
										}
									}
									if(status.initLoaded === true) {
										socketData.handleStream33(chunk, command, res);
									}
								}
							}
						}
					}
				}
			});
	}

	// ---------- for connection status ---------- //
	// Keep track of variables used to close the port.
	function addConnectionStatus(s) {
		status.connectionState = status.connectionState | s;
		if(status.connectionState === status.portFlagSum) {
			status.TCPconnected = true;
		}
		else {
			status.TCPconnected = false;
		}
	}
	function removeConnectionStatus(s) {
		status.connectionState = status.connectionState & ~s;
	}

	// ---------- RENDER INDEX PAGE ---------- //
	res.render('index', {
		title: 'HMI',
		machineOnline: status.isOnline,
		hmiVersion: packagejson.version,
		config: JSON.stringify(config)
	});

});

module.exports = router;