var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var appRoot = require('app-root-path');

var config = appRoot.require('config');
var usbManager = appRoot.require('usbManager');
var logger = appRoot.require('logger');
var status = appRoot.require('machineStatus');

/*
 * GET /selectfile
 *
 * This route is for selecting files on the server.
 * It is only exposed to the user if config.server.enabled===true from the /newjobfrom page
 *
 * Request: none
 * Response:
 * 	filesList <array of all folder names in /public/uploads/jobs>
 */
router.get('/', function(req, res) {
	status.previousPage = 'selectfile';
	var serverPath = path.join(__dirname, ".." , "public/uploads");
	if(config.production === true) {
		serverPath = config.server.path;
	}
	// TODO check if the server is connected or not so we can show a message to the user


	// Get the list of all files and folders at the server path as an array of objects.
	usbManager.getFilesAndFolders(serverPath, function(err, filesAndFolders) {
		if(err) {
			logger.log('[HMI] Server read error ' + err);
			res.render('selectfile', {
				title: 'Select File',
				itemsFound: false
			});
		}
		else {
			// Check if we're at the server root path. If not, display a "go up a level" button.
			var pathIsRoot = true;
			if(filesAndFolders.path !== serverPath) {
				pathIsRoot = false;
			}
			var itemsFound = true;
			if((filesAndFolders.files.length < 1) && (filesAndFolders.folders.length < 1)) {
				itemsFound = false;
			}
			res.render('selectfile', {
				title: 'Select File',
				itemsFound: itemsFound,
				pathIsRoot: pathIsRoot,
				path: filesAndFolders.path,
				files: JSON.stringify(filesAndFolders.files),
				folders: JSON.stringify(filesAndFolders.folders)
			})
		}
	});
});

/** Check if the FabServer is connected or not
 *
 * @param folderPath
 * @param callback
 */
function checkIfServerIsConnected(folderPath, callback) {
	fs.readdir(folderPath, function (err, itemArray) {
		if(err) {

		}
		else if(itemArray.length === 0) {
			// No items are in the array
		}
		else {

		}
	})
}

module.exports = router;