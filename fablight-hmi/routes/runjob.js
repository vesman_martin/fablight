/*
 * runjob.js
 *
 * Routes for the /runjob page
 * Actually sends the job to the laser.
 *
 * If a fileName is provided:
 * 	Create the UCJ file
 * 	Get the job bounds
 * 	Render the jog page (final page in file workflow)
 * Otherwise load the regular jog page.
 *
 * Request:
 * 	fileName*			file name of jobfile w/o extension
 *
 * Response:
 * 	filePath			file path of jobfile
 * 	fileName			file name of jobfile w/ extension
 *  fileExtension		just the file extension
 * 	fileType			'2d' or '3d'
 * 	is3dFile			boolean true or false
 * 	title				title of page

 */



var express = require('express');
var router = express.Router();
var processTableHelper = require('../processTableHelper');
var dbManager = require('../dbManager');

router.get('/', function(req, res) {
	var fileName = req.query.fileName;
	// First get the machine units.
	dbManager.getDisplayUnit(function (unit) {
		processTableHelper.getJobBoundsFromProcessFile(fileName, function (err, bounds) {
			// if there's any problem getting the bounds, set them to 0.
			if (err) {
				bounds = {
					xMin: 0,
					xMax: 0,
					yMin: 0,
					yMax: 0
				}
			}
			// create the UCJ
			processTableHelper.addFullProcessTableToUCJ(fileName, function (err) {
				if (err) throw err;
				// figure out if the file is 2D or 3D
				if (processTableHelper.masterProcessObj.properties.stock_type === "SHEET") {
					res.render('runjob2D', {
						fileName: req.query.fileName,
						is3Dfile: false,
						title: 'Run 2D job',
						bounds: JSON.stringify(bounds),
						machineUnit: unit,
						machineSpecs: JSON.stringify(dbManager.machineSettingsObj.specs)
					});
				}
				else {
					res.render('runjob3D', {
						fileName: req.query.fileName,
						is3Dfile: true,
						title: 'Run 3D job',
						bounds: JSON.stringify(bounds),
						machineUnit: unit,
						machineSpecs: JSON.stringify(dbManager.machineSettingsObj.specs)
					});
				}
			});
		});
	});
});

module.exports = router;