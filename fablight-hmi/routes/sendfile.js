/*
 * sendfile.js
 * Routes for the /sendfile page
 * 
 * Sends the job to the laser over its TCP ports.
 * 
 * */

var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var appRoot = require('app-root-path');

// require socket from machineStatus.js
var status = appRoot.require('machineStatus');
var usbManager = appRoot.require('usbManager');
var socketData = appRoot.require('socketDataFunctions');
var logger = appRoot.require('logger');
var processTableHelper = appRoot.require('processTableHelper');

/** POST /sendfile
 *
 Request:
	 fileName: <string>,
	 is3Dfile: <bool>,
	 homePosition: <obj>,
	 isDryRun: <bool>
 *
 */
router.post('/', function(req, res, next) {
	// send response immediately to prevent POSTing multiple times
	res.send('[sendfile.js] posted to send-file');
	res.end();

	// send the dry run command. either on or off.
	var isDryRun = req.body.isDryRun;
	sendMachineDryRunMode(isDryRun);

	// Get the current gas name to print in the log file.
	processTableHelper.getCurrentGas(function (err, currentGas) {
		// first send process ucj path, then send the actual job geometry as a binary file.
		var processUCJpath = path.join(usbManager.jobPath.folder, 'processTMP.ucj');
		logger.log('\n\n', false);
		logger.log('====================================================');
		logger.log('\t\t\t\t\t |----- Fab Job name:\t' +  usbManager.jobPath.fabFileName, false);
		logger.log('\t\t\t\t\t |----- UCJ file name:\t' +  usbManager.jobPath.file, false);
		logger.log('\t\t\t\t\t |----- Dry run:\t\t' +  isDryRun, false);
		logger.log('\t\t\t\t\t |----- Gas type:\t\t' +  currentGas, false);
		logger.log('\t\t\t\t\t |----- Process table: -------------------------------', false);
		sendBinaryFile(processUCJpath, true, function() {
			var UCJfilePath = path.join(usbManager.jobPath.folder, usbManager.jobPath.file);
			sendBinaryFile(UCJfilePath, false, function() {
				socketData.writeToLaser(23, 'host_terminate');
			});
		});
	});
});

/** Stream binary file to the laser TCP port 23. Used to send jobs to the machine.
 *
 * @param filePath {string}
 * @param log {bool} - If true, write the file data to console.log
 * @param callback - Called when reached end of file
 */
function sendBinaryFile(filePath, log, callback) {
	// create ReadStream to read the UCJX file --> return a new ReadStream object
	var fileStream = fs.createReadStream(filePath, {
		encoding: 'binary'
	});
	fileStream.on('error', function(err){
		logger.log('[HMI][sendBinaryFile] Error: ' + err);
	});
	fileStream.on('open',function() {
	});
	fileStream.on('close', function(){
	});
	fileStream.on('end', function(chunk) {
		callback();
	});
	// actually write the file to the machine
	fileStream.on('data', function(chunk) {
		if(status.laserSocket23 != null) {
			status.laserSocket23.write(chunk);
			if(log) {
				logger.log(chunk, false);
			}
		}
	});
}

function sendMachineDryRunMode(isDryRunVar) {
	if(isDryRunVar === 'true') {
		socketData.writeToLaser(33, 'fl_dry_run_on');
	}
	else {
		socketData.writeToLaser(33, 'fl_dry_run_off');
	}
}


module.exports = router;