/*
 * Routes for the /files pages
 *
 * Contains logic for uploading files to the HMI, typically over USB, using multer.
 * Extracts files and places in public/uploads/tmp. Deletes this directory each time.
 * */

var express = require('express');
var router = express.Router();
var fs = require("fs");
var path = require("path");
var usbManager = require('../usbManager');
var processTableHelper = require('../processTableHelper');
var dbManager = require('../dbManager');
var extractZip = require('extract-zip');
var rimraf = require('rimraf');
var unzipper = require('unzipper');
var stream = require('stream');
var appRoot = require('app-root-path');
var status = appRoot.require('machineStatus');

/* POST /files
 *
 * Given a .fab file name and path, parse the fab file. Copy the relevant files
 * to /public/uploads/jobs to work with locally.
 *
 * Request:
	{
		fileName: fileName<string>,
		path: path<string>,
		fileLocation: 'usb' or 'server
	}
 *
 */
router.post('/', function(req, res) {
	var filePath = req.body.path;
	var fileName = req.body.fileName;
	var fileLocation = req.body.fileLocation;
	// If for some reason the file does not have a .fab extension, then add it.
	if(path.basename(fileName) !== '.fab') {
		fileName += '.fab';
	}
	usbManager.jobPath.fabFileName = fileName;
	if(fileLocation === 'server') {
		usbManager.jobPath.location = 'server';
	}
	else {
		usbManager.jobPath.location = 'usb';
	}
	var fullFilePath = path.join(filePath, fileName);
	var targetFolder = path.normalize(__dirname + '/../public/uploads/jobs/');
	// delete the existing job/ directory and all its contents.
	rimraf(targetFolder, function(err) {
		if(err) throw err;
		// remake the job/ folder.
		fs.mkdir(targetFolder, function() {
			// parse the .fab (archive, like .zip) and copy the relevant files to public/uploads/job/
			fs.createReadStream(fullFilePath)
				.on('error', function () {
					res.send({
						err: 'Can\'t read file from usb. Is it plugged in?'
					});
				})
				.pipe(unzipper.Parse())
				.pipe(stream.Transform({
					objectMode: true,
					transform: function(entry,e,cb) {
						var fileName = entry.path.trim();
						var fileExtension = path.extname(fileName).toLowerCase();
						// We assume there are only 3 relevant files we need:
						// 1. process.ucj
						// 2. hmifile.js OR hmifile
						// 3. <anything>.ucj
						if (((fileExtension == ".ucj") || fileExtension == ".js") || fileName == "hmifile") {
							var targetFilePath = path.join(targetFolder, fileName);
							saveRelevantPaths(fileName);
							entry.pipe(fs.createWriteStream(targetFilePath))
								.on('finish', cb);
						} else {
							entry.autodrain();
							cb();
						}
					}
				}))
				.on('finish', function() {
					// If the file does not have a process.ucj, it's the wrong version.
					if(usbManager.jobPath.process == null) {
						res.send({
							err: 'Wrong fab file version. Please reprocess in fabcreator.',
							errCode: 1
						});
					}
					else {
						// Parse the process UCJ file and create the masterProcessObj.
						processTableHelper.getProcessIdentifiersFromUCJ(
							path.join(usbManager.jobPath.folder, usbManager.jobPath.process),
							function(err, processObj) {
							if(err) {
								res.send({
									err: 'Error parsing fab file. Please remake file.',
									errCode: 2
								});
							}
							else {
								var rotaryExists = dbManager.machineSettingsObj.specs.rotary.rotary_exists;
								if(!(rotaryExists) && (processObj.properties.stock_type === 'TUBE')) {
									res.send({
										err: 'Can\'t run tube file.',
										errCode: 0
									});
								}
								else {
									processTableHelper.getCurrentGas(function(err, currentGas) {
										processObj.gas = currentGas;
										// Iterate through each process. Reset "temporary" to false
										//
										for(var i = 0; i < processObj.processes.length; i++) {
											processObj.processes[i].temporary = false;
										}
										processTableHelper.masterProcessObj = processObj;
										// reset the temporary process object and PIN code
										status.processes.processPinEntered = false;
										res.send({
											err: null,
											status: 'Copying files to HMI...Do not unplug USB.'
										});
									});
								}
							}
						});
					}
				});
		});
	});  
});



// Depending on the file name, save the paths to an object in usbManager.
function saveRelevantPaths(fileName) {
	if(fileName === "process.ucj") {
		usbManager.jobPath.process = fileName;
	} else if(fileName.indexOf('hmifile') > -1) {
		usbManager.jobPath.preview = fileName;
	} else {
		usbManager.jobPath.file = fileName;
	}
}

module.exports = router;