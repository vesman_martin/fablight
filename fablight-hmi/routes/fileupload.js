/*
 * fileupload.js
 *
 * Contains logic for uploading files from USB using the npm package 'multer'.
 * Also contains logic for detecting USB mounting/unmounting
 */

var express = require('express');
var router = express.Router();
var appRoot = require('app-root-path');
var config = appRoot.require('config');
var usbManager = appRoot.require('usbManager');
var logger = appRoot.require('logger');
var status = appRoot.require('machineStatus');

/* GET home page. */
router.get('/', function(req, res) {
	status.previousPage = 'fileupload';

	// Get the list of all connected USB devices.
	// If there IS a USB connected, info is in the usb object. Its path is mountpoint.
	usbManager.getUSBList( function(err, usb, mountpoint) {
		// if there is NO removable and mounted USB connected
		if(err) {
			logger.log('[HMI] ' + err);
			res.render('fileupload', {
				title: 'Select File',
				usbFound: false
			});
		}
		else {
			// Get the list of all files and folders at this path as an array of objects.
			usbManager.getFilesAndFolders(mountpoint, function(err, filesAndFolders) {
				if(err) {
					logger.log('[HMI] ' + err);
					res.render('fileupload', {
						title: 'Select File',
						usbFound: false
					});
				}
				else {
					// If we are not at the root of the USB drive we are in a folder.
					// We need to display a "go up a level" button.
					var pathIsRoot = true;
					if(filesAndFolders.path !== mountpoint) {
						pathIsRoot = false;
					}
					res.render('fileupload', {
						title: 'Select File',
						usbFound: true,
						path: filesAndFolders.path,
						pathIsRoot: pathIsRoot,
						files: JSON.stringify(filesAndFolders.files),
						folders: JSON.stringify(filesAndFolders.folders),
						serverEnabled: config.server.enabled
					});
				}
			});
		}
	});
});

module.exports = router;