/*
 * processes.js
 *
 * Routes for the /processes page
 * Connects to the DB and generates UCJ files
 * Inserts UCJ files into .ucj files from the .fab file
 *
 */

var express = require('express');
var router = express.Router();
var appRoot = require('app-root-path');
var path = require('path');
var toRegexRange = require('to-regex-range');

// store db in dbManager file so it can be used elsewhere
var dbManager = appRoot.require('dbManager');

// helper functions
var processTableHelper = appRoot.require('processTableHelper');
var SQLcommands = appRoot.require(path.join('db', 'SQLiteCommands', 'SQLiteCommands.js'));
var logger = appRoot.require('logger');
var status = appRoot.require('machineStatus');


/* ========== ROUTES ========== */

/*
 * GET /processes
 *
 * This route reads the .ucj file and parses the process strings for job material, thickness, and process(es).
 * It looks for these exact combos in the DB.
 * If the process does not exist, then it also renders the processes page but the processesExist parameter
 * 	that it passes to the page = false. The page handles the correct rendering.
 *
 * Request:
 * { fileName: fileName <string> }
 * where fileName is the folder name in /public/uploads/jobs (i.e. no .fab extension)
 *
 * Response (if process exists):
 {
 title: 'Process table',
 fileName: fileName,
 processParams: JSON.stringify(processArray),
 processesExist: true,
 db: null,
 currentGas: null,
 fileProcessArray: null
 }

 where processArray is an array of objects each of which has the following keys:
 gasType <str>
 materialName <str>
 processExists <bool> <-- this will be set to true or false per process
 processName <str>
 processNumber <int>
 processParams <obj of all the laserAttribute values etc>
 thicknessUnit <str: inches || gauge>
 thicknessValue <float>

 where processParams is an object with the following keys:
 BRIGHTNESS = null
 CGAS_DELAY = null
 CONTRAST = null
 ...more...
 VMIN_POWER = null
 VPOWER = null
 VPPI = null
 XYDN_FEED = null
 *
 * Response (if process does NOT exist):
 {
	 title: 'Process table',
	 fileName: fileName,
	 processParams: JSON.stringify(processArray),
	 processesExist: false,
	 db: JSONdb,
	 currentGas: currentGasFromDB
 }
 *
 */
router.get('/', function(req, res) {
	var fileName = req.query.fileName;
	var allParamsExist = true;
	var previousPage = status.previousPage;

	processTableHelper.getCurrentGas(function(err, currentGas) {
		// check if current gas is the same as the one in the master process object
		if(processTableHelper.masterProcessObj.gas == currentGas) {
			// gas is the same as the saved process object, so check if the parameters exist in the master
			for(var i = 0; i < processTableHelper.masterProcessObj.processes.length; i++) {
				var currentProcess = processTableHelper.masterProcessObj.processes[i];
				if("params" in currentProcess === false) {
					allParamsExist = false;
				}
			}
			if(allParamsExist === true) {
				// All parameters exist in the master object already. Could be temporary, could be from DB.
				processTableHelper.generateDisplayProcessObject(processTableHelper.masterProcessObj, function(displayObj) {
					// Get the display unit and then render the page
					dbManager.getProcessPinSettings(function (err, pinSettings) {
						dbManager.getDisplayUnit(function(unit) {
							res.render('processes', {	// render page w/saved master object
								title: 'Process table',
								err: null,
								fileName: fileName,
								process: JSON.stringify(processTableHelper.masterProcessObj),
								displayProcess: JSON.stringify(displayObj),
								processesExist: true,
								db: null,
								currentGas: currentGas,
								currentUnit: unit,
								previousPage: previousPage,
								pinSettings: JSON.stringify(pinSettings),
								pinEntered: status.processes.processPinEntered
							});
						});
					})
				});
			}
			else {	// all the parameters DON'T exist in the master process object w/this gas.
				// try getting the missing ones from DB.
				processTableHelper.getParamsForProcesses(processTableHelper.masterProcessObj, function(processObjFromDB, allProcessExists) {
					// see if processes that were missing the params exist in the db:

					// 1. Iterate through each process in the master obj to add the params from the db (if applicable).
					for(var k = 0; k < processTableHelper.masterProcessObj.processes.length; k++) {
						var currentProcess = processTableHelper.masterProcessObj.processes[k];
						if(("params" in currentProcess) == false) {
							// this process does not have parameters. it needs some!
							// 2. scan the object that came from the DB lookup.
							// see if it has the parameters for this specific processes.
							for(var j = 0; j < processObjFromDB.processes.length; j++) {
								if ((processObjFromDB.processes[j].name == currentProcess.name) &&
									("params" in processObjFromDB.processes[j])) {
									// 4. use these db parameters in the master.
									processTableHelper.masterProcessObj.processes[k] = processObjFromDB.processes[j];
								}
							}
						}
					}
					// 5. Now the master process has the parameters from the DB if they exist.
					processTableHelper.generateDisplayProcessObject(processTableHelper.masterProcessObj, function(displayObj) {
						dbManager.getProcessPinSettings(function (err, pinSettings) {
							dbManager.getDisplayUnit(function(unit) {
								res.render('processes', {
									title: 'Process table',
									err: null,
									fileName: fileName,
									process: JSON.stringify(processTableHelper.masterProcessObj),
									displayProcess: JSON.stringify(displayObj),
									processesExist: allProcessExists,
									db: null,
									currentGas: currentGas,
									currentUnit: unit,
									previousPage: previousPage,
									pinSettings: JSON.stringify(pinSettings),
									pinEntered: status.processes.processPinEntered
								});
							});
						})
					});
				});
			}
		}
		else {	// Using new gas, different from the master.
			// Need to re-check the DB for parameters, because we are CLEARING the temporary processes out.
			processTableHelper.masterProcessObj.gas = currentGas;

			// 1. Clear all of the processes in the master.
			for(var m = 0; m < processTableHelper.masterProcessObj.processes.length; m++ ){
				delete processTableHelper.masterProcessObj.processes[m].temporary;
				delete processTableHelper.masterProcessObj.processes[m].params;
			}

			// 2. Try getting the parameters from the DB
			processTableHelper.getParamsForProcesses(processTableHelper.masterProcessObj, function(processObjFromDB, allProcessExists) {
				// 3. Set the master process to the db obj.
				processTableHelper.masterProcessObj = processObjFromDB;
				// 4. Get the display unit
				processTableHelper.generateDisplayProcessObject(processTableHelper.masterProcessObj, function (displayObj) {
					// 5. Render page with the master or the master converted to metric.
					dbManager.getProcessPinSettings(function (err, pinSettings) {
						dbManager.getDisplayUnit(function(unit) {
							res.render('processes', {
								title: 'Process table',
								err: null,
								fileName: fileName,
								process: JSON.stringify(processTableHelper.masterProcessObj),
								displayProcess: JSON.stringify(displayObj),
								processesExist: allProcessExists,
								db: null,
								currentGas: currentGas,
								currentUnit: unit,
								previousPage: previousPage,
								pinSettings: JSON.stringify(pinSettings),
								pinEntered: status.processes.processPinEntered
							});
						});
					})
				});
			});
		}
	});
});

/*
 * GET /processes/details
 *
 * Shows the parameters for the given process and provides an interface for the user to edit and save (update)
 * the process in the DB. Renders the processDetails page.
 *
 * Request
 * {
 fileName: fileName,
 process: processObj (includes parameters),
 selectedProcessName:
 }
 */
router.get('/details', function(req, res) {
	var fileName = req.query.fileName;
	var processObj = req.query.process;
	var selectedProcessName = req.query.selectedProcessName;
	if(selectedProcessName.indexOf('*') > -1) {
		selectedProcessName = selectedProcessName.slice(0, selectedProcessName.length - 1);
	}

	// projectsObj may have multiple processes. We only want the process that we want to see the details of.
	var processDetailsObj = {
		properties: processObj.properties,
		processes: [],	// Remove all processes.
		gas: processObj.gas
	};
	for(var i = 0; i < processObj.processes.length; i ++) {
		var currentProcessName = processObj.processes[i].name;
		if(currentProcessName == selectedProcessName) {
			// Add the process (including params) from the processObj.
			// This process was passed as a request to this page.
			processDetailsObj.processes.push(processObj.processes[i]);
		}
	}
	// Check if the kerf is in the process. If it ISN'T, then add it from the machine settings.
	if((typeof processDetailsObj.processes[0].params.kerf == 'undefined') ||
		(processDetailsObj.processes[0].params.kerf == null) ||
		(processDetailsObj.processes[0].params.kerf == "")) {
		processDetailsObj.processes[0].params.kerf = dbManager.getMachineSettingsObj().kerf;
	}

	//var displayProcess = processDetailsObj;
	// Get the other user vs factory options and render the page.
	dbManager.getParamOptions(processDetailsObj, processDetailsObj.processes[0], function(err, paramOptions) {
		processTableHelper.generateDisplayProcessObject(processDetailsObj, function (displayObj) {
			dbManager.getProcessPinSettings(function (err, pinSettings) {
				dbManager.getDisplayUnit(function (unit) {
					res.render('processDetails', {
						title: 'Process details',
						fileName: fileName,
						process: JSON.stringify(processDetailsObj),
						displayProcess: JSON.stringify(displayObj),
						paramOptions: JSON.stringify(paramOptions),
						currentUnit: unit,
						gasSettings: gasPressureForDisplay(unit),
						pinSettings: JSON.stringify(pinSettings),
						pinEntered: status.processes.processPinEntered
					});
				});
			});
		});
	});
});

/*
 * GET /processes/creator
 * This route renders the processCreator page and allows the user to create a new process in the DB
 * from an existing material and thickness combo.
 *
 * The process_to_copy object is either filled out or NULL. IF NULL, then the page will be
 * rendered with a default process and the user can create a material from scratch.
 * REQUEST:
 {
	 process_to_copy: {
		 property_id: currentPropertyID <int>
	 },
	 job_process_info: {
		 file_name: fileName, <str: folder name in /public/uploads/jobs>
		 selected_process_name: selectedJobProcess,
		 processes: <array of objs>,
		 properties: <obj>,
		 gas: <str>
	 }
 }
 * RESPONSE (if a complete request was provided):
 {
	 title: 'Process table',
	 fileName: fileName,
	 processParams: JSON.stringify(processArray),
	 processesExist: true,
	 db: null,
	 currentGas: null,
	 fileProcessArray: null,
	 defaultKerf: <float>
 }
 */
router.get('/creator', function (req, res) {
	var file_name = req.query.job_process_info.file_name;
	var property_id = req.query.process_to_copy.property_id;
	if(property_id == "") {
		property_id = null;
	}

	// the job may have more than one process, so get just the one that we
	// want to work with, using the provided selected_process_name key.
	// only this process will be passed to the page
	var job_process = [];
	var selected_process_name = req.query.job_process_info.selected_process_name;
	for(var i = 0; i < req.query.job_process_info.processes.length; i ++) {
		var currentProcess = req.query.job_process_info.processes[i];
		if(currentProcess.name == selected_process_name) {
			job_process.push(currentProcess);
		}
	}
	// job_process is now an array with a SINGLE <OBJ> with keys: number, name, type.
	var kerf = dbManager.getMachineSettingsObj().kerf;
	var gas_name = req.query.job_process_info.gas;
	// if the params ARE provided in the request
	if(property_id != null) {
		processTableHelper.getLaserParamsForPropertyID(property_id, gas_name, function(err, paramsObj) {
			// create new object to pass to the page. This follows the default process object format.
			// will be used by the page to show the parameters.
			var process = {
				properties: req.query.job_process_info.properties,
				processes: job_process,
				gas: req.query.job_process_info.gas
			};
			if(err == null) { // this is executed when NO parameters exist in the db.
				process.processes[0].params = paramsObj;
			}
			// The kerf in the process from the DB may not be filled in. In this case, we need to get the kerf
			// AGAIN from the machine settings object.
			if(process.processes[0].params.kerf == null) {
				process.processes[0].params.kerf = dbManager.getMachineSettingsObj().kerf;
			}
			// Yes, the parameters to create this new process ARE provided.
			processTableHelper.generateDisplayProcessObject(process, function(displayObj) {
				dbManager.getProcessPinSettings(function (err, pinSettings) {
					dbManager.getDisplayUnit(function(unit) {
						res.render('processCreator', {
							title: 'Process creator',
							file_name: file_name,
							process: JSON.stringify(process),
							// need to pass this along for display only
							displayProcess: JSON.stringify(displayObj),
							process_to_copy: req.query.process_to_copy,
							err: null,
							default_kerf: kerf,
							currentUnit: unit,
							gasSettings: gasPressureForDisplay(unit),
							pinSettings: JSON.stringify(pinSettings),
							pinEntered: status.processes.processPinEntered
						});
					});
				})
			});
		});
	}
	else {
		// get the current gas on the machine and display a blank page
		processTableHelper.getCurrentGas(function(err, gasName) {
			var process = {
				properties: req.query.job_process_info.properties,
				processes: job_process,
				gas: gasName
			};
			// NO, there are NO parameters to show on the creator page.
			// Add some default parameters
			dbManager.getDisplayUnit(function(unit) {
				// TODO later, apply default parameters to the tube
				// TODO ex) process.processes[0].params = makeDefaultParameters();
				if (unit == 'MM') {
					kerf = processTableHelper.convertToMetric.kerf(dbManager.getMachineSettingsObj().kerf);
				}
				else {
					kerf = processTableHelper.roundParametersIN.kerf(dbManager.getMachineSettingsObj().kerf);
				}
				dbManager.getProcessPinSettings(function (err, pinSettings) {
					res.render('processCreator', {
						title: 'Process creator',
						file_name: file_name,
						process: JSON.stringify(process),
						displayProcess: null,
						err: null,
						gas: gasName,
						default_kerf: kerf,
						currentUnit: unit,
						gasSettings: gasPressureForDisplay(unit),
						pinSettings: JSON.stringify(pinSettings),
						pinEntered: status.processes.processPinEntered
					});
				})
			});
		});
	}
});

/** Return an object with default process parameters.
 *
 */
function makeDefaultParameters() {
	return {
		laser_mode: 2,
		pressure: 100,
		nozzle_gap: 0.03,
		focus_offset: 0,
		pierce_laser_mode: 2,
		pierce_power: 100,
		pierce_frequency: 250,
		pierce_pulses: 1,
		pierce_duration: 15,
		pierce_nozzle_gap: 0.06,
		pierce_pressure: 100,
		pierce_gas_delay: 0
	}
}

/*  ----------- POST /processes/  -----------  */
/* creates a new entry into the process table
 * req.body will be a process object.
 {
 "gas" : <str>,
 "processes" : [{	// this is a SINGLE object in this array.
 name: <str>,
 number: <int>,
 *type: <str>,
 params: <obj>
 }]
 "properties" : <obj>
 }
 */

router.post('/', function(req, res) {
	var processObj = req.body;
	var currentProcess = processObj.processes[0];
	if(currentProcess.name == "RASTER") {
		processObj.processes[0].params.laser_mode = 0; 	// force laser mode to CW
	}
	processTableHelper.insertEntryIntoDB(processObj, res);
});

/*  ----------- PUT /processes  -----------  */
/* update the requested item (material, thickness, gas, process name, or params
 * format of req.body (data is sent through ajax call):
 *
 * req.body.data:
 * {
 fileName: fileName,
 process: processObj,
 updateData: {
 typeOfUpdate: <string: 'material', 'thickness', 'gas', 'process', 'params'>,
 temporary: <bool>,	// if TRUE save params to processTableHelper
 data: <object> {
 feedrate: <float>,
 ...
 pressure: <float>
 }
 }
 * }
 */
router.put('/', function(req, res) {
	var request = JSON.parse(req.body.data);	// parse the stringified JSON
	var process = request.process;
	var typeOfUpdate = request.updateData.typeOfUpdate;
	var SQLcommand;
	if(typeOfUpdate === 'params') {
		var currentProcess = process.processes[0];
		if(request.updateData.temporary === true) { // Saving a temp process
			// Update master process obj's parameters only for the requested process.
			// Important because a processObj can have multiple processes but only want
			// to update the one that was PUT
			for(var i = 0; i < processTableHelper.masterProcessObj.processes.length; i++) {
				if(processTableHelper.masterProcessObj.processes[i].name === currentProcess.name) {
					processTableHelper.masterProcessObj.processes[i].params = request.updateData.data;
					processTableHelper.masterProcessObj.processes[i].temporary = true;
				}
			}
			// send the page response
			res.sendStatus(204);
			res.end();
		}
		else {	// Saving a process to the DB. Either: a) update the DB parameters OR b) create a new USER row.
			// First build the object that will be used to update/make the row
			var laserParamRow = {
				"laser_parameter_id": null,
				"user_id": null,
				"unit": null,
				"acceleration": null,
				"feedrate": null,
				"power": null,
				"min_power": null,
				"frequency": null,
				"pressure": null,
				"gas_delay": null,
				"nozzle_gap": null,
				"focus_offset": null,
				"lift_height": null,
				"laser_mode": null,
				"pierce_laser_mode": null,
				"pierce_power": null,
				"pierce_frequency": null,
				"pierce_pulses": null,
				"pierce_duration": null,
				"pierce_nozzle_gap": null,
				"pierce_pressure": null,
				"pierce_gas_delay": null,
				"kerf": null,
				"is_default": null,
				"gas_id": null
			};
			// Populate this object with the data from the ajax request form
			var paramsToUpdate = request.updateData.data; 	// object with laser params as key, value pairs
			for(var param in paramsToUpdate) {
				laserParamRow[param] = paramsToUpdate[param];
			}
			var processUnit = laserParamRow.unit;
			// Manually add in these items
			laserParamRow.gas_delay = (laserParamRow.gas_delay == null) ?
				0 : laserParamRow.gas_delay;
			laserParamRow.gas_id = (laserParamRow.gas_id == null) ?
				currentProcess.params.gas_id : laserParamRow.gas_id;
			laserParamRow.is_default = 1; // HARDCODED to 1 to make this default.
			laserParamRow.user_id = 2;	// HARDCODED USER_ID IS ALWAYS 2=USER
			laserParamRow.laser_parameter_id = (laserParamRow.laser_parameter_id == null) ?
				currentProcess.params.laser_parameter_id : laserParamRow.laser_parameter_id;
			// At this point laserParamRow has the complete definition of a row we are inserting or updating.

			// You are now saving to DB. In all cases, we first get the parameters from the DB,
			// even if we already had them (if previously using a saved process). This is because if
			// we were previously using a temporary process, some params are missing in the laserParamRow obj
			// like laser_parameter_id.
			// So just to be safe, always get the parameters from the database again.
			dbManager.getParamOptions(process, currentProcess, function(err, options) {
				// Determine if we have a USER and/or a FACTORY process in the DB.
				var userProcess = [];
				var factoryProcess = [];
				for(var n = 0; n < options.optionsList.length; n++) {
					if(options.optionsList[n].name == "USER") {
						userProcess.push(options.optionsList[n]);
					}
					else if(options.optionsList[n].name == "FACTORY") {
						factoryProcess.push(options.optionsList[n]);
					}
				}
				// If there is one user process:
				if(userProcess.length > 0) {
					// Update the laserParamRow based on the process given in the DB.
					var laserParamRowFromDB = userProcess[0].params;
					laserParamRow.is_default = 1; // HARDCODED to 1 to make this default.
					laserParamRow.user_id = 2;	// HARDCODED USER_ID IS ALWAYS 2=USER
					laserParamRow.laser_parameter_id = laserParamRowFromDB.laser_parameter_id;
					laserParamRow.gas_id = laserParamRowFromDB.gas_id;
					laserParamRow.unit = processUnit;
					// If there is a USER process available, then we need to update it.
					// logger.log('[HMI] A USER process exists. Proceeding to update....');
					SQLcommand = SQLcommands.update_laser_parameter_row_from_params(laserParamRow);
					dbManager.db.run(SQLcommand, function(err) {
						if(err) throw err;
						// Finally, update the master process object and send the page response.
						for(var i = 0; i < processTableHelper.masterProcessObj.processes.length; i++) {
							if(processTableHelper.masterProcessObj.processes[i].name === currentProcess.name) {
								processTableHelper.masterProcessObj.processes[i].temporary = false;
								processTableHelper.masterProcessObj.processes[i].params = laserParamRow;
							}
						}
						res.sendStatus(204);
						res.end();
						// logger.log('[HMI] Updated entry ID = ' + laserParamRow.laser_parameter_id);
					});
					// If there also is a FACTORY process available, then make that NOT default.
					if(factoryProcess.length > 0) {
						dbManager.db.run(
							SQLcommands.set_laser_parameter_row_to_default(
								factoryProcess[0].params.laser_parameter_id, false));
					}
				}
				// If there is no user process, we assume there is a factory:
				else {
					// Update the laserParamRow based on the process given in the DB.
					var factoryParams = factoryProcess[0].params;
					laserParamRow.is_default = 1; // HARDCODED to 1 to make this default.
					laserParamRow.user_id = 2;	// HARDCODED USER_ID IS ALWAYS 2=USER
					laserParamRow.laser_parameter_id = factoryParams.laser_parameter_id;
					laserParamRow.gas_id = factoryParams.gas_id;
					laserParamRow.unit = processUnit;
					// If there is no USER process available, then we need to insert a new one
					// AND make the FACTORY not default.
					// Make the Factory is_default = 0
					if(factoryProcess.length > 0) {
						dbManager.db.run(
							SQLcommands.set_laser_parameter_row_to_default(
								factoryProcess[0].params.laser_parameter_id, false));
					}
					logger.log('[HMI] No existing USER process. Inserting new one.');
					var processObjToInsert = {
						properties: process.properties,
						processes: [{
							name: currentProcess.name,
							number: currentProcess.number,
							params: laserParamRow,
							temporary: false
						}],
						gas: process.gas
					};
					if("type" in currentProcess) { processObjToInsert.processes[0].type = currentProcess.type; }
					// Insert a new one...
					processTableHelper.insertEntryIntoDB(processObjToInsert, res);
					// ...And update the master process object.
					for(var i = 0; i < processTableHelper.masterProcessObj.processes.length; i++) {
						if(processTableHelper.masterProcessObj.processes[i].name == currentProcess.name) {
							processTableHelper.masterProcessObj.processes[i].temporary = false;
							processTableHelper.masterProcessObj.processes[i].params = laserParamRow;
						}
					}
				}
			});

		}
	}
	else {
		switch(typeOfUpdate) {
			case 'material':
				SQLcommand = 'UPDATE Materials SET MaterialName = \'' + request.newValue + '\' WHERE MaterialName = \'' + request.oldValue + '\';';
				break;
			case 'thickness' :
				break;
			case 'process' :
				SQLcommand = 'UPDATE Processes SET ProcessName = \'' + request.newValue + '\' WHERE ProcessName = \'' + request.oldValue + '\';';
				break;
			case 'gas' :
				SQLcommand = 'UPDATE GasTypes SET GasName = \'' + request.newValue + '\' WHERE GasName = \'' + request.oldValue + '\';';
				break;
		}
		dbManager.db.run(SQLcommand);
		res.sendStatus(205);
	}
});


/** Given a unit which is either inches or millimeters, get the
 *
 * @param unit {string} - either 'IN' or 'MM'
 * @returns {{regex: RegExp, maxPressure: *}}
 */
function gasPressureForDisplay(unit) {
	var machineSettings = dbManager.getMachineSettingsObj();
	var maxGasPressure = machineSettings.specs.gas.max_pressure;
	var minGasPressure;
	var gasSettingsRegex;
	var gasUnit;
	if(unit === 'IN') {
		gasUnit = 'PSI';
		minGasPressure = 5;
		gasSettingsRegex = new RegExp('^' + toRegexRange(minGasPressure, maxGasPressure) + '$');
		// Add extra backslash for front end
		gasSettingsRegex = gasSettingsRegex.toString().replace(/\\/g, "\\\\");
		if(gasSettingsRegex.charAt(gasSettingsRegex.length - 1) === '/') {
			gasSettingsRegex = gasSettingsRegex.slice(0, -1);
		}
		if(gasSettingsRegex.charAt(0) === '/') {
			gasSettingsRegex = gasSettingsRegex.slice(1, gasSettingsRegex.length);
		}
	}
	else {
		gasUnit = ' bar';
		maxGasPressure = (maxGasPressure < 200) ? 10.0: 20.0;
		minGasPressure = 0.5;
		if(maxGasPressure === 10) {	// Low pressure regulator goes up to 10 bar
			gasSettingsRegex = '^(0+\\.[5-9]+[0-9]*|[1-9]|[1-9]\\..*|10)$';
		}
		else {
			gasSettingsRegex = '^(0+\\.[5-9]+[0-9]*|[1-9]|[1-9]\\..*|1[0-9]|1[0-9](?:\\.[0-9]*)|20)$';
		}
	}
	return {
		regex: gasSettingsRegex,
		maxPressure: maxGasPressure,
		minPressure: minGasPressure,
		unit: gasUnit
	}
}

module.exports = router;