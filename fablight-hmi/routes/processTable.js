/*
 * processtable.js
 *
 * Routes for /processtable to upload and download the processDatabase.db
 * Uses code from dbManager.js
 *
 * Routes:
 * PUT /processtable/ with request object { action: 'download' or 'upload' }
 *
 */

var express = require('express');
var router = express.Router();
var path = require('path');
var dbManager = require('../dbManager');

// for file upload
var multer = require('multer');
var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, __dirname + '/../public/processTable');
	},
	filename: function(req, file, cb) {
		var filename = file.originalname;
		var extension = path.extname(filename);
		cb(null, "processTable-" + Date.now() + extension);
	}
});
var upload = multer({
	storage: storage,
	fileFilter: function(req, file, callback) {
		if (path.extname(file.originalname).indexOf('csv') === -1) {
			return callback(new Error('Wrong extension type'));
		}
		callback(null, true);
	}
}).single('processTable');

/*
 *	
 *	ROUTES
 *	
 */

/*
 * PUT /processtable
 *
 * Request is of the form
 * { action: 'download' or 'upload' }
 *
 */
router.put('/', function(req, res) {
	switch(req.body.action) {
		case 'download':
			dbManager.downloadDB(function(err, dbName) {
				if(err !== null) {
					res.status(204).send({
						err: 'No removable USBs found'
					}).end();
				}
				else {
					res.status(200).send({
						err: null,
						message: 'Copied ' + dbName + ' to USB.'
					}).end();
				}
			});
			break;
		case 'upload':
			dbManager.uploadDB(req.body.usbFileName, function(err) {
				if(err) {
					res.status(204).send({
						err: 'No removable USBs found'
					}).end();
				}
				else {
					dbManager.check_machine_settings(function() {
						res.status(200).send({
							err: null
						}).end();	
					})
				}
			});
			break;
		default:
			res.end();
			break;
	}
});

module.exports = router;