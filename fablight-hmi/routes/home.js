/*
 * home.js
 * Routes for the /home page
 * */

var express = require('express');
var router = express.Router();
var appRoot = require('app-root-path');
var status = appRoot.require('machineStatus');
var config = appRoot.require('config');
var packageJSON = appRoot.require('package.json');

// ---------- ROUTES ---------- //
/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('home', {
		title: 'HMI',
		machineOnline: status.isOnline,
		config: JSON.stringify(config),
		hmiVersion: packageJSON.version
	});
});

module.exports = router;