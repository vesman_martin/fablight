/*
 * settings.js
 *
 * Routes for the /settings page
 * Allows the user to set the gas used in the process table
 */

var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var appRoot = require('app-root-path');
var moment = require('moment');
var archiver = require('archiver');
var rimraf = require('rimraf');
var spawn = require('child_process').spawn;

var dbManager = appRoot.require('dbManager');
var usbManager = appRoot.require('usbManager');
var processTableHelper = appRoot.require('processTableHelper');
var logger = appRoot.require('logger');
var config = appRoot.require('config');

var packageJSON = appRoot.require('package.json');

router.get('/', function(req, res) {
	// get the current gas from the DB
	processTableHelper.getCurrentGas(function(err, currentGas) {
		dbManager.getDBname(function(err, dbName) {
			dbManager.getDisplayUnit(function(unit) {
				var rotaryExists = false;
				if(dbManager.machineSettingsObj.specs.rotary.rotary_exists === true) {
					rotaryExists = true;
				}
				res.render('settings', {
					title: 'Settings',
					currentGas: currentGas,
					currentDBname: dbName,
					hmiVersion: packageJSON.version,
					currentUnit: unit,
					rotaryExists: rotaryExists
				});
			});
		});
	});
});

router.get('/rotCenterY', function(req, res) {
	res.render('settingsPages/rotCenterY', {
		title: 'Rotary center Y'
	});
});

router.get('/chs', function(req, res) {
	res.render('settingsPages/chs', {
		title: 'Capacitive Height Sensor'
	});
});

router.get('/focusTest', function(req, res) {
	res.render('settingsPages/focusTest', {
		title: 'Focus Test'
	});
});

router.get('/beamCentering', function(req, res) {
	res.render('settingsPages/beamCentering', {
		title: 'Beam Centering',
		options: JSON.stringify(dbManager.getMachineSettingsObj().beam_centering)
	});
});

/* 	GET /uploadProcessTable
	Get a list of all .db files on USB and render the uploadProcessTable page.
 */
router.get('/uploadProcessTable', function(req, res) {
	// get the list of all connected USB devices
	usbManager.getUSBList( function(err, usb, mountpoint) {
		// if there is NO removable and mounted USB connected
		if(err) {
			logger.log('[HMI] ' + err);
			res.render('settingsPages/uploadProcessTable', {
				title: 'Upload Process Table',
				usb: null,
				files: null
			});
		}

		// if there IS a USB connected, its info is in the "usb" obj
		else {
			usbManager.getDBFileNames(mountpoint, function(files) {
				// files is an array of all file names
				res.render('settingsPages/uploadProcessTable', {
					title: 'Upload Process Table',
					usb: usb,
					files: files
				});
			});
		}
	});
});

/* 	POST /uploadProcessTable
	Actually upload the selected USB file to the HMI.
 */
router.post('/uploadProcessTable', function(req, res) {
	var usbFileName = req.body.usbFileName;
	dbManager.uploadDB(usbFileName, function(err) {
		if(err) {
			res.status(200).json({
				err: err
			}).end();
		}
		else {
			res.status(200).json({
				err: null
			}).end();
		}
	});
});

/*
 * GET /laser (/settings/laser)
 *
 * Laser status page
 */
router.get('/laser', function(req, res) {
	res.render('settingsPages/laser', {
		title: 'Laser status page'
	});
});

/*
 * GET /machineStatusViewer (/settings/machineStatusViewer)
 *
 * Laser and controller status page
 */
router.get('/machineStatusViewer', function(req, res) {
	res.render('settingsPages/machineStatusViewer', {
		title: 'Machine Status'
	});
});

/*
 * GET /adminLogin (/settings/adminLogin)
 *
 * Page to log in as an administrator.
 */
router.get('/adminLogin', function(req, res) {
	res.render('settingsPages/adminLogin', {
		title: 'Admin login',
		adminPassword: dbManager.getMachineSettingsObj().admin_password
	});
});

/*
 * GET /admin (/settings/admin)
 *
 * Admin page to set and manage process PIN
 */
router.get('/admin', function(req, res) {
	dbManager.getProcessPinSettings(function (err, pinSettings) {
		res.render('settingsPages/admin', {
			title: 'Admin settings',
			pinSettings: JSON.stringify(pinSettings)
		});
	})
});



/*
 * PUT /log (/settings/log)
 *
 * Download the log file from the HMI.
 */
router.put('/log', function(req, res) {
	downloadLog(function(err) {
		res.status(200).send({
			err: err
		}).end();
	});
});

/** Download the hmi log on UDOO to the root of the USB drive.
 * First generates the archive in the /logs/ directory, then copies it to the USB drive.
 *
 * @param callback(err)
 */
function downloadLog(callback) {
	// First check if there's a USB plugged in
	usbManager.getUSBPath(function(err, mountpoint) {
		if(err) {
			callback('No USB found.');
		}
		else {
			if(config.os.os.toLowerCase() === 'linux') {
				if(config.os.release === 16.04) {
					downloadUbuntuLog(mountpoint, callback);
				}
				else {
					// UDOO
					downloadUDOOlog(mountpoint, callback);
				}
			}
			else {
				// Mac
			}
		}
	});
}

function downloadUDOOlog(mountpoint, callback) {
	var timeStamp = moment().format('YYYY-DD-MM')
	var localLogPath = path.join(appRoot.path, 'logs', 'fablight_LOG_' + timeStamp + '.zip');
	var usbLogPath = path.join(mountpoint, 'fablight_LOG_' + timeStamp + '.zip');
	// console.log(localLogPath);
	/// create a file to stream archive data to.
	var output = fs.createWriteStream(localLogPath);
	var archive = archiver('zip', {
		zlib: { level: 9 } // Sets the compression level.
	});

	// listen for all archive data to be written
	// 'close' event is fired only when a file descriptor is involved
	output.on('close', function() {
		// console.log(archive.pointer() + ' total bytes');
		// console.log('archiver has been finalized and the output file descriptor has closed.');
	});

	// This event is fired when the data source is drained no matter what was the data source.
	// It is not part of this library but rather from the NodeJS Stream API.
	// @see: https://nodejs.org/api/stream.html#stream_event_end
	output.on('end', function() {
		// console.log('Data has been drained');
	});

	var warnings = null;
	// good practice to catch warnings (ie stat failures and other non-blocking errors)
	archive.on('warning', function(err) {
		if (err.code === 'ENOENT') {
			// log warning
			warnings = 'Warning: Log file does not exist';
		} else {
			// throw error
			warnings = err;
		}
	});

	var errors = null;
	// good practice to catch this error explicitly
	archive.on('error', function(err) {
		errors = err;
		console.log('[HMI] Error creating log file: ' + err);
	});

	// pipe archive data to the file
	archive.pipe(output);

	// append a file. First arg is location, second is parameters including name inside zip.
	archive.file(path.join(config.logging.location, config.logging.file_name), { name: 'fablight-hmi-0.log' });
	archive.file(path.join(config.logging.location, config.logging.file_name + '.1'), { name: 'fablight-hmi-1.log' });
	archive.file(path.join(config.logging.location, config.logging.file_name + '.2'), { name: 'fablight-hmi-2.log' });
	archive.file(path.join(config.logging.location, config.logging.file_name + '.3'), { name: 'fablight-hmi-3.log' });
	archive.file(path.join(config.logging.location, config.logging.file_name + '.4'), { name: 'fablight-hmi-4.log' });

	// console.log('file location: ' + path.join(config.logging.location, config.logging.file_name + '.4.gz'));

	archive.on('finish', function (err) {
		if(err) {
			callback(err);
		}
		else {
			// Only proceed if there are no errors. There could be warnings as well, but we're ignore those for now.
			if(errors === null) {
				// The file has been created in the /logs/ directory. Now copy to the USB drive
				usbManager.copyFileToUSB(localLogPath, usbLogPath, function (err) {
					if(err) {
						callback(err)
					}
					else {
						// Remove the file in the /logs/ directory.
						rimraf(localLogPath, function(err) {
							callback(err);
						})
					}
				})
			}
			else {
				callback(err);
			}
		}
	})

	// finalize the archive (ie we are done appending files but streams have to finish yet)
	// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
	archive.finalize();
}

function downloadUbuntuLog(mountpoint, callback) {

}

module.exports = router;