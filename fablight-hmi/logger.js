/* logger.js
 * Log to the HMI's log using console.log. Add a date and time stamp.
 *
 * Usage:
 * var require('logger');
 * logger.log(<str>);
 */

var moment = require('moment');

module.exports = {
	/**
	 *
	 * @param message {str} - Message to log
	 * @param addTimeStamp {bool} - If true, add a timestamp. True by default.
	 */
	log: function (message, addTimeStamp) {
		var timeStamp = moment().format('YYYY-MM-DD HH:mm:ss');
		var loggedMessage;
		if(addTimeStamp === false) {
			loggedMessage = message;
		}
		else {
			loggedMessage = timeStamp + ': ' + message;
		}
		process.stdout.write(loggedMessage.toString()+'\r\n');
	}
};