/*
 * networkSocketFunctions.js
 *
 * Contains all TCP, UDP code that runs on the machine. Easily get the state of the machine.
 *
 */

// networking
var net = require('net');			// for TCP socket
var dgram = require('dgram'); 		// for UDP
var readline = require('readline');	// for parsing TCP info
var split = require('split');

// load modules
var fs = require('fs');
var path = require('path');

// machine status
var status = require('./machineStatus');	// for keeping track of state info
var config = require('./config');	// for keeping track of state info
var socketData = require('./socketDataFunctions');	// for TCP onData functions

// ping for port 29
var pingPort29;
var pingPort29delay = 5000;

// buffer messages
var port29statusRequest = new Buffer([0xFF, 0xE0, 0x0A, 0x00]);

module.exports = {

	TCPport: function(port, IPaddress) {
		this.port = port;
		this.IPaddress = IPaddress;
		this.socket = null;
		this.create = function() {
			this.socket = net.connect(this.port, this.IPaddress);
		}
	},

	UDPsocket: function(port, IPaddress) {
		this.port = port;
		this.IPaddress = IPaddress;
		
	},

	/* pingTCPport: pings the TCP port at the given IP address.
	 * 
	 * @param1	port	integer
	 * @param2	address	string
	 * @param3	delay	integer (ms)
	 * @param3	callback	<err>
	 */
	pingTCPport: function(port, address, delay, callback) {

		if(status.laserSocket29 != null) {
			pingPort29 = setInterval(function() {
				status.laserSocket29.write(port29statusRequest);
			}, pingPort29delay);
		}
	},

	/* pingUDPport: pings the UDP port at the given IP address.
	 * 
	 * @param1	port	integer
	 * @param2	address	string
	 * @param3	delay	integer (ms)
	 * @param3	callback	<err>
	 */
	pingUDPport: function(port, address, delay, callback) {
		setInterval(function() {
			
		})
	},

	/* pingUDPport: pings the UDP port at the given IP address.
	 * 
	 * @param1	port	integer
	 * @param2	address	string
	 * @param3	delay	integer (ms)
	 * @param3	callback	<err>
	 */
	setUpUDPsocket: function(port, address, delay, callback) {
		setInterval(function() {

		})
	},

	/* setUpUDP - binds event listeners to UDPreceive object.
	 * These should only be bound ONCE, which is why this is in a separate function.
	 */
	setUpUDP: function() {
		status.udp.socketReceive.on('listening', function() {
			res.io.emit('udp-status', 'Finding machine...');
			pingUDP(); // start pinging UDP port.
		});

		// when a message is received, open TCP socket.
		status.udp.socketReceive.on('message', function(msg, rinfo) {
			msg = msg.toString();
			// verify message contains "FabLight" or "RPC"
			if((msg.indexOf('FabLight') > -1) || (msg.indexOf('RPC') > -1)){
				status.udp.lastUDPreceived = Date.now();
			}
		});
		status.udp.socketReceive.on('error', function(err) {
			res.io.emit('udp-status', 'UDP error.');
		});
	},
	
	pingUDP: function() {
	}

};