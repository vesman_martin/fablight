// This is the Client Program for Fablight Agent
// Author: Dharani Vengadesh
// Licensed: Maxelerator
// Version: 1.0.0
// Date: Apr 19, 2023
// Port Numbers used: 8000 and 8080
// The program performs the following steps:
// Step 1: Create a signed certificate with the help of CSR if it doesn't exist.
// Step 2: Authenticate the certificate.
// Step 3: Connect to the WebSocket for communication with the server.

const forge = require('node-forge');
const io = require('socket.io-client');
const fs = require('fs');
const readline = require('readline');
const { exec } = require('child_process');
require('dotenv').config();

// UserName Password For verification Process When Request a Cert
let userName = process.env.userName;
let password = process.env.password;

// Check if client have the cert 
fs.access('keys/clientCert.pem', (err) => {
  if (err) {
      console.log("client do not have a cert");

      const socket = io.connect(`https://${process.env.hostName}:8080`, { rejectUnauthorized: false });

      socket.on('connect', async () => {
        console.log('Connected to server.');

        //verify the user
        console.log("Sending User Credentials.....")
        socket.emit('userDetails', {
          userName: userName,
          passWord: password
        });
      });

      //Handle Verification status

      socket.on('verified',async (status) => {
        console.log(status);
        if(status){
          // Generate a new RSA key pair for the client certificate
          const keySize = 4096;
          const keys = forge.pki.rsa.generateKeyPair({ bits: keySize });

          // Create a new certificate signing request (CSR)
          const csr = forge.pki.createCertificationRequest();
          csr.publicKey = keys.publicKey;
          csr.setSubject([
              { name: 'commonName', value: process.env.clientCommonName },
              { name: 'countryName', value: process.env.clientCountryName},
              { shortName: 'ST', value: process.env.clientState },
              { name: 'localityName', value: process.env.clientLocalityName },
              { name: 'organizationName', value: process.env.clientOrgName },
              { shortName: 'OU', value: process.env.clientUnitName },
          ]);
          fs.writeFileSync('keys/clientPrivateKey.pem', forge.pki.privateKeyToPem(keys.privateKey));
          console.log("clientKey saved");
          // Sign the CSR with the private key
          csr.sign(keys.privateKey);

          // Convert the CSR to PEM format
          const csrPem = forge.pki.certificationRequestToPem(csr);

          // Write the CSR to disk
          fs.writeFileSync('keys/clientCsr.pem', csrPem);
          console.log("clientCSR saved");

          console.log('Sending CSR to server...');
          socket.emit('csr', csrPem);
        }else{
          console.log("Credentials Not Match");
        }
      })

      // Got the cert from server
      socket.on('cert', (cert) => {
        console.log('Received certificate from server:');
        fs.writeFileSync('keys/clientCert.pem', cert);
        console.log(cert);
        socket.disconnect();
      });

      socket.on('disconnect', () => {
        console.log('Disconnected from Certificate Provider.');
        connectToTheServer();
      });
  } else {
    console.log('Client Have Certificate');
    connectToTheServer();
  }
});

// If Client Have the Cert Connect to the Server
function connectToTheServer(){
  let username = userName;
  const options = {
      ca: fs.readFileSync('keys/rootCaCert.pem'),
      key: fs.readFileSync('keys/clientPrivateKey.pem'),
      cert: fs.readFileSync('keys/clientCert.pem'),
      host: process.env.hostName,
      port: 8000,
      rejectUnauthorized:true,
      requestCert:true
  };

  const socket = io(`https://${options.host}:${options.port}`, {
      ca: options.ca,
      key: options.key,
      cert: options.cert,
      rejectUnauthorized: options.rejectUnauthorized,
      requestCert: options.requestCert,
      secure: true
  });

  socket.on('connect', () => {
      console.log('client connected');
      socket.emit('add user', username);
      socket.emit('subscribe', 'topic1');
  });

  //receive the command details from the server
  socket.on('send command', (commandDetails) => {
  
    console.log(commandDetails);
    let command = commandDetails['command'];
    let sender = commandDetails['sender'];
    
    const child = exec(command);

    child.stdout.on('data', (data) => {
      socket.emit('response message',data,sender);
    });
  
    child.stderr.on('data', (data) => {
      socket.emit('response message',data,sender);
    });
  
    child.on('error', (error) => {
      socket.emit('response message',error.message,sender);
    });

    child.on('close', (code) => {
      socket.emit('response result',code,sender);
    });
  
  });
  
  //receive the responses
  socket.on('response message', (data) => {
    console.log("\n From: "+data['username']);
    console.log("\n"+data['message']);
  });
  socket.on('errorMessage', (data) => {
    console.log(data);
  });
  socket.on('successMessage', (data) => {
    console.log(data);
  });
  // Define a function to emit the heartbeat event
  function emitHeartbeat() {
    socket.emit('heartbeat', { username: username, currentTime: new Date().toLocaleString() });
  }

  socket.on('sendChunk', (fileInfo) => {
    const fileName = fileInfo.fileName;
    const fileSize = fileInfo.fileSize;
    const chunkSize = fileInfo.chunkSize;
    const chunkNum = fileInfo.chunkNum;
    const chunkData = fileInfo.chunkData;
    // console.log(`receiving chunk data:${chunkData}`)
    console.log(`Receiving file: ${fileName}, size: ${fileSize}`);
    const writeStream = fs.createWriteStream(`uploads/${fileName}`, {
      flags: 'a'
    });
    writeStream.write(chunkData);
    writeStream.end();
  });

  // Emit the heartbeat event every 10 seconds
  setInterval(emitHeartbeat, 10000);
}