#!/bin/bash
# Given a complete zip file for an FabServer node.js application,
# this script sets up the FabServer on an existing HMI.
#
# For consistency, place this script into /opt/ directory.
#
# Usage: sudo bash setupfabserver.sh <path to zipped fabserver file>.zip

set -u      # Throw error if an variable is referenced before being set
set -e      # Exit as soon as any line fails
# set -x      # Shows the commands that get run

bold=$(tput bold)
normal=$(tput sgr0)

usage() { echo "Usage: sudo bash $0 [fabserver file].zip" 1>&2; }
# If there isn't exactly one argument then exit.
if [[ $# > 1 ]]; then
    usage
    exit 1
else
    if [[ $# == 0 ]]; then
	usage
        echo -e "\e[43m Warning: \e[49m No FabServer package provided when running this script."
        echo "After this script is run, you will need to install the FabServer package manually."
        echo "Do you wish to continue? [y/n]"
        read continue
        if [[ "$continue" == "y" ]]; then
            echo "Continuing script..."
        else
            echo "Exiting script."
            exit 2
        fi
    fi
fi

echo -e "\e[42mStarting the 3D Fab Light set up FabServer script...VERSION 1.0\e[49m"
newFabServer=$1
newFabServername="$(basename "$newFabServer" ".zip")"

# Print a statement to the terminal and color code it. Takes 2 parameters:
# $1 - level. 0=error, 1=warning, 2=info
# $2 - message
printMsg() {
    levelString=""
    startFormat=""
    endFormat="\e[39m"
    if [[ $1 == 0 ]]; then
        levelString="\e[41m Error: \e[49m"
        startFormat="\e[31m"
    elif [[ $1 == 1 ]]; then
        levelString="\e[43m Warning: \e[49m"
        startFormat="\e[33m"
    else
        levelString="\e[42m Info: \e[49m"
        startFormat="\e[32m"
    fi
    echo -e "$levelString $startFormat $2 $endFormat"
}

printMsg 2 "Installing FabServer"
cd /tmp
unzip ${newFabServer}
if [[ -d /opt/fabserver ]]; then
   cd /opt
   zip -r /tmp/fabserver_old.zip fabserver
   cd /tmp
fi
rm -rf /opt/fabserver
mv fabserver /opt

NEW_SCRIPT_LOCATION="/opt/fabserver/setup/"
FILE_AVAHI_SERVICE="/etc/avahi/services/fabserver.service"
FILE_FABSERVER_SERVICE="/etc/systemd/system/fablight-fabserver.service"

printMsg 2 "Copying new service scripts"
# fablight-fabserver.service
cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_FABSERVER_SERVICE}) $(dirname ${FILE_FABSERVER_SERVICE})
printMsg 2 "Copied fablight-fabserver.service"

printMsg 2 "Copying new avahi service scripts"
# fabserver.service
cp ${NEW_SCRIPT_LOCATION}$(basename ${FILE_AVAHI_SERVICE}) $(dirname ${FILE_AVAHI_SERVICE})
printMsg 2 "Copied fabserver.service"

printMsg 2 "Enable the service"
systemctl enable fablight-fabserver

