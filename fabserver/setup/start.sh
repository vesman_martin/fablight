#!/bin/bash
# This script needs to stay within this app location at:
# /opt/fabserver/setup/start.sh

echo "fabserver start.sh script starting..."
echo "sourcing nvm"
. /home/oem/.nvm/nvm.sh
echo "sourced nvm, now starting npm"
npm start
