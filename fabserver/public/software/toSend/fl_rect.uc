ifmissing SHAPE_X 0.0 fvar SHAPE_X
ifmissing SHAPE_Y 0.0 fvar SHAPE_Y
` ifexists fl_rect forget fl_rect
: fl_rect
    #fstk 3 < #istk 2 < or if "" <f.width> <f.height> <f.feedrate> <accel 0/1/2> <dir 0/1> fl_rect" syntax :exit endif
    == FLC_DIR
    == FLC_ACCEL
    == FLC_FEED
    == SHAPE_Y
    == SHAPE_X
   
    xchk if :exit endif
    
    #set_home
    start_motion
    FLC_FEED == MACH_FEED
    FLC_ACCEL >ACCEL_MODE set_accel_mode
    machine_mode
    nl " fl_rect"
    loop
        ?terminal until
    
        FLC_DIR if
            SHAPE_X 0.5 f* SHAPE_Y shape_ac ` start in middle Max Y
            SHAPE_X SHAPE_Y shape_ac
            SHAPE_X 0.0 shape_ac
            0.0 0.0 shape_ac
            0.0 SHAPE_Y shape_ac
            SHAPE_X 0.5 f* SHAPE_Y shape_ac ` end in middle Max Y
        else
            SHAPE_X 0.5 f* SHAPE_Y shape_ac ` start in middle Max Y
            0.0 SHAPE_Y shape_ac
            0.0 0.0 shape_ac
            SHAPE_X 0.0 shape_ac
            SHAPE_X SHAPE_Y shape_ac
            SHAPE_X 0.5 f* SHAPE_Y shape_ac ` end in middle Max Y
        endif
        fp
    repeat
    end_motion
    1.0 set_accel_factor
    slew_mode
;

