/* config.js
 *
 * Stores app configuration (either 'development' or 'production') as well as static
 * data about the machine
 */
const env = 'production'; //process.env.NODE_ENV;	// 'development' or 'production'
const appRoot = require('app-root-path');
const path = require('path');

const development = {
	production: false,
	files: {
		path: path.join(appRoot.path, 'public', 'uploads'),
		tmpPath: path.join(appRoot.path, 'public', 'tmp')
	},
	machine: {
		IPaddress: '192.168.3.232',  // IP Address of controller if not 192.168.3.232
		jobPort: 23,
		debugPort: 43,
		commandPort: 33
	},
	logs: {
		userDateFormat: 'hh:mm:ss'
	},
	softwareUpdate: {
		fileName: null,
		path: null,
		location: null
	}
};

const production = {
	production: true,
	files: {
		path: '/export/fabserver',
		// path: path.join(appRoot.path, 'public', 'uploads'),
		tmpPath: path.join(appRoot.path, 'public', 'tmp')
	},
	machine: {
		IPaddress: '192.168.3.232',
		jobPort: 23,
		debugPort: 43,
		commandPort: 33
	},
	logs: {
		userDateFormat: 'hh:mm:ss'
	},
	softwareUpdate: {
		fileName: null,
		path: null,
		location: null
	}
};

const config = {
	development,
	production
};

if(typeof config[env] === 'undefined') {
	config[env] = production;
}
console.log('[config.js] env: ' + env);

module.exports = config[env];
