// Module dependencies
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var appRoot = require('app-root-path');

var routes = require('./routes/index');
var users = require('./routes/users');
var upload = require('./routes/upload');
var files = require('./routes/files');
var terminal = require('./routes/terminal');
var service = require('./routes/service');

var app = express();

// Create HTTP server
var server = require('http').Server(app);
var io = require('socket.io')(server);

// Add socket.io as a middleware so it's accessible from the routes
app.use(function(req, res, next){
	res.io = io;
	next();
});

var socketIOfunctions = appRoot.require(path.join('app', 'socketIO'));
// Socket.io connection
io.on('connection', function (socket) {
	socketIOfunctions.socketListeners(socket);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/upload', upload);
app.use('/files', files);
app.use('/terminal', terminal);
app.use('/service', service);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = {
	app: app,
	server: server
};