var fs = require('fs');
var path = require('path');
const rimraf = require('rimraf');
const appRoot = require('app-root-path');
const util = require('util')
var status = appRoot.require('status');
const moment = require('moment');
var fileManager = appRoot.require(path.join('app', 'fileManager'));
var machineManager = appRoot.require(path.join('app', 'machineManager'));
const config = appRoot.require('config');

module.exports = {
	handleTCPdata: function (port, chunk, socket) {
		socket.emit('terminal_data', {
			port: port,
			data: chunk,
			time: moment().format(config.logs.userDateFormat)
		})
	},

	/** @function socketListeners - List of functions
	 *
	 * @param socket
	 */
	socketListeners: function (socket) {
		/* ---------- EVENT: terminal_connect ----------
		 * from /terminal
		 * Attempt to connect to the machine over a TCP terminal at a given port.
		 * data: {
		 * 	port: data.port
		 * }
		 */
		socket.on('terminal_connect', function (data) {
			// Acknowledge that a request was made:
			socket.emit('terminal_connect', {
				portName: data.portName,
				portNumber: config.machine[data.portName],
				completed: false,
				time: moment().format(config.logs.userDateFormat)
			});
			// Try connecting to the machine at the requested port
			machineManager.connect(data.portName, socket);
		});

		/* ---------- EVENT: terminal_disconnect ----------
		 * from /terminal
		 * Disconnect the TCP port.
		 * data: {
		 * 	port: data.port
		 * }
		 */
		socket.on('terminal_disconnect', function (data) {
			// Acknowledge that a request was made:
			socket.emit('terminal_disconnect', {
				portName: data.portName,
				portNumber: config.machine[data.portName],
				completed: false,
				time: moment().format(config.logs.userDateFormat)
			});
			// Disconnect from machine
			machineManager.disconnect(data.portName, socket);
		});

		/* ---------- EVENT: terminal_data ----------
		 * from /terminal
		 * Send data to the specific port
		 * data: {
		 * 	port: data.port
		 * 	data: <string>
		 * }
		 */
		socket.on('terminal_data', function (data) {
			machineManager.write(data.portName, data.message, socket);
		});

		/* ---------- EVENT: terminal_status-info ----------
		 * from /terminal
		 * Request for current status of terminal
		 */
		socket.on('terminal_status-info', function () {
			console.log('[socket] terminal_status_info');
			// Try connecting to the machine at the requested port
			let statusInfo = machineManager.getSocketStatus();
			socket.emit('terminal_status-info', statusInfo);
			// Broadcast the status to any other terminals that might be open
			socket.broadcast.emit('terminal_status-info', statusInfo);
		});

		/* ---------- EVENT: fileupload_folder ----------
		 * from /fileupload
		 * Emitted when a folder is selected
		 *
		 * data:
		 * {
		 * 	currentPath: <str>,
		 * 	selectedItem: {
		 * 		name: <str>
		 * 		type: <str: "file", "folder">
		 * 	  	}
		 * }
		 *
		 * Emits filesAndFolders, an object with 3 keys:
		 * {
		 * 	"path:"	/Volumes/OLSTICK/ramya",
		 * 	"files":
		 * 		[
		 * 			{"name":"tube square 1.5in 0.065thick 0.125rad"}
		 * 		],
		 * 	"folders":
		 * 		[
		 * 			{"name":"bottle opener"},
		 * 			{"name":"processtable-utility"}
		 * 		]
		 * 	}
		 */
		socket.on('fileupload_select-folder', function(data) {
			var pathToCheck = data.currentPath;
			if(data.selectedItem !== null) {
				pathToCheck = path.join(data.currentPath, data.selectedItem.name);
			}
			console.log('[fileupload_select-folder] pathToCheck: ' + pathToCheck);
			fileManager.getItemsAtPath(pathToCheck , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: fileupload_select-navigation ----------
		 * from /fileupload
		 * Emitted when the "go up" button is pressed
		 *
		 * path: {string} Current navigation path. Need to go up a level.
		 */
		socket.on('fileupload_select-navigation', function(currentPath) {
			var requestedPath = path.join(currentPath, '..');
			fileManager.getItemsAtPath(requestedPath , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					// Added case for the UDOO. If "going up a level" does not have any files or folders,
					// this indicates the USB has been unplugged. There should always be at least one folder
					// on your "way up", because that is how you got to be one level deeper.
					if((filesAndFolders.folders.length == 0)) {
						socket.emit('fileupload_error', 'Change of USB detected. Refreshing...');
					}
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: fileupload_delete-item ----------
		 * from /fileupload
		 * Emitted when the delete button is pressed.
		 *
		 * Deletes the item.
		 *
		 * data: {
		 * 	currentPath: <str>,
		 * 	selectedItemToDelete: {
		 * 		name: <str>,
		 * 		type: <str: "folder" or "file">
		 * 		}
		 * }
		 */
		socket.on('fileupload_delete-item', function (data) {
			var typeToDelete = data.selectedItemToDelete.type;
			var itemName = data.selectedItemToDelete.name;
			if(typeToDelete === 'file') {
				if(path.basename(itemName) !== '.fab') {
					itemName += '.fab';
				}
			}
			var pathToDelete = path.join(data.currentPath, itemName);
			rimraf(pathToDelete, function (err) {
				socket.emit('fileupload_delete-item-complete', err);
			})
		});

		/* ---------- EVENT: terminal_save ----------
		 * from /terminal
		 * Send data to the specific port
		 * data: {
		 * 	port: data.port
		 * 	data: <string>
		 * }
		 */
		socket.on('terminal_save', function (data) {
			console.log("socket.on terminal save");
			machineManager.saveLog(data);
		});

	}
};