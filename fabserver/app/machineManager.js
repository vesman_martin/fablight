/* machineManager.js
 *
 * Functions for interacting with the FabLight machine through the ExtraTech Motion Controller.
 * Manages TCP port connections to the controller including connecting, disconnecting, and sending data.
 * Only one TCP port can be open at a time over all users of the application.
 *
 */

// "use strict";
const net = require('net');
const appRoot = require('app-root-path');
const path = require('path');
const split = require('split');
const util = require('util');
const fs = require('fs');
const config = appRoot.require('config');
const moment = require('moment');
const http = require('http')

const machineAddress = config.machine.IPaddress;

module.exports = {
	status: {
		isConnected: false
	},

	/* Object to save (reuse) TCP socket objects and connection status.
	Each key in sockets is of the form:
	<portName> : {
		socket: <socket obj>,
		isConnected: <bool>
	}
	Where portName is a string that names the port, found in config.js.
	Ex) 'jobPort', 'debugPort', 'commandPort'
	*/
	sockets: {},
	debugPort: null,
	jobPort: null,
	commandPort: null,

	/** Try connecting to the TCP port
	 *
	 * if the TCP socket is already connected, do not re-connect.
	 * if the TCP socket already exists, re-use it and try to connect.
	 * if the TCP socket does not exist, create it and try to connect.
	 * if there is an error connecting, return an error message.
	 *
	 * @param portName
	 * @param sIO - socket IO socket object for sending requests back to the page.
	 */
	connect: function (portName, sIO) {
		console.log('[MM] connect ');
		let portNumber = config.machine[portName];
		// If the socket is not already in the sockets object, then create it.
		if(typeof module.exports.sockets[portName] === 'undefined') {
			module.exports.sockets[portName] = {
				socket: null,
				isConnected: false
			}
		}
		// Connect to the socket if not already connected and bind listeners
		if(module.exports.sockets[portName].isConnected === false) {
			module.exports.sockets[portName].socket = new net.Socket();
			module.exports.sockets[portName].socket = net.connect(portNumber, machineAddress);
			module.exports.sockets[portName].socket.setEncoding('utf8');
			// setKeepAlive required for connections that are open a long time. Every 1min (60,000ms)
			// an empty TCP packet is sent/received on the TCP layer.
			module.exports.sockets[portName].socket.setKeepAlive(true, 1000 * 60);
			// Bind socket events:
			module.exports.sockets[portName].socket.on('connect',
				module.exports.onSocketConnect.bind(
					{},
					portName,
					sIO,
					module.exports.sockets[portName].socket
				)
			);
			module.exports.sockets[portName].socket.on('end',
				module.exports.onSocketEnd.bind(
					{},
					sIO,
					module.exports.sockets[portName].socket
				)
			);
			module.exports.sockets[portName].socket.on('close',
				module.exports.onSocketClose.bind(
					{},
					portName,
					sIO,
					module.exports.sockets[portName].socket
				)
			);
			// FabServer receives TCP data from the controller.
			module.exports.sockets[portName].socket.on('data', function (chunk) {
				sIO.emit('terminal_data', {
					portName: portName,
					portNumber: config.machine[portName],
					data: chunk,
					time: moment().format(config.logs.userDateFormat)
				});
				// Certain commands, like store_binary require handshake between server and controller.
				// Listen for these messages in parseTCPdata function. Currently only a single function for port 43
				module.exports.parseTCPdata(chunk, sIO);
			});
			module.exports.sockets[portName].socket.on('error', function (error) {
					console.log('[MM] socket error!');
					// Kill socket
					module.exports.sockets[portName].socket.destroy();
					module.exports.sockets[portName].socket.unref();
					sIO.emit('terminal_error', {
						portName: portName,
						portNumber: config.machine[portName],
						err: error.message,
						time: moment().format(config.logs.userDateFormat)
					});
				}
			);
		}
	},

	/** Parse data coming through a TCP socket from the controller.
	 * Used to look for messages with sockets
	 *
	 * @param chunk
	 * @param sIO
	 */
	parseTCPdata: function(chunk, sIO) {
		console.log('[MM] parseTCPdata ' + chunk);
		
		if(chunk.indexOf('Pleasesss download file now') > -1) {
			// Controller is ready to get a module update.
			// Get the file path of the binary file in public/software/toSend
			let filePath = path.join(appRoot.path, 'public', 'software', 'toSend');
			console.log("filepath: " + filePath);
			fs.readdir(filePath, function (err, itemArray) {
				for(let i = 0; i < itemArray.length; i++) {
					let itemName = itemArray[i];
					console.log("filename to send: " + itemName);
					let fileExtension = path.extname(itemName).toLowerCase();
					if(fileExtension === ".uc") {
						filePath = path.join(filePath, itemName);
					}
				}
			});
			console.log("filepath with item: " + filePath);

			// If a .uc file was found, then send that and delete it.
			if(filePath !== null) {
				module.exports.sendBinaryFile(filePath, 'debugPort', sIO, function (err) {
					if(err) {
						sIO.emit('terminal_error', {
							portName: portName,
							portNumber: config.machine[portName],
							err: 'Error sending binary file. Details: ' + err,
							time: moment().format(config.logs.userDateFormat)
						});
					}
					else {
						sIO.emit('terminal_message', {
							portName: portName,
							portNumber: config.machine[portName],
							data: 'Stored file on machine.',
							time: moment().format(config.logs.userDateFormat)
						});
						fs.unlink(filePath, function (err) {
							sIO.emit('terminal_error', {
								portName: portName,
								portNumber: config.machine[portName],
								err: 'Error deleting stored file. Details: ' + err,
								time: moment().format(config.logs.userDateFormat)
							});
						});
					}
				})
			}
			else {
				sIO.emit('terminal_error', {
					portName: portName,
					portNumber: config.machine[portName],
					err: 'Can\'t find file to store. Details: ' + err,
					time: moment().format(config.logs.userDateFormat)
				});
			}
		}
	},

	/** Listener event when the socket is connected
	 *
	 * @param portName
	 * @param sIO
	 * @param socket
	 */
	onSocketConnect: function (portName, sIO, socket) {
		console.log('[MM] onSocketConnect')
		module.exports.sockets[portName].isConnected = true;
		let sIOdata = {
			portName: portName,
			portNumber: config.machine[portName],
			completed: true,
			time: moment().format(config.logs.userDateFormat)
		}
		sIO.emit('terminal_connect', sIOdata);
		// Broadcast to all other sockets that might be active (i.e. another browser)
		sIO.broadcast.emit('terminal_connect', sIOdata);
	},
	onSocketEnd: function (sIO, socket) {
		console.log('[MM] onSocketEnd');
	},
	onSocketClose: function (portName, sIO, socket) {
		module.exports.sockets[portName].isConnected = false;
		console.log('[MM] onSocketClose!');
		let sIOdata = {
			portName: portName,
			portNumber: config.machine[portName],
			completed: true,
			time: moment().format(config.logs.userDateFormat)
		};
		sIO.emit('terminal_disconnect', sIOdata);
		// Broadcast to all other sockets that might be active (i.e. another browser)
		sIO.broadcast.emit('terminal_disconnect', sIOdata);
	},

	/** Disconnect the socket stored with the given portName
	 *
	 * @param portName
	 * @param sIO - socketIO socket to send information to the browser
	 */
	disconnect: function(portName, sIO) {
		console.log('[MM] disconnect');
		if(typeof module.exports.sockets[portName] !== 'undefined') {
			module.exports.sockets[portName].socket.end();
		}
		else {
			sIO.emit('terminal_error', {
				portName: portName,
				portNumber: config.machine[portName],
				err: 'Socket does not exist yet',
				time: moment().format(config.logs.userDateFormat)
			});
		}
	},

	/** Write something to do the socket or send back an error using a socket.io socket.
	 *
	 * @param portName
	 * @param message - Message string
	 * @param sIO - (optional) socketIO socket to send information to the browser
	 */
	write: function(portName, message, sIO) {
		// Only write to a port if the socket is open.
		if (typeof module.exports.sockets[portName] !== 'undefined') {
			if(module.exports.sockets[portName].isConnected === true) {
				module.exports.sockets[portName].socket.write(
					message + '\n',		// message
					'UTF8',				// encoding
					function (err) {	// callback when data written out
						//console.log('data written');
					});
			}
			else {
				if(typeof sIO !== 'undefined') {
					sIO.emit('terminal_error', {
						portName: portName,
						portNumber: config.machine[portName],
						err: 'Not connected to machine.',
						time: moment().format(config.logs.userDateFormat)
					});
				}
			}
		}
		else {
			if(typeof sIO !== 'undefined') {
				sIO.emit('terminal_error', {
					portName: portName,
					portNumber: config.machine[portName],
					err: 'Never connected to machine.',
					time: moment().format(config.logs.userDateFormat)
				});
			}
		}
	},



	/* Object created with the following form to easily see which sockets
	 * are connected and which ones aren't. Example:
	 * { debugPort: true }
	 */
	getSocketStatus: function() {
		console.log('[MM] getSocketStatus');
		let socketStatus = {};
		for(let socket in module.exports.sockets) {
			socketStatus[socket] = module.exports.sockets[socket].isConnected;
		}
		return socketStatus;
	},

	/** Send a file to the machine using a TCP port.
	 *
	 * @param filePath
	 * @param portName
	 * @param sIO - optional. socketIO object
	 * @param callback - called once complete. Returns an error if there's an error.

	 A better way, if possible, would be to store the file in a binary mode.
	 To do this, the command is <file size in bytes>  <file location>  store_binary
	 Then wait for the "Please download file now..."
	 then send the file.

	 By doing it this way, it will not translate non-printable characters.
	 */
	sendBinaryFile: function (filePath, portName, sIO, callback) {
		if(typeof module.exports.sockets[portName] !== 'undefined') {
			if(module.exports.sockets[portName].isConnected === true) {
				var body = '';
				var fileStream = fs.createReadStream(filePath, {
					encoding: 'binary'
				});
				fileStream.on('error', function(err){
					console.log('[MM][sendBinaryFile] Error: ' + err);
				});
				fileStream.on('open',function() {
					console.log('[MM][sendBinaryFile] open filestream on ' + filePath);
					fileStream.setEncoding('binary');
				});
				fileStream.on('close', function(){
					console.log('[MM][sendBinaryFile] file stream close event on ' + filePath);
				});
				fileStream.on('end', function(chunk) {
					console.log('[MM][sendBinaryFile] end of file stream on ' + filePath);
					//module.exports.sockets[portName].socket.write(body, 'binary');
					console.log('-----socket write' +
						'\nsize:\t' + body.length +
						'\nbytesWritten:\t' + module.exports.sockets[portName].socket.bytesWritten +
						'\n-----end socket write'
					);
					callback();
				});
				// actually write the file to the machine
				fileStream.on('data', function(chunk) {
					body += chunk;
					module.exports.sockets[portName].socket.write(chunk, 'binary');
					console.log('-----socket info' +
						'\nbytesRead:\t' + fileStream.bytesRead +
						'\nbytesRead:\t' + module.exports.sockets[portName].socket.bytesRead +
						'\nbytesWritten:\t' + module.exports.sockets[portName].socket.bytesWritten +
						'\n-----end socket info'
					);
				});
			}
			else {
				callback('Port is not connected.')
			}
		}
		else {
			// The port does not exist
			callback('Port does not exist')
		}
	}
};