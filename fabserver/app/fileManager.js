var fs = require('fs');
var path = require('path');
const rimraf = require('rimraf');
const appRoot = require('app-root-path');
var status = appRoot.require('status');

module.exports = {

	/** Get a list of all .fab and folders at a given path
	 *
	 * @requestedPath
	 * @callback
	 */
	getItemsAtPath: function (requestedPath, callback) {
		// itemArray is array with all file & folder names in a given directory
		fs.readdir(requestedPath, function (err, itemArray) {
			if (err) {
				console.log('[usbManager][getFilesAndFolders] Error getting files at ' + requestedPath);
				// "No such file or directory"
				// If the USB has been unplugged
				if (err.code == "ENOENT") {
					callback('File or directory not found.');
				}
				else {
					callback('Error reading file. Code: ' + err.code);
				}
			}
			else {
				var result = {
					path: requestedPath,
					files: [],
					folders: []
				};
				// If there are no items in the array, still call the callback:
				if(itemArray.length === 0) {
					callback(null, {
						folders: [],
						files: [],
						empty: true,
						path: requestedPath
					})
				}
				else {
					// Iterate over each item in the array
					var count = 0;
					for (var i = 0; i < itemArray.length; i++) {
						(function (idx) {
							var itemName = itemArray[idx];
							var itemPath = path.join(requestedPath, itemName);
							fs.stat(itemPath, function (err, stats) {
								if (err) throw err;
								if (stats.isDirectory() == true) {
									// Filter out hidden folders, they start with a "."
									if (!/^\..*/.test(itemName)) {
										result.folders.push({
											name: path.basename(itemName)
										});
									}
								}
								else {
									// Filter out hidden files that start with a "._"
									if (!/^\.\_.*/.test(itemName)) {
										// Only care about .fab files, not .fab1 or anything else.
										var fileExtension = path.extname(itemName).toLocaleLowerCase();
										if (fileExtension == ".fab") {
											result.files.push({
												name: path.basename(itemName, '.fab')
											})
										}
									}
								}
								count++;
								// Callback once every item has been processed.
								if (count > itemArray.length - 1) {
									// Sort the files and folders array case-insensitively
									result.files.sort(module.exports.sortNamesAlphaNoCase);
									result.folders.sort(module.exports.sortNamesAlphaNoCase);
									result.empty = false;
									callback(null, result);
								}
							});
						}(i)); // Closure: pass the index from the for loop
					}
				}
			}
		})
	}
}