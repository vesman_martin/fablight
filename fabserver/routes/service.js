var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
	res.render('service', {
		title: 'Service Tools'
	})
});

/* GET users listing. */
router.get('/update', function(req, res, next) {
	res.render('Update', {
		title: 'Update software'
	})
});

module.exports = router;