var express = require('express');
var router = express.Router();
const path = require('path');
const multer = require('multer');
const appRoot = require('app-root-path');
const mv = require('mv-lite');
var config = require('../config');

router.get('/', function (req, res, next) {
	res.render('upload', {
		title: "Upload a file"
	})
});

// Set standard upload directory
var tmpUploadDir = config.files.tmpPath;
var uploadDir = config.files.path;
// multer set up to store to disk
var storage = multer.diskStorage({
	destination: tmpUploadDir,
	filename: function(req, file, callback) {
		// var originalName = file.originalname;
		// var extension = path.extname(originalName);
		// var nameOnly = originalName.slice(0, -extension.length);
		// var filename = nameOnly + '-processed' + extension;
		var filename = file.originalname;
		callback(null,filename);
	}
});
var upload = multer({ storage: storage }).single('file');

/* Upload form to /upload */
router.post('/', function(req, res, next) {

	upload(req, res, function(err) {
		if(err) {
			return res.status(500).json({ uploaded: false, err: err }).end();
		}
		else {
			// The job file is uploaded to the temporary path.
			// req.body from fabcreator will have: overwrite, username, password, customer, jobnumber
			var filename = req.file.filename;
			var username = req.body.username;
			var customer = req.body.customer;
			var jobnumber = req.body.jobnumber;
			var overwrite = req.body.overwrite;

			// Move the job to final location in the shared folder accesible by the HMI.
			var oldPath = path.join(tmpUploadDir, filename);
			var newPath = path.join(uploadDir, filename);
			if(typeof req.body !== 'undefined') {
				if(username.length > 0) {
					if(customer.length > 0) {
						if(jobnumber.length > 0) {
							newPath = path.join(uploadDir, username, customer, jobnumber, filename);
						}
						else {
							newPath = path.join(uploadDir, username, customer, filename);
						}
					}
					else {
						newPath = path.join(uploadDir, username, filename);
					}
				}
			}
			// If the file name doesn't have a .fab extension, add it to the new path.
			if(path.extname(filename) === '') {
				newPath = newPath + '.fab';
			}
			console.log('[upload.js] New Path: ' + newPath);
			// Move to final upload place, creating subfolders if necessary.
			mv(oldPath, newPath, {mkdirp: true}, function (err) {
				if(err) {
					res.status(500).json({ uploaded: false, err: err }).end();
				}
				else {
					res.status(200).json({ uploaded: true }).end();
				}
			});
		}
	});
});


module.exports = router;