// required modules
const express = require('express');
const router = express.Router();
const split = require('split');
const appRoot = require('app-root-path');
const path = require('path');
const multer = require('multer');
const fs = require('fs');

var status = appRoot.require('status');	// for keeping track of state info
var machineManager = appRoot.require(path.join('app', 'machineManager'));	// for TCP onData functions
var socketIO = appRoot.require(path.join('app', 'socketIO'));
var config = appRoot.require('config');	// config prod vs dev for laser address and port, see config.js for info

const laserPort = config.machine.debugPort;
const laserAddress = config.machine.IPaddress;

/* ----------- router functions ----------- */
router.get('/', function(req, res, next) {
	res.render('terminal', {
		title: 'Terminal'
	});
});

// Set standard upload directory
let uploadDir = path.join(appRoot.path, 'public', 'software', 'toSend');
// multer set up to store to disk
let storage = multer.diskStorage({
	destination: uploadDir,
	filename: function(req, file, callback) {
		// var originalName = file.originalname;
		// var extension = path.extname(originalName);
		// var nameOnly = originalName.slice(0, -extension.length);
		// var filename = nameOnly + '-processed' + extension;
		var filename = file.originalname;
		callback(null,filename);
	}
});
let upload = multer({ storage: storage }).single('file');

// POST request to send a .uc file
router.post('/send', function (req, res, next) {
	upload(req, res, function(err) {
		if(err) {
			return res.status(500).json({ uploaded: false, err: err }).end();
		}
		else {
			// The .ucj file is uploaded to public/software temporarily and then deleted
			let fileName = req.file.filename;
			let filePath = path.join(uploadDir, fileName);
			// Send (Stream) file over TCP debug port
			console.log('[MM][sendFile] ' + fileName);
			machineManager.sendBinaryFile(filePath, 'debugPort', null, function (err) {
				if(err) {
					fs.unlink(filePath, function (err) {
						res.status(500).json({ uploaded: false, err: err }).end();
					});
				}
				else {
					fs.unlink(filePath, function (err) {
						res.status(200).json({ uploaded: true }).end();
					});
				}
			})
		}
	});
});

// POST request to store a .uc file
router.post('/store', function (req, res, next) {
	upload(req, res, function (err) {
		if(err) {
			return res.status(500).json({ stored: false, err: err }).end();
		}
		else {
			// The .ucj file is uploaded to public/software temporarily and then deleted
			let fileName = req.file.filename;
			let location = req.body.location;
			let filePath = path.join(uploadDir, fileName);
			// <file size bytes> <file location> store_binary
			fs.stat(filePath, function (err, stats) {
				let fileSizeBytes = stats.size;
				//let location = getFileLocation(fileName);
				console.log('[MM][storeFile] location: ' + location + '  size: ' + fileSizeBytes);
				if(location !== null) {
					let command = fileSizeBytes + ' ' + location + ' store_binary';
					config.softwareUpdate = {
						fileName: null,
						path: null,
						location: null
					}
					machineManager.write('debugPort', command);

					// Send (Stream) file over TCP debug port
					machineManager.sendBinaryFile(filePath, 'debugPort', null, function (err) {
						if(err) {
							fs.unlink(filePath, function (err) {
								res.status(500).json({ uploaded: false, err: err }).end();
							});
						}
						else {
							fs.unlink(filePath, function (err) {
								res.status(200).json({ uploaded: true }).end();
							});
						}
					})

					//return res.status(200).json({ stored: true}).end();
				}
				else {
					return res.status(200).json({ stored: false}).end();
				}
			});
			
		}
	})
});

/**	@function getFileLocation
 * Given a file name of the MOD or INIT to upload, get the location to store it to.
 *
 * @param fileName
 * @returns {str} Location to store the file to. Returns null if the name of the file does not have enough
 * 	info to determine location.
 */
function getFileLocation(fileName) {
	// First look for MOD files
	let found = fileName.match(/^MOD(\d*)_(.+).uc$/);
	let location = null;
	// Result of found:
	// 0 - original string
	// 1 - first match (MODX or INIT)
	// 2 - second match (name)
	if(found !== null) {
		if(found[1].indexOf('MOD') > -1) {
			location = found[1].slice(3);
		}
	}
	// Then look for INIT files
	else {
		let found = fileName.match(/^INIT_(.+).uc$/);
		if(found !== null) {
			if(found[1].indexOf('INIT') > -1) {
				location = '-3';
			}
			else {
				// TODO more robust qualification of file
				location = '-3';
			}
		}
	}
	return location;
}


module.exports = router;