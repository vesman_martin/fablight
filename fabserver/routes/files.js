var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
const appRoot = require('app-root-path');
var config = appRoot.require('config');
var fileManager = appRoot.require(path.join('app', 'fileManager'));

/* GET home page. */
router.get('/', function(req, res, next) {
	var storagePath = config.files.path;
	fileManager.getItemsAtPath(storagePath, function (err, filesAndFolders) {
		if(err) {
			res.render('files', {
				title: 'File manager',
				itemsFound: false
			});
		}
		else {
			// Check if we're at the server root path. If not, display a "go up a level" button.
			var pathIsRoot = true;
			if(filesAndFolders.path !== storagePath) {
				pathIsRoot = false;
			}
			var itemsFound = true;
			if((filesAndFolders.files.length < 1) && (filesAndFolders.folders.length < 1)) {
				itemsFound = false;
			}
			res.render('files', {
				title: 'File manager',
				itemsFound: itemsFound,
				pathIsRoot: pathIsRoot,
				path: filesAndFolders.path,
				files: JSON.stringify(filesAndFolders.files),
				folders: JSON.stringify(filesAndFolders.folders)
			})
		}
	})
});

function sortNamesAlphaNoCase(a, b) {
	a = a.name.toLowerCase();
	b = b.name.toLowerCase();
	if (a === b) return 0;
	if (a > b) return 1;
	return -1;
}

module.exports = router;