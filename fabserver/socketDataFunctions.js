var fs = require('fs');
var path = require('path');
const rimraf = require('rimraf');
const appRoot = require('app-root-path');
var status = require(appRoot + path.sep + 'status')

module.exports = {

	getItemsAtPath: function (requestedPath, callback) {
		// itemArray is array with all file & folder names in a given directory
		fs.readdir(requestedPath, function (err, itemArray) {
			if (err) {
				console.log('[usbManager][getFilesAndFolders] Error getting files at ' + requestedPath);
				// "No such file or directory"
				// If the USB has been unplugged
				if (err.code == "ENOENT") {
					callback('File or directory not found.');
				}
				else {
					callback('Error reading file. Code: ' + err.code);
				}
			}
			else {
				var result = {
					path: requestedPath,
					files: [],
					folders: []
				};
				// If there are no items in the array, still call the callback:
				if(itemArray.length === 0) {
					callback(null, {
						folders: [],
						files: [],
						empty: true,
						path: requestedPath
					})
				}
				else {
					// Iterate over each item in the array
					var count = 0;
					for (var i = 0; i < itemArray.length; i++) {
						(function (idx) {
							var itemName = itemArray[idx];
							var itemPath = path.join(requestedPath, itemName);
							fs.stat(itemPath, function (err, stats) {
								if (err) throw err;
								if (stats.isDirectory() == true) {
									// Filter out hidden folders, they start with a "."
									if (!/^\..*/.test(itemName)) {
										result.folders.push({
											name: path.basename(itemName)
										});
									}
								}
								else {
									// Filter out hidden files that start with a "._"
									if (!/^\.\_.*/.test(itemName)) {
										// Only care about .fab files, not .fab1 or anything else.
										var fileExtension = path.extname(itemName).toLocaleLowerCase();
										if (fileExtension == ".fab") {
											result.files.push({
												name: path.basename(itemName, '.fab')
											})
										}
									}
								}
								count++;
								// Callback once every item has been processed.
								if (count > itemArray.length - 1) {
									// Sort the files and folders array case-insensitively
									result.files.sort(module.exports.sortNamesAlphaNoCase);
									result.folders.sort(module.exports.sortNamesAlphaNoCase);
									result.empty = false;
									callback(null, result);
								}
							});
						}(i)); // Closure: pass the index from the for loop
					}
				}
			}
		})
	},

	handleSocketIO: function (socket) {
		/* ---------- EVENT: fileupload_folder ----------
		 * from /fileupload
		 * Emitted when a folder is selected
		 *
		 * data:
		 * {
		 * 	currentPath: <str>,
		 * 	selectedItem: {
		 * 		name: <str>
		 * 		type: <str: "file", "folder">
		 * 	  	}
		 * }
		 *
		 * Emits filesAndFolders, an object with 3 keys:
		 * {
		 * 	"path:"	/Volumes/OLSTICK/ramya",
		 * 	"files":
		 * 		[
		 * 			{"name":"tube square 1.5in 0.065thick 0.125rad"}
		 * 		],
		 * 	"folders":
		 * 		[
		 * 			{"name":"bottle opener"},
		 * 			{"name":"processtable-utility"}
		 * 		]
		 * 	}
		 */
		socket.on('fileupload_select-folder', function(data) {
			var pathToCheck = data.currentPath;
			if(data.selectedItem !== null) {
				pathToCheck = path.join(data.currentPath, data.selectedItem.name);
			}
			console.log('[fileupload_select-folder] pathToCheck: ' + pathToCheck);
			module.exports.getItemsAtPath(pathToCheck , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: fileupload_select-navigation ----------
		 * from /fileupload
		 * Emitted when the "go up" button is pressed
		 *
		 * path: {string} Current navigation path. Need to go up a level.
		 */
		socket.on('fileupload_select-navigation', function(currentPath) {
			var requestedPath = path.join(currentPath, '..');
			module.exports.getItemsAtPath(requestedPath , function(err, filesAndFolders) {
				if(err) {
					socket.emit('fileupload_error', err);
				}
				else {
					// Added case for the UDOO. If "going up a level" does not have any files or folders,
					// this indicates the USB has been unplugged. There should always be at least one folder
					// on your "way up", because that is how you got to be one level deeper.
					if((filesAndFolders.folders.length == 0)) {
						socket.emit('fileupload_error', 'Change of USB detected. Refreshing...');
					}
					socket.emit('fileupload_folder-contents', filesAndFolders);
				}
			});
		});

		/* ---------- EVENT: fileupload_delete-item ----------
		 * from /fileupload
		 * Emitted when the delete button is pressed.
		 *
		 * Deletes the item.
		 *
		 * data: {
		 * 	currentPath: <str>,
		 * 	selectedItemToDelete: {
		 * 		name: <str>,
		 * 		type: <str: "folder" or "file">
		 * 		}
		 * }
		 */
		socket.on('fileupload_delete-item', function (data) {
			var typeToDelete = data.selectedItemToDelete.type;
			var itemName = data.selectedItemToDelete.name;
			if(typeToDelete === 'file') {
				if(path.basename(itemName) !== '.fab') {
					itemName += '.fab';
				}
			}
			var pathToDelete = path.join(data.currentPath, itemName);
			rimraf(pathToDelete, function (err) {
				socket.emit('fileupload_delete-item-complete', err);
			})
		})

		/* event: port23-userCommand
		 * data:  command sent from terminal in browser
		 * action: pass the command to the TCP port 23
		 */
		socket.on('port23-userCommand', function(command) {
			console.log('from user: ' + command);
			if(status.laserSocket23 != null) {
				status.laserSocket23.write(command);	// send over TCP
			}
			else {
				socket.emit('port23-status', 'Can\'t write to machine. Machine offline.');
			}
		});

	}
}