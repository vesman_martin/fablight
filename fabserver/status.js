/*
 * status.js
 *
 * Keep track of machine status
 *
 */

module.exports = {
	isOnline: false,			// general status of machine, on or offline
	machine: {
		port43socket: null,		// laser socket object
		port43stream: null
	}
};